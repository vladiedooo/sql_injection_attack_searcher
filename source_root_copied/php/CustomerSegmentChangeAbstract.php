<?php
/**
 * Author: Sean Dunagan (github: dunagan5887)
 * Date: 4/28/17
 */

namespace Scc\PremiumMembership\Observer\CustomerSegment;

use Magento\Framework\Event\ObserverInterface;

/**
 * Class CustomerSegmentChangeAbstract
 *
 * @package Scc\PremiumMembership\Observer\CustomerSegment
 */
abstract class CustomerSegmentChangeAbstract extends SegmentConversionAbstract implements ObserverInterface
{
    /**
     * Defines whether the subclass is specific to adding customers to a segment, or removing them
     *
     * @return bool
     */
    abstract public function isAddingToSegment();

    /**
     * @param \Magento\Framework\Event\Observer $observer
     * @return $this
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        $segment_ids = $observer->getData('segment_ids');
        $website_id = $observer->getData('website_id');
        foreach($segment_ids as $segment_id)
        {
            if ($this->_isPremiumCustomerSegmentId($segment_id))
            {
                $customer_id = $observer->getData('customer_id');
                $this->_executeCustomerSegmentConversions($segment_id, [$customer_id], $this->isAddingToSegment(),
                                                          $website_id);
            }
        }
    }
}
