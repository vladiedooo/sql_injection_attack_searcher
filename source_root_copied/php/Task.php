<?php
/**
 * Author: Sean Dunagan (https://github.com/dunagan5887)
 * Created: 4/12/16
 */

namespace Dunagan\ProcessQueue\Model;

/**
 * Author: Sean Dunagan (https://github.com/dunagan5887)
 * Created: 4/20/16
 *
 * Class Task
 * @package Dunagan\ProcessQueue\Model
 *
 * This class represents the ORM model class for the Process Queue task table
 *
 * @method \Dunagan\ProcessQueue\Model\ResourceModel\Task getResource()
 */
class Task extends \Magento\Framework\Model\AbstractModel implements TaskInterface
{
    const ERROR_INVALID_OBJECT_CLASS = 'The specified object class %1 does not refer to any existing classes in the system';
    const ERROR_METHOD_DOES_NOT_EXIST = 'Method %1 does not exist on object of class %2';
    // The fields on the database table
    const PRIMARY_KEY_FIELD = 'task_id';
    const CODE_FIELD = 'code';
    const STATUS_FIELD = 'status';
    const OBJECT_FIELD = 'object';
    const METHOD_FIELD = 'method';
    const ARGUMENTS_FIELD = 'serialized_arguments_object';
    const STATUS_MESSAGE_FIELD = 'status_message';
    const SUBJECT_ID = 'subject_id';
    const CREATED_AT_FIELD = 'created_at';
    const LAST_EXECUTED_AT_FIELD = 'last_executed_at';
    // These are the valid statuses that a task can be placed in
    const STATUS_PENDING = 1;
    const STATUS_PROCESSING = 2;
    const STATUS_COMPLETE = 3;
    const STATUS_ERROR = 4;
    const STATUS_ABORTED = 5;

    /**
     * @var \Magento\Framework\ObjectManagerInterface
     */
    protected $_objectManager;

    // Define the task states which should allow a task to be processed by the crontab
    protected $_open_for_processing_states = array(self::STATUS_PENDING, self::STATUS_ERROR);
    // Define the tasks which should be considered as finalized
    protected $_finalized_states = array(self::STATUS_ABORTED, self::STATUS_COMPLETE);
    // A mapping of task statuses to labels
    protected $_valid_status_labels = array(self::STATUS_PENDING => 'Pending',
                                            self::STATUS_PROCESSING => 'Processing',
                                            self::STATUS_COMPLETE => 'Complete',
                                            self::STATUS_ERROR => 'Error',
                                            self::STATUS_ABORTED => 'Aborted');
    // A mapping of task statuses to action labels
    protected $_status_action_labels = array(self::STATUS_PENDING => 'Execute',
                                             self::STATUS_ERROR => 'Retry');

    /**
     * This method implements the functionality to execute the task's callback method
     *
     * @return \Dunagan\ProcessQueue\Model\Task\ResultInterface
     * @throws \Exception
     */
    public function executeTask()
    {
        // Create the callback object
        $object_class = $this->getObject();
        $callbackObject = $this->_objectManager->create($object_class);
        // Verify that the callback object was successfully created
        if (!is_object($callbackObject))
        {
            // Throw an exception that prevents execution
            $error_message = __(self::ERROR_INVALID_OBJECT_CLASS, $object_class);
            throw new \Exception($error_message);
        }
        // Verify that the callback method exists on the callback object
        $callback_method = $this->getMethod();
        if (!method_exists($callbackObject, $callback_method))
        {
            // Throw an exception that prevents execution
            $error_message = __(self::ERROR_METHOD_DOES_NOT_EXIST, $callback_method, $object_class);
            throw new \Exception($error_message);
        }
        $argumentsObject = $this->getArgumentsObject();
        // Execute the task object's callback method
        $methodCallbackResult = $callbackObject->$callback_method($argumentsObject);
        // Check if the callback returned an object which implements ResultInterface
        if (!($methodCallbackResult instanceof \Dunagan\ProcessQueue\Model\Task\ResultInterface))
        {
            // If not, create a Task\Result object to return to the calling block
            $methodCallbackResultToReturn = $this->_objectManager->create('\Dunagan\ProcessQueue\Model\Task\Result');
            // Set whatever the callback returned on the Task\Result object
            $methodCallbackResultToReturn->setMethodCallbackResult($methodCallbackResult);
            // Assume that the task should be placed in the COMPLETE status
            $methodCallbackResultToReturn->setTaskStatus(\Dunagan\ProcessQueue\Model\Task::STATUS_COMPLETE);
            return $methodCallbackResultToReturn;
        }
        // Return the callback result object
        return $methodCallbackResult;
    }

    /**
     * After the task's callback method has been executed, act on the Task\ResultInterface object that was returned
     *  by the call to executeTask()
     *
     * @param Task\ResultInterface $taskExecutionResult
     */
    public function actOnTaskResult(\Dunagan\ProcessQueue\Model\Task\ResultInterface $taskExecutionResult)
    {
        // Validate that the status specified by $taskExecutionResult is a valid status
        $execution_status = $taskExecutionResult->getTaskStatus();
        if (!$this->isStatusValid($execution_status))
        {
            // If the status set on the result object wasn't valid, assume task completion
            $execution_status = self::STATUS_COMPLETE;
            // TODO Log error message here
        }
        // Get the task's status_message from the $taskExecutionResult object
        $status_message = $taskExecutionResult->getTaskStatusMessage();
        // Update the status and status_message value for the task object
        $this->getResource()->setExecutionStatusForTask($execution_status, $this, $status_message);
    }

    /**
     * Update the task's database row to reflect that the task is now being processed to prevent other execution
     *  threads from executing the task
     *
     * @return int
     */
    public function attemptUpdatingRowAsProcessing()
    {
        return $this->getResource()->attemptUpdatingRowAsProcessing($this);
    }

    /**
     * Select the task's database row for update to prevent other mysql connections from modifying the row while we're
     *  processing the task
     *
     * @return int
     */
    public function selectForUpdate()
    {
        return $this->getResource()->selectForUpdate($this);
    }

    /**
     * Return the task states which denote that a task is available to be processed
     *
     * @return array
     */
    public function getOpenForProcessingStates()
    {
        return $this->_open_for_processing_states;
    }

    /**
     * @return array
     */
    public function getFinalizedStates()
    {
        return $this->_finalized_states;
    }

    /**
     * Returns an array mapping task status values to the status labels
     *
     * @return array
     */
    public function getStatusAndLabelsOptionsArray()
    {
        $options_array = array();
        foreach ($this->_valid_status_labels as $status_value => $status_label)
        {
            $options_array[$status_value] = __($status_label);
        }
        return $options_array;
    }

    /**
     * Returns the status value for the task
     *
     * @return int
     */
    public function getStatus()
    {
        return $this->getData(self::STATUS_FIELD);
    }

    /**
     * Returns the subject_id for the task, if one is set
     *
     * @return string
     */
    public function getSubjectId()
    {
        return $this->getData(self::SUBJECT_ID);
    }

    /**
     * Returns the primary key value for the task's database row
     *
     * @return int
     */
    public function getTaskId()
    {
        return $this->getData(self::PRIMARY_KEY_FIELD);
    }

    /**
     * Returns whether the task is currently processing
     *
     * @return bool
     */
    public function isCurrentlyProcessing()
    {
        $current_status = $this->getStatus();
        return ($current_status == self::STATUS_PROCESSING);
    }

    /**
     * Update the task's database row to reflect that the task's status is ERROR.
     * Also allows for updating the database row's status_message to display an error message
     *
     * @param null|string $error_message
     * @return mixed
     */
    public function setTaskAsErrored($error_message = null)
    {
        return $this->getResource()->setTaskAsErrored($this, $error_message);
    }

    /**
     * Returns the callback object's classname
     *
     * @return string
     */
    public function getObject()
    {
        return $this->getData(self::OBJECT_FIELD);
    }

    /**
     * Returns the callback method
     *
     * @return string
     */
    public function getMethod()
    {
        return $this->getData(self::METHOD_FIELD);
    }

    /**
     * Returns an unserialized PHP object or array which contains data which should be specific to this particular
     * task object's callback execution
     *
     * @return \stdClass|array
     */
    public function getArgumentsObject()
    {
        $serialized_arguments_object_string = $this->getData(self::ARGUMENTS_FIELD);
        $argumentsObject = unserialize($serialized_arguments_object_string);

        if ((!is_object($argumentsObject)) && !is_array($argumentsObject))
        {
            $argumentsObject = new \stdClass();
        }

        return $argumentsObject;
    }

    /**
     * Public scoped alias for _returnSuccessCallbackResult()
     *
     * @param string $success_message
     * @return Task\ResultInterface
     */
    public function returnSuccessCallbackResult($success_message)
    {
        return $this->_returnSuccessCallbackResult($success_message);
    }

    /**
     * Creates a callback result object to return to the ProcessQueue Task Processor so the system knows the status
     *  and success messaging of this task execution
     *
     * @param string $success_message - Success message to record on the task in the system
     * @return \Dunagan\ProcessQueue\Model\Task\ResultInterface
     */
    protected function _returnSuccessCallbackResult($success_message)
    {
        $methodCallbackResultToReturn = $this->_objectManager->create('\Dunagan\ProcessQueue\Model\Task\ResultInterface');
        /* @var $taskResultObject \Dunagan\ProcessQueue\Model\Task\ResultInterface */
        $methodCallbackResultToReturn->setTaskStatusMessage($success_message);
        $methodCallbackResultToReturn->setTaskStatus(self::STATUS_COMPLETE);

        return $methodCallbackResultToReturn;
    }

    /**
     * Public scoped alias for returnErrorCallbackResult()
     *
     * @param string $error_message
     * @return Task\ResultInterface
     */
    public function returnErrorCallbackResult($error_message)
    {
        return $this->_returnErrorCallbackResult($error_message);
    }

    /**
     * Creates a callback result object to return to the ProcessQueue Task Processor so the system knows the status
     *  and error messaging of this task execution
     *
     * @param string $error_message - Error message to record on the task in the system
     * @return \Dunagan\ProcessQueue\Model\Task\ResultInterface
     */
    protected function _returnErrorCallbackResult($error_message)
    {
        $methodCallbackResultToReturn = $this->_objectManager->create('\Dunagan\ProcessQueue\Model\Task\ResultInterface');
        /* @var $taskResultObject \Dunagan\ProcessQueue\Model\Task\ResultInterface */
        $methodCallbackResultToReturn->setTaskStatusMessage($error_message);
        $methodCallbackResultToReturn->setTaskStatus(self::STATUS_ERROR);

        return $methodCallbackResultToReturn;
    }

    /**
     * Public scoped alias for _returnAbortCallbackResult()
     *
     * @param string $error_message
     * @return Task\ResultInterface
     */
    public function returnAbortCallbackResult($error_message)
    {
        return $this->_returnAbortCallbackResult($error_message);
    }

    /**
     * Creates a callback result object to return to the ProcessQueue Task Processor so the system knows the status
     *  and error messaging of this task execution
     *
     * @param string $error_message - Error message to record on the task in the system
     * @return \Dunagan\ProcessQueue\Model\Task\ResultInterface
     */
    protected function _returnAbortCallbackResult($error_message)
    {
        $methodCallbackResultToReturn = $this->_objectManager->create('\Dunagan\ProcessQueue\Model\Task\ResultInterface');
        /* @var $taskResultObject \Dunagan\ProcessQueue\Model\Task\ResultInterface */
        $methodCallbackResultToReturn->setTaskStatusMessage($error_message);
        $methodCallbackResultToReturn->setTaskStatus(self::STATUS_ABORTED);

        return $methodCallbackResultToReturn;
    }

    /**
     * Returns whether the status is a valid status for a task to have
     *
     * @param string $status
     * @return bool
     */
    public function isStatusValid($status)
    {
        return in_array($status, array_keys($this->_valid_status_labels));
    }

    /**
     * Returns an array containing the valid task statuses and labels
     *
     * @return array
     */
    public function toOptionArray()
    {
        return $this->_valid_status_labels;
    }

    /**
     * Returns an array containing the labels for the actions which can be taken on tasks in various states
     *
     * @return array
     */
    public function taskActionsToOptionArray()
    {
        return $this->_status_action_labels;
    }

    /**
     * Initialize resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('Dunagan\ProcessQueue\Model\ResourceModel\Task');
    }

    /**
     * @param \Magento\Framework\Model\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param \Magento\Framework\ObjectManagerInterface $objectManager
     * @param \Magento\Framework\Model\ResourceModel\AbstractResource $resource
     * @param \Magento\Framework\Data\Collection\AbstractDb $resourceCollection
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\Model\Context $context,
        \Magento\Framework\Registry $registry,
        \Magento\Framework\ObjectManagerInterface $objectManager,
        \Magento\Framework\Model\ResourceModel\AbstractResource $resource = null,
        \Magento\Framework\Data\Collection\AbstractDb $resourceCollection = null,
        array $data = []
    )
    {
        $this->_objectManager = $objectManager;
        parent::__construct($context, $registry, $resource, $resourceCollection, $data);
    }
}
