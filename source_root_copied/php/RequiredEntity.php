<?php
/**
 * Author: Sean Dunagan (github: dunagan5887)
 * Date: 10/12/16
 */

namespace Scc\WebApi\Model\Exception\Service\Transmission;

/**
 * Class RequiredEntity
 * @package Scc\WebApi\Model\Exception\Service\Transmission
 */
class RequiredEntity extends \Scc\WebApi\Model\Exception
{

}
