<?php
/**
 * Author: Sean Dunagan (github: dunagan5887)
 * Date: 2/21/17
 */

namespace Scc\Products\Setup;

use Magento\Eav\Setup\EavSetupFactory;
use Magento\Framework\Setup\InstallDataInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;

/**
 * Class InstallData
 * @package Scc\Products\Setup
 */
class InstallData implements InstallDataInterface
{
    /**
     * EAV setup factory
     *
     * @var EavSetupFactory
     */
    private $eavSetupFactory;

    /**
     * Init
     *
     * @param EavSetupFactory $eavSetupFactory
     */
    public function __construct(EavSetupFactory $eavSetupFactory)
    {
        $this->eavSetupFactory = $eavSetupFactory;
    }

    /**
     * {@inheritdoc}
     */
    public function install(ModuleDataSetupInterface $setup, ModuleContextInterface $context)
    {
        $setup->startSetup();

        /** @var \Magento\Eav\Setup\EavSetup $eavSetup */
        $eavSetup = $this->eavSetupFactory->create(['setup' => $setup]);

        // Add the "Does Not Require Shipping" product attribute if it does not already exist
        $attribute_data_array
            = $eavSetup->getAttribute(\Magento\Catalog\Model\Product::ENTITY,
                                      \Scc\Products\Model\Shipping::DOES_PRODUCT_NOT_REQUIRE_SHIPPING_ATTRIBUTE_CODE);
        if (empty($attribute_data_array))
        {
            $eavSetup->addAttribute(
                \Magento\Catalog\Model\Product::ENTITY,
                \Scc\Products\Model\Shipping::DOES_PRODUCT_NOT_REQUIRE_SHIPPING_ATTRIBUTE_CODE,
                [
                    'type' => 'int',
                    'input' => 'boolean',
                    'backend' => 'Magento\Catalog\Model\Product\Attribute\Backend\Boolean',
                    'source' => 'Magento\Catalog\Model\Product\Attribute\Source\Boolean',
                    'label' => 'Product Does Not Require Shipping',
                    'default' => 1,
                    'required' => false,
                    'user_defined' => true,
                    'used_for_promo_rules' => true,
                    'global' => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_GLOBAL,
                    'group' => 'Product Details',
                    'sort_order' => '10000',
                    'visible_on_front' => true,
                    'used_in_product_listing' => false,
                    'used_for_sort_by' => false,
                    'searchable' => false,
                    'search_weight' => 1,
                    'filterable' => false,
                    'filterable_in_search' => false,
                    'position' => '',
                    'comparable' => false,
                    'visible_in_advanced_search' => false,
                    'apply_to' => 'virtual',
                    'is_used_in_grid' => false,
                    'is_visible_in_grid' => false,
                    'is_filterable_in_grid' => false,
                    'wysiwyg_enabled' => false,
                    'is_html_allowed_on_front' => false
                ]
            );
        }

        $setup->endSetup();
    }
}
