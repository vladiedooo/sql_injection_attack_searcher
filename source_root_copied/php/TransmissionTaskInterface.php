<?php
/**
 * Author: Sean Dunagan (github: dunagan5887)
 * Date: 9/21/16
 */

namespace Dunagan\WebApi\Model\Task\Api;

/**
 * This class defines functionality common to all ProcessQueue tasks which transmit entities to external systems via an
 *  API web service
 *
 * Class Transmission
 * @package Dunagan\WebApi\Model\Task\Api
 */
interface TransmissionTaskInterface
{
    /**
     * The task code which should be defined on the task during task creation.
     *
     * @return string
     */
    public function getTaskCode();

    /**
     * Should queue the entity to be transmitted to the external system
     * Should return the task which was just queued
     *
     * @param mixed $entity
     * @param array $additional_arguments_array
     * @param bool $queue_in_processing_state - If the transmission task should be executed immediately after being
     *                                              created, this should be set to true.
     *                                          If the task can wait to be executed by the next crontab iteration of the
     *                                              ProcessQueue task processor, this should be set to false.
     *                                          Default: false
     * @return \Dunagan\ProcessQueue\Model\TaskInterface
     */
    public function queueEntityTransmissionToExternalSystem($entity, array $additional_arguments_array = array(),
                                                            $queue_in_processing_state = false);

    /**
     * Executes the functionality to transmit the entity from the Magento system to the external system
     *
     * @param array $arguments_array
     * @return \Dunagan\ProcessQueue\Model\Task\ResultInterface
     */
    public function transmitEntityToExternalSystem($arguments_array);
}
