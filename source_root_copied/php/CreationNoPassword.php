<?php
/**
 * Author: Sean Dunagan (github: dunagan5887)
 * Date: 9/14/16
 */

namespace Scc\Campaign\Observer\Queue\Account;

use Magento\Framework\Event\ObserverInterface;

/**
 * Observer class to queue up a transmission of the customer account creation to the Campaign system
 *
 * Class CreationNoPassword
 * @package Scc\Campaign\Observer\Queue\Account
 */
class CreationNoPassword extends \Scc\Campaign\Observer\Queue\Email\AbstractTransmission implements ObserverInterface
{
    const ERROR_NO_CUSTOMER_OBJECT_ON_OBSERVER = 'The template_vars array provided by the event observer object for a call to %1::%2() did not contain a loaded customer object in the "customer" index of the template_vars data array. Provided keys in the array were: %3';
    const ERROR_INVALID_RP_TOKEN = 'No rp token was provided for customer %1 in an event observer call to %2::%3. Only provided data keys were: %4';

    /**
     * We want transmission tasks for the Account Creation email to be executed immediately, not on the next crontab
     *  iteration
     *
     * @var bool
     */
    protected $_should_execute_task_upon_creation = true;

    /**
     * @var \Scc\Campaign\Helper\Flags
     */
    protected $_flagsHelper;

    /**
     * THIS METHOD EXPECTS THE CALLING BLOCK TO HANDLE RELATED EXCEPTIONS
     *
     * We need to flag whether this customer account creation is the result of an admin creating an account in the
     *  admin panel
     *
     * @param \Magento\Framework\Event\Observer $observer
     * @return array
     * @throws \Scc\Campaign\Model\Exception\Task\QueueingException
     */
    protected function _prepareAdditionalArgumentsArray(\Magento\Framework\Event\Observer $observer)
    {
        $admin_is_saving_customer_data = $this->_flagsHelper->getAdminIsSavingCustomerData();
        $additional_arguments_array
            = array('scc_campaign_admin_is_saving_customer_data' => $admin_is_saving_customer_data);

        $template_vars_array = $observer->getData('template_vars');
        // The existence of $template_vars_array will already have been validated by this point
        $customerObject = isset($template_vars_array['customer'])
                            ? $template_vars_array['customer'] : null;
        /* @var \Magento\Customer\Model\Data\CustomerSecure $customerObject */
        if ((!is_object($customerObject)) || (!$customerObject->getId()))
        {
            // Throw an exception so that the native Magento email will be sent
            $template_vars_data_array_keys = array_keys($template_vars_array);
            $imploded_template_vars_data_array_keys = implode(', ', $template_vars_data_array_keys);
            $error_message = __(self::ERROR_NO_CUSTOMER_OBJECT_ON_OBSERVER, get_class($this), __METHOD__,
                                $imploded_template_vars_data_array_keys);
            throw new \Scc\Campaign\Model\Exception\Task\QueueingException($error_message);
        }
        // Validate the existence of the rp token
        $rp_token = $customerObject->getRpToken();
        if (empty($rp_token))
        {
            $customer_data_array = $customerObject->getData();
            $customer_data_array_keys = array_keys($customer_data_array);
            $imploded_customer_data_array_keys = implode(', ', $customer_data_array_keys);
            $error_message = __(self::ERROR_INVALID_RP_TOKEN, $customerObject->getId(), get_class($this),
                                __METHOD__, $imploded_customer_data_array_keys);
            throw new \Scc\Campaign\Model\Exception\Task\QueueingException($error_message);
        }

        $expected_customer_rp_token_argument = $this->_getApiTransmissionQueueTask()->getExpectedCustomerRpTokenArgument();
        $additional_arguments_array[$expected_customer_rp_token_argument] = $rp_token;

        return $additional_arguments_array;
    }

    /**
     * @inheritdoc
     *
     * @return \Scc\Campaign\Model\Task\Queue\Account\CreationNoPasswordInterface
     */
    protected function _getApiTransmissionQueueTask()
    {
        return $this->_objectManager->get('\Scc\Campaign\Model\Task\Queue\Account\CreationNoPasswordInterface');
    }

    /**
     * Creation constructor.
     * @param \Scc\Campaign\Model\LoggerInterface $logger
     * @param \Magento\Framework\ObjectManagerInterface $objectManager
     * @param \Scc\Campaign\Helper\Flags $flagsHelper
     */
    public function __construct(\Scc\Campaign\Model\LoggerInterface $logger,
                                \Magento\Framework\ObjectManagerInterface $objectManager,
                                \Scc\Campaign\Helper\Flags $flagsHelper)
    {
        parent::__construct($logger, $objectManager);
        $this->_flagsHelper = $flagsHelper;
    }
}
