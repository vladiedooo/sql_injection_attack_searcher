<?php
/**
 * Author: Sean Dunagan (github: dunagan5887)
 * Date: 4/30/17
 */

namespace Scc\Campaign\Model\Task\Queue\Account;

/**
 * Interface UpdateInterface
 * @package Scc\Campaign\Model\Task\Queue\Account
 */
interface UpdateInterface extends \Dunagan\WebApi\Model\Task\Api\TransmissionTaskInterface
{
    
}
