<?php
/**
 * Author: Sean Dunagan (github: dunagan5887)
 * Date: 1/27/17
 */

namespace Scc\AEMCatalog\Model\Api\Service\Product\Media;

/**
 * Class AbstractGallery
 * @package Scc\AEMCatalog\Helper\Renderer\Api
 */
class Gallery
{
    const AEM_COMMUNICATION_FAILURE_TYPE = 'Catalog Gallery';

    /**
     * @var \Scc\AEMCatalog\Model\Product\Media\Config
     */
    protected $_aemProductMediaConfig;

    /**
     * @var \Scc\AEMCatalog\Helper\Url
     */
    protected $_aemCatalogUrlHelper;

    /**
     * @var \Scc\AEMCatalog\Model\Client\Curl
     */
    protected $_sccWebApiCurlClient;

    /**
     * @var null|\Scc\AEMWebApi\Model\ResultInterface
     */
    protected $_combinedMediaGalleryApiResultObject = null;

    /**
     * Makes an API call to AEM for the combined media gallery json data for this product
     *
     * This call is invoked multiple times during a product detail page page load, so cache it to prevent erroneous
     *  API calls from being made
     *
     * @param \Magento\Catalog\Model\Product $product
     *
     * @return \Scc\AEMWebApi\Model\ResultInterface
     */
    public function getCombinedMediaGalleryApiResultObject($product)
    {
        if (is_null($this->_combinedMediaGalleryApiResultObject))
        {
            $product_gallery_combined_media_json_api_url = $this->_getCombinedMediaGalleryJsonApiUrlForProduct($product);
            $parameters_array = ['url' => $product_gallery_combined_media_json_api_url];

            $apiCallResultObject = $this->_sccWebApiCurlClient->callApiMethod("GET", $parameters_array);
            /* @var \Scc\AEMWebApi\Model\ResultInterface $apiCallResultObject */
            $this->_combinedMediaGalleryApiResultObject = $apiCallResultObject;
        }
    
        return $this->_combinedMediaGalleryApiResultObject;
    }
    
    /**
     * @param \Magento\Catalog\Model\Product\Interceptor $product
     * @return string
     */
    protected function _getCombinedMediaGalleryJsonApiUrlForProduct($product)
    {
        $gallery_json_api_url_template
            = $this->_aemProductMediaConfig->getAEMProductCombinedMediaGalleryJsonApiUrlTemplate();
        $gallery_json_url
            = $this->_aemCatalogUrlHelper->replaceTemplatedUrlValuesForProductUrl($gallery_json_api_url_template, $product);
        return $gallery_json_url;
    }

    /**
     * AbstractGallery constructor.
     * @param \Scc\AEMCatalog\Model\Product\Media\Config $aemProductMediaConfig
     * @param \Scc\AEMCatalog\Helper\Url $aemCatalogUrlHelper
     * @param \Scc\AEMCatalog\Model\Client\Curl $sccWebApiCurlClient
     */
    public function __construct(\Scc\AEMCatalog\Model\Product\Media\Config $aemProductMediaConfig,
                                \Scc\AEMCatalog\Helper\Url $aemCatalogUrlHelper,
                                \Scc\AEMCatalog\Model\Client\Curl $sccWebApiCurlClient)
    {
        $this->_aemProductMediaConfig = $aemProductMediaConfig;
        $this->_aemCatalogUrlHelper = $aemCatalogUrlHelper;
        $this->_sccWebApiCurlClient = $sccWebApiCurlClient;
    }
}
