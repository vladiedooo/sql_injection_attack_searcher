<?php
/**
 * Author: Sean Dunagan (github: dunagan5887)
 * Date: 10/26/16
 */

namespace Dunagan\ProcessQueue\Helper;

/**
 * Interface TaskArchiverInterface
 * @package Dunagan\ProcessQueue\Helper
 */
interface TaskArchiverInterface
{
    /**
     * Will archive all completed tasks
     *
     * @return void
     */
    public function archiveAllCompletedTasks();

    /**
     * Archives the tasks in the collection
     *
     * @param \Dunagan\ProcessQueue\Model\ResourceModel\Task\Collection $taskCollectionToArchive
     * @return mixed
     */
    public function archiveTaskCollection(\Dunagan\ProcessQueue\Model\ResourceModel\Task\Collection $taskCollectionToArchive);
}
