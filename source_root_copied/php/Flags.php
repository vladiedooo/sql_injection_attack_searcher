<?php
/**
 * Author: Sean Dunagan (github: dunagan5887)
 * Date: 10/4/16
 */

namespace Scc\Campaign\Helper;

/**
 * Class Flags
 * @package Scc\Campaign\Helper
 */
class Flags
{
    /**
     * @var bool
     */
    protected $_do_not_queue_address_update_transmission = false;

    /**
     * @var bool
     */
    protected $_do_not_queue_account_update_transmission = false;

    /**
     * A flag denoting whether an admin is saving customer data
     *
     * @var bool
     */
    protected $_admin_is_saving_customer_data = false;

    /**
     * @return bool
     */
    public function getDoNotQueueAddressUpdateTransmission()
    {
        return $this->_do_not_queue_address_update_transmission;
    }

    /**
     * @param bool $do_not_queue_update_address_transmission
     * @return $this
     */
    public function setDoNotQueueAddressUpdateTransmission($do_not_queue_update_address_transmission)
    {
        $this->_do_not_queue_address_update_transmission = $do_not_queue_update_address_transmission;
        return $this;
    }

    /**
     * @return bool
     */
    public function getDoNotQueueAccountUpdateTransmission()
    {
        return $this->_do_not_queue_account_update_transmission;
    }

    /**
     * @param bool $do_not_queue_account_update_transmission
     * @return $this
     */
    public function setDoNotQueueAccountUpdateTransmission($do_not_queue_account_update_transmission)
    {
        $this->_do_not_queue_account_update_transmission = $do_not_queue_account_update_transmission;
        return $this;
    }

    /**
     * Accessor for the flag indicating if an admin is saving customer data
     *
     * @return bool
     */
    public function getAdminIsSavingCustomerData()
    {
        return $this->_admin_is_saving_customer_data;
    }

    /**
     * Mutator for the flag indicating if an admin is saving customer data
     *
     * @param bool $admin_is_saving_customer_data
     * @return $this
     */
    public function setAdminIsSavingCustomerData($admin_is_saving_customer_data)
    {
        $this->_admin_is_saving_customer_data = (bool)$admin_is_saving_customer_data;
        return $this;
    }
}
