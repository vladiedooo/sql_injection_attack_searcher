<?php
/**
 * Author: Sean Dunagan (github: dunagan5887)
 * Date: 2/27/17
 */

namespace Scc\PremiumMembership\Observer\Quote;

use Magento\Framework\Event\ObserverInterface;

/**
 * Class Item
 * @package Scc\PremiumMembership\Observer\Quote\Add
 */
class CheckQuoteAddItemObserver implements ObserverInterface
{
    const EXCEPTION_CUSTOMER_IS_ALREADY_PREMIUM_MEMBER = 'Customer with email %1 is already a Premium Member.';
    const EXCEPTION_CHECK_ADD_ITEM_TO_CART = 'An exception occurred while attempting to check if an item being added to the cart is the premium membership product for customer %1: %2';

    /**
     * @var \Magento\Customer\Model\Session
     */
    protected $_customerSession;

    /**
     * @var \Scc\PremiumMembership\Helper\Product
     */
    protected $_sccPremiumMembershipProductHelper;

    /**
     * @var \Scc\PremiumMembership\Helper\Customer
     */
    protected $_sccPremiumMembershipCustomerHelper;

    /**
     * @var \Magento\Framework\Message\ManagerInterface
     */
    protected $_messageManager;

    /**
     * @var \Psr\Log\LoggerInterface
     */
    protected $_logger;

    /**
     * Throws an exception if the user is a logged-in customer who is already in the Premium Member customer segment
     *
     * @param \Magento\Framework\Event\Observer $observer
     * @return $this
     * @throws \Exception
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        try
        {
            // Check to ensure the customer is a logged in customer and is a Premium Customer
            if (!$this->_sccPremiumMembershipCustomerHelper->isSessionCustomerPremium())
            {
                // If not, return
                return;
            }

            // Check if the item being added is the premium membership product
            $item = $observer->getQuoteItem();
            /* @var \Magento\Quote\Model\Quote\Item $item */
            $sku = $item->getSku();
            if (!$this->_sccPremiumMembershipProductHelper->isPremiumMembershipSku($sku))
            {
                return;
            }
        }
        catch(\Exception $e)
        {
            $customer_id = ($this->_customerSession->isLoggedIn())
                            ? $this->_customerSession->getCustomer()->getId() : 'guest';
            $error_message = __(self::EXCEPTION_CHECK_ADD_ITEM_TO_CART, $customer_id, $e->getMessage());
            $this->_logger->error($error_message);
            return;
        }

        // The customer is already a Premium Member, and is trying to purchase the membership again. Throw an exception
        $customer_email = $this->_customerSession->getCustomer()->getEmail();
        $error_message = __(self::EXCEPTION_CUSTOMER_IS_ALREADY_PREMIUM_MEMBER, $customer_email);
        $this->_messageManager->addErrorMessage($error_message);
        throw new \Exception($error_message);
    }

    /**
     * CheckQuoteAddItemObserver constructor.
     * @param \Magento\Customer\Model\Session $customerSession
     * @param \Scc\PremiumMembership\Helper\Product $sccPremiumMembershipProductHelper
     * @param \Scc\PremiumMembership\Helper\Customer $sccPremiumMembershipCustomerHelper
     * @param \Magento\Framework\Message\ManagerInterface $messageManager
     * @param \Psr\Log\LoggerInterface $logger
     */
    public function __construct(\Magento\Customer\Model\Session $customerSession,
                                \Scc\PremiumMembership\Helper\Product $sccPremiumMembershipProductHelper,
                                \Scc\PremiumMembership\Helper\Customer $sccPremiumMembershipCustomerHelper,
                                \Magento\Framework\Message\ManagerInterface $messageManager,
                                \Psr\Log\LoggerInterface $logger)
    {
        $this->_customerSession = $customerSession;
        $this->_sccPremiumMembershipProductHelper = $sccPremiumMembershipProductHelper;
        $this->_sccPremiumMembershipCustomerHelper = $sccPremiumMembershipCustomerHelper;
        $this->_messageManager = $messageManager;
        $this->_logger = $logger;
    }
}
