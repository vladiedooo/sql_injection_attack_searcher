<?php
/**
 * Author: Sean Dunagan (github: dunagan5887)
 * Date: 1/30/17
 */

namespace Scc\AEMProductVideo\Plugin\Block\Product\View;

/**
 * Class GalleryPlugin
 * @package Scc\AEMCatalog\Plugin\ProductVideo\Block\Product\View
 */
class GalleryPlugin extends \Magento\ProductVideo\Block\Product\View\Gallery
{
    /**
     * @var \Scc\AEMProductVideo\Helper\Renderer\Api\Gallery\Videos
     */
    protected $_aemProductVideoGalleryJsonRendererApiHelper;

    /**
     * This plugin will make an API call to the AEM system for a json representing the gallery videos to be
     *  displayed for the product.
     * If no value is returned from the API call, the system will default to native Magento functionality
     *
     * @param \Magento\ProductVideo\Block\Product\View\Gallery\Interceptor $interceptor
     * @param \Closure $proceed
     * @return string
     */
    public function aroundGetMediaGalleryDataJson($interceptor, $proceed)
    {
        $product = $this->getProduct();
        $aem_gallery_videos_json = $this->_aemProductVideoGalleryJsonRendererApiHelper->getGalleryJsonForProduct($product);
        if (empty($aem_gallery_videos_json))
        {
            // If no json was returned from the AEM system, default to native Magento functionality
            return $this->getBackupDefaultMediaGalleryDataJson();
        }
        // Return the gallery videos json returned from the AEM system
        return $aem_gallery_videos_json;
    }

    /**
     * Retrieve media gallery data in JSON format
     *
     * @return string
     */
    public function getBackupDefaultMediaGalleryDataJson()
    {
        $mediaGalleryData = [];
        $mediaGalleryData[] = [
            'mediaType' => 'image',
            'videoUrl' => null,
            'isBase' => true,
        ];
        return $this->jsonEncoder->encode($mediaGalleryData);
    }

    /**
     * @param \Magento\Catalog\Block\Product\Context $context
     * @param \Magento\Framework\Stdlib\ArrayUtils $arrayUtils
     * @param \Magento\ProductVideo\Helper\Media $mediaHelper
     * @param \Magento\Framework\Json\EncoderInterface $jsonEncoder
     * @param \Scc\AEMProductVideo\Helper\Renderer\Api\Gallery\Videos $aemProductVideoGalleryJsonRendererApiHelper
     * @param array $data
     */
    public function __construct(
        \Magento\Catalog\Block\Product\Context $context,
        \Magento\Framework\Stdlib\ArrayUtils $arrayUtils,
        \Magento\Framework\Json\EncoderInterface $jsonEncoder,
        \Magento\ProductVideo\Helper\Media $mediaHelper,
        \Scc\AEMProductVideo\Helper\Renderer\Api\Gallery\Videos $aemProductVideoGalleryJsonRendererApiHelper,
        array $data = []
    ) {
        parent::__construct(
            $context,
            $arrayUtils,
            $jsonEncoder,
            $mediaHelper,
            $data
        );

        $this->_aemProductVideoGalleryJsonRendererApiHelper = $aemProductVideoGalleryJsonRendererApiHelper;
    }
}
