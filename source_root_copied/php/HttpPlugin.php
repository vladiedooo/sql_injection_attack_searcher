<?php
/**
 * Author: Sean Dunagan (github: dunagan5887)
 * Date: 1/19/17
 */

namespace Scc\AEMCookie\Plugin\Model\App\Response;

use Magento\Framework\App\Http\Context;

/**
 * Class HttpPlugin
 * @package Scc\AEMCookie\Plugin\Model\App\Response
 */
class HttpPlugin
{
    /**
     * @var \Magento\Customer\Model\Session
     */
    protected $_customerSession;

    /**
     * @var \Scc\AEMCookie\Cookie\Customer
     */
    protected $_aemMagentoSharedCustomerCookie;

    /**
     * @var Context
     */
    protected $_context;

    /**
     * Set the customer session data in the shared AEM-Magento COOKIE if a customer is logged in
     *
     * @param \Magento\Framework\App\Response\Http\Interceptor $interceptor
     * @param \Closure $proceed
     */
    public function aroundSendVary($interceptor, $proceed)
    {
        $varyString = $this->_context->getVaryString();
        if ($varyString)
        {
            $customer = $this->_customerSession->getCustomer();
            $this->_aemMagentoSharedCustomerCookie->set($customer);
        } elseif ($this->_aemMagentoSharedCustomerCookie->get())
        {
            $this->_aemMagentoSharedCustomerCookie->delete();
        }

        return $proceed();
    }

    /**
     * HttpPlugin constructor.
     * @param \Magento\Customer\Model\Session $customerSession
     * @param \Scc\AEMCookie\Cookie\Customer $aemMagentoSharedCustomerCookie
     * @param Context $context
     */
    public function __construct(\Magento\Customer\Model\Session $customerSession,
                                \Scc\AEMCookie\Cookie\Customer $aemMagentoSharedCustomerCookie,
                                Context $context)
    {
        $this->_customerSession = $customerSession;
        $this->_aemMagentoSharedCustomerCookie = $aemMagentoSharedCustomerCookie;
        $this->_context = $context;
    }
}
