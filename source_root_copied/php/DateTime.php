<?php
/**
 * Author: Sean Dunagan (https://github.com/dunagan5887)
 * Created: 4/22/16
 */

namespace Dunagan\Base\Block\Adminhtml\Widget\Grid\Column\Renderer;

/**
 * Author: Sean Dunagan (https://github.com/dunagan5887)
 * Created: 4/22/16
 *
 * Class Datetime
 * @package Dunagan\Base\Block\Adminhtml\Widget\Grid\Column\Renderer
 *
 * This is a grid column renderer block for datetime fields which will display an empty string if a datetime is
 *  '0000-00-00 00:00:00'
 */
class Datetime extends \Magento\Backend\Block\Widget\Grid\Column\Renderer\Datetime
{
    /**
     * If the datetime is '0000-00-00 00:00:00', return an empty string
     *
     * @param   \Magento\Framework\DataObject $row
     * @return  string
     */
    public function render(\Magento\Framework\DataObject $row)
    {
        $datetime = $this->_getValue($row);
        if (!strcmp('0000-00-00 00:00:00', $datetime))
        {
            // In this event do not show any date
            return '';
        }

        return parent::render($row);
    }
}
