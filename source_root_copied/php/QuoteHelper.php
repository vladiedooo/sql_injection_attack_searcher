<?php
/**
 * Author: Sean Dunagan (github: dunagan5887)
 * Date: 12/8/16
 */

namespace Scc\Salesrule\Helper;

/**
 * Class QuoteHelper
 * @package Scc\Salesrule\Helper
 */
class QuoteHelper
{
    /**
     * @var array
     */
    protected $_highest_price_item_id_by_quote_id = array();

    /**
     * @var array
     */
    protected $highest_price_item_eligible_for_salesrule = array();

    /**
     * @param \Magento\Quote\Model\Quote    $quote
     * @param \Magento\Salesrule\Model\Rule $salesrule
     *
     * @return int|null
     */
    public function getHighestPriceItemInQuoteEligibleForSalesrule(\Magento\Quote\Model\Quote $quote,
                                                                   \Magento\Salesrule\Model\Rule $salesrule)
    {
        $key = $quote->getId() . '_' . $salesrule->getId();

        if (!isset($this->highest_price_item_eligible_for_salesrule[$key]))
        {
            $eligible_item_ids = array();
            $quote_items_array = $quote->getItemsCollection()->getItems();
            foreach($quote_items_array as $quoteItem)
            {
                if ($salesrule->getActions()->validate($quoteItem))
                {
                    $eligible_item_ids[] = $quoteItem->getId();
                }
            }

            $highest_price_eligible_item_id = $this->_identifyHighestPriceItemInQuote($quote, $eligible_item_ids);

            $this->highest_price_item_eligible_for_salesrule[$key] = $highest_price_eligible_item_id;
        }

        return $this->highest_price_item_eligible_for_salesrule[$key];
    }

    /**
     * Will return the item id of the highest price item in the quote
     *
     * @param \Magento\Quote\Model\Quote $quote
     * @return int
     */
    public function getHighestPriceItemInQuote(\Magento\Quote\Model\Quote $quote)
    {
        $quote_id = $quote->getId();
        if (!isset($this->_highest_price_item_id_by_quote_id[$quote_id]))
        {
            $this->_highest_price_item_id_by_quote_id[$quote_id] = $this->_identifyHighestPriceItemInQuote($quote);
        }
    
        return $this->_highest_price_item_id_by_quote_id[$quote_id];
    }

    /**
     * There may be times where items are added to a cart during an execution thread, or where the price of an item
     *  may change for whatever reason. As a result, each time the Magento system decides to calculate the totals for
     *  a quote or an address, we should clear the cached result for the highest priced item in a quote
     *
     * @param \Magento\Quote\Model\Quote $quote
     */
    public function clearHighestPricedItemCacheForQuote(\Magento\Quote\Model\Quote $quote)
    {
        $quote_id = $quote->getId();
        unset($this->_highest_price_item_id_by_quote_id[$quote_id]);
    }

    /**
     * Will return the id of the item in the quote which has the highest price
     *
     * @param \Magento\Quote\Model\Quote $quote
     * @param array $eligible_item_ids
     * @return int
     */
    protected function _identifyHighestPriceItemInQuote(\Magento\Quote\Model\Quote $quote, $eligible_item_ids = array())
    {
        $quote_items_array = $quote->getItemsCollection()->getItems();
        $highest_price = -1000000;
        $highest_price_item_id = null;
        foreach($quote_items_array as $quoteItem)
        {
            /* @var \Magento\Quote\Model\Quote\Item $quoteItem */
            // For configurable and bundled products, we want to take the price listed on the parent items, not the children
            $parent_item_id = $quoteItem->getParentItemId();
            if (!empty($parent_item_id))
            {
                // Do not inspect the prices of child items
                continue;
            }

            if (!empty($eligible_item_ids))
            {
                $quote_item_id = $quoteItem->getId();
                if (!in_array($quote_item_id, $eligible_item_ids))
                {
                    // Most likely this item is not eligible for a sales rule, ignore it
                    continue;
                }
            }

            $quote_item_price = $quoteItem->getPrice();
            if ($quote_item_price > $highest_price)
            {
                $highest_price = $quote_item_price;
                $highest_price_item_id = $quoteItem->getId();
            }
        }

        return $highest_price_item_id;
    }
}
