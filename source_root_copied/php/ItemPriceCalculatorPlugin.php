<?php
/**
 * Author: Sean Dunagan (github: dunagan5887)
 * Date: 4/24/17
 */

namespace Ideal\OfflineShipping\Plugin\Model\Carrier\Flatrate;

/**
 * Class ItemPriceCalculatorPlugin
 *
 * @package Ideal\OfflineShipping\Plugin\Model\Carrier\Flatrate
 */
class ItemPriceCalculatorPlugin
{
    /**
     * @var \Ideal\OfflineShipping\Helper\Flatrate\Pricing
     */
    protected $_idealFlatratePricingHelper;

    /**
     * @var \Magento\Framework\ObjectManagerInterface
     */
    protected $_objectManager;

    /**
     * Check to see if there is a special flatrate shipping price configured for this customer's group
     *
     * @param \Magento\OfflineShipping\Model\Carrier\Flatrate\ItemPriceCalculator\Interceptor   $interceptor
     * @param \Magento\Quote\Model\Quote\Address\RateRequest                                    $request
     * @param float                                                                             $basePrice
     * @param int                                                                               $freeBoxes
     *
     * @return array|float
     */
    public function beforeGetShippingPricePerOrder($interceptor,
                                                   \Magento\Quote\Model\Quote\Address\RateRequest $request,
                                                   $basePrice,
                                                   $freeBoxes)
    {
        // Need to determine whether we should modify the $basePrice parameter
        // We need to get the customer who is placing this order
        // First, we need to get the quote
        $request_items = $request->getAllItems();
        $firstItem = reset($request_items);
        if ((!is_object($firstItem)) || (!$firstItem->getId()))
        {
            // This should never happen, but account for the case where it does
            return false; // Returning false denotes that we don't want to modify any parameters
        }
        /* @var \Magento\Quote\Model\Quote\Item $firstItem */
        $quote = $firstItem->getQuote();
        $customer = $quote->getCustomer();
        $customer_group_id = $customer->getGroupId();
        $customerGroup = $this->_objectManager->create('Magento\Customer\Model\Group')->load($customer_group_id);
        if ((!is_object($customerGroup)) || (!$customerGroup->getId()))
        {
            // This should never happen, but account for the case where it does
            return false; // Returning false denotes that we don't want to modify any parameters
        }
        /* @var \Magento\Customer\Model\Group $customerGroup */
        $customer_group_code = $customerGroup->getCode();
        // See if there is a special price configured for this customer group
        $customer_group_price = $this->_idealFlatratePricingHelper
                                     ->getSpecialShippingPriceByWebsiteAndCustomerGroup($customer_group_code);
        if (!is_null($customer_group_price))
        {
            return [$request, $customer_group_price, $freeBoxes];
        }
        // There is no special pricing set for this customer group, return false to denote that we don't want to modify
        //      the arguments to the method
        return false;
    }
    
    /**
     * ItemPriceCalculatorPlugin constructor.
     * @param \Ideal\OfflineShipping\Helper\Flatrate\Pricing $idealFlatratePricingHelper
     * @param \Magento\Framework\ObjectManagerInterface $objectManager
     */
    public function __construct(\Ideal\OfflineShipping\Helper\Flatrate\Pricing $idealFlatratePricingHelper,
                                \Magento\Framework\ObjectManagerInterface $objectManager)
    {
        $this->_idealFlatratePricingHelper = $idealFlatratePricingHelper;
        $this->_objectManager = $objectManager;
    }
}
