<?php
/**
 * Author: Sean Dunagan (github: dunagan5887)
 * Date: 10/26/16
 */

namespace Dunagan\ProcessQueue\Model\CronJob;

/**
 * Class ArchiveTasks
 * @package Dunagan\ProcessQueue\Model\CronJob
 */
class ArchiveTasks
{
    const EXCEPTION_EXECUTING_TASK_ARCHIVAL = 'An uncaught exception occurred while attempting to archive ProcessQueue tasks: %1';

    /**
     * @var \Dunagan\ProcessQueue\Model\Log
     */
    protected $_logger;

    /**
     * @var \Dunagan\ProcessQueue\Helper\TaskArchiverInterface
     */
    protected $_taskArchiverHelper;

    public function execute()
    {
        try
        {
            $this->_taskArchiverHelper->archiveAllCompletedTasks();
        }
        catch(\Exception $e)
        {
            $error_message = __(self::EXCEPTION_EXECUTING_TASK_ARCHIVAL, $e->getMessage());
            $this->_logger->error($error_message);
        }
    }

    /**
     * ArchiveTasks constructor.
     * @param \Dunagan\ProcessQueue\Model\Log $logger
     * @param \Dunagan\ProcessQueue\Helper\TaskArchiverInterface $taskArchiverHelper
     */
    public function __construct(\Dunagan\ProcessQueue\Model\Log $logger,
                                \Dunagan\ProcessQueue\Helper\TaskArchiverInterface $taskArchiverHelper)
    {
        $this->_logger = $logger;
        $this->_taskArchiverHelper = $taskArchiverHelper;
    }
}
