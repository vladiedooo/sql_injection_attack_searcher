/**
* Copyright 2016 aheadWorks. All rights reserved.
* See LICENSE.txt for license details.
*/

define([
    'jquery',
    'awLayeredNavFilterValue',
    'awLayeredNavFilterItemCurrent',
    'awLayeredNavUrl'
], function ($, filterValue, currentFilterItem, url) {
    'use strict';

    return {
        config: {
            mainColumn: 'div.column.main',
            navigation: '[data-role=filter-block]'
        },

        /**
         * Initialize
         *
         * @param {Object} config
         */
        init: function (config) {
            $.extend(this.config, config);
        },

        /**
         * Performs update blocks and window url
         *
         * @param {String} windowUrl
         * @param {Object} blocksHtml
         */
        update: function (windowUrl, blocksHtml) {
            if (blocksHtml) {
                var mainColumnHtml = blocksHtml.mainColumn,
                    navigationHtml = blocksHtml.navigation;

                var tempDiv = $('<div>').html(blocksHtml.navigation);
                $(tempDiv).find("script").remove();
                navigationHtml = $(tempDiv).html();

                $(this.config.mainColumn).replaceWith(mainColumnHtml);
                $(this.config.navigation).replaceWith(navigationHtml);
                filterValue.reset();
                currentFilterItem.reset();
                $(this.config.navigation).trigger('contentUpdated');
                $(this.config.mainColumn).trigger('contentUpdated');
                this._setCurrentUrl(windowUrl);
            }
        },

        /**
         * Set current url
         *
         * @param {String} windowUrl
         */
        _setCurrentUrl: function (windowUrl) {
            url.setCurrentUrl(windowUrl);
            if (typeof(window.history.pushState) == 'function') {
                window.history.pushState(null, url.getCurrentUrl(), url.getCurrentUrl());
            } else {
                window.location.hash = '#!' + url.getCurrentUrl();
            }
        }
    };
});
