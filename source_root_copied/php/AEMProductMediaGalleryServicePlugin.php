<?php
/**
 * Author: Sean Dunagan (github: dunagan5887)
 * Date: 3/7/17
 */

namespace Scc\AEMCacheManager\Plugin\AEMCatalog\Model\Api\Service\Product\Media;

/**
 * Class AEMProductMediaGalleryServicePlugin
 *
 * @package Scc\AEMCacheManager\Plugin\AEMCatalog\Model\Api\Service\Product\Media
 */
class AEMProductMediaGalleryServicePlugin
{
    /**
     * @var \Scc\AEMCacheManager\Helper\Flag
     */
    protected $_sccAemCacheManagerFlagHelperSingleton;

    /**
     * @param \Scc\AEMCatalog\Model\Api\Service\Product\Media\Gallery\Interceptor $interceptor
     * @param \Closure $proceed
     * @param \Magento\Catalog\Model\Product $product
     *
     * @return \Scc\AEMWebApi\Model\ResultInterface
     */
    public function aroundGetCombinedMediaGalleryApiResultObject($interceptor, $proceed, $product)
    {
        $resultObject = $proceed($product);
        /* @var \Scc\AEMWebApi\Model\ResultInterface $resultObject */
        if (!$resultObject->getWasSuccessful())
        {
            if ($this->_sccAemCacheManagerFlagHelperSingleton->isCachePreventionDueToGalleryAPIFailureEnabled())
            {
                $this->_sccAemCacheManagerFlagHelperSingleton->flagPageNotToBeCached();
            }
        }

        return $resultObject;
    }

    /**
     * AEMProductMediaGalleryServicePlugin constructor.
     *
     * @param \Scc\AEMCacheManager\Helper\Flag $sccAemCacheManagerFlagHelperSingleton
     */
    public function __construct(\Scc\AEMCacheManager\Helper\Flag $sccAemCacheManagerFlagHelperSingleton)
    {
        $this->_sccAemCacheManagerFlagHelperSingleton = $sccAemCacheManagerFlagHelperSingleton;
    }
}
