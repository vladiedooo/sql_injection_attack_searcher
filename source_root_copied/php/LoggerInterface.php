<?php
/**
 * Author: Sean Dunagan (github: dunagan5887)
 * Date: 9/18/16
 */

namespace Scc\WebApi\Model;

/**
 * Interface LoggerInterface
 * @package Scc\WebApi\Model
 */
interface LoggerInterface extends \Psr\Log\LoggerInterface
{
    /**
     * Method which should log a message related to an entity definition error on an API transmission queue task
     *
     * @param string $error_message
     */
    public function logTransmissionTaskEntityDefinitionError($error_message);

    /**
     * @param string $error_message
     */
    public function logTransmissionExecutionError($error_message);

    /**
     * @param string $error_message
     */
    public function logEntityTransmissionQueueingError($error_message);
}
