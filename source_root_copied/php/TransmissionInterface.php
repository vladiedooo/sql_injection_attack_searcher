<?php
/**
 * Author: Sean Dunagan (github: dunagan5887)
 * Date: 10/6/16
 */

namespace Scc\Campaign\Model\Task\Queue\Email;

/**
 * Interface TransmissionInterface
 * @package Scc\Campaign\Task\Queue\Account\Email
 */
interface TransmissionInterface extends \Dunagan\WebApi\Model\Task\Api\TransmissionTaskInterface
{
    /**
     * @param $customerObject
     * @param string $template_id
     * @param string|null $store_id
     * @param array $additional_arguments_array
     * @param bool $queue_in_processing_state
     * @return \Dunagan\ProcessQueue\Model\TaskInterface
     */
    public function queueEmailTransmission($customerObject, $template_id, $store_id,
                                           array $additional_arguments_array = array(), $queue_in_processing_state);
}
