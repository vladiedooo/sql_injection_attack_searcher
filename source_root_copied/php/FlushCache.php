<?php
/**
 * Author: Sean Dunagan (github: dunagan5887)
 * Date: 1/26/17
 */

namespace Scc\AEMCacheManager\Model;

use Scc\AEMCacheManager\Api\FlushCacheInterface;

/**
 * Class FlushCache
 * @package Scc\AEMCacheManager\Model
 */
class FlushCache implements FlushCacheInterface
{
    /**
     * @var \Magento\Framework\App\Cache\Manager
     */
    protected $_cacheManager;

    /**
     * {@inheritdoc}
     */
    public function flushFullPageAndBlockHtmlCache()
    {
        $cache_types_to_flush = array('block_html', 'full_page');
        $this->_cacheManager->flush($cache_types_to_flush);
        return true;
    }

    /**
     * FlushCache constructor.
     * @param \Magento\Framework\App\Cache\Manager $cacheManager
     */
    public function __construct(\Magento\Framework\App\Cache\Manager $cacheManager)
    {
        $this->_cacheManager = $cacheManager;
    }
}
