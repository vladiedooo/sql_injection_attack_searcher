define([
    'uiComponent'

], function (Component) {
    'use strict';

    return Component.extend({
        defaults: {
            template: 'Ideal_DTMEvents/checkout/billing/dtm-tag'
        }
    });
});