<?php
/**
 * Author: Sean Dunagan (github: dunagan5887)
 * Date: 3/2/17
 */

namespace Scc\AEMTheme\Model\Api;

use \Scc\AEMTheme\Api\BlockHtmlApiServiceInterface;

/**
 * Class BlockHtmlApiService
 * @package Scc\AEMTheme\Model\Api
 */
class BlockHtmlApiService implements BlockHtmlApiServiceInterface
{
    const EXCEPTION_SETTING_AEM_BLOCK_HTML_IN_CACHE = 'An exception occurred while attempting to set the AEM block html in the local database cache for block %1 for store code %2, customer group id %3 and website code %4: %5';
    const EXCEPTION_FLUSHING_DATABASE_CACHE = 'An exception occurred while attempting to flush the AEM html database cache for website `%1` and language code `%2`: %3';
    const EXCEPTION_FLUSHING_MAGENTO_CACHE = 'An exception occurred while attempting to flush the magento %1 cache types in response to an AEM API flush html call for website `%2` and language code `%3`: %4';

    /**
     * @var \Scc\AEMTheme\Api\BlockHtmlCacheInterface
     */
    protected $_aemThemeBlockHtmlCacheInterface;

    /**
     * @var \Scc\WebApi\Model\LoggerInterface
     */
    protected $_sccWebApiLogger;

    /**
     * @var \Magento\Framework\App\Cache\Manager
     */
    protected $_magentoAppCacheManager;

    /**
     * {@inheritdoc}
     */
    public function flushThemeBlockHtmlCache($website_code, $language_code, $block_name_to_html_mapping = array(),
                                             $customer_group_id = null)
    {
        $errors = array();

        try
        {
            // CURRENTLY: This will just truncate the local database cache table
            $this->_aemThemeBlockHtmlCacheInterface->flushHtmlCacheEntries($website_code, $language_code, null,
                                                                           $customer_group_id);
        }
        catch(\Exception $e)
        {
            $error_message = __(self::EXCEPTION_FLUSHING_DATABASE_CACHE, $website_code, $language_code, $e->getMessage());
            $this->_sccWebApiLogger->error($error_message);
            $errors[] = $error_message;
        }

        // CURRENTLY: Flush the entire Magento page and block html cache
        $cache_types_to_flush = array('full_page', 'block_html');

        try
        {
            $this->_magentoAppCacheManager->flush($cache_types_to_flush);
        }
        catch(\Exception $e)
        {
            $imploded_cache_types = implode(', ', $cache_types_to_flush);
            $error_message = __(self::EXCEPTION_FLUSHING_MAGENTO_CACHE, $imploded_cache_types, $website_code,
                                $language_code, $e->getMessage());
            $this->_sccWebApiLogger->error($error_message);
            $errors[] = $error_message;
        }

        $result_json_to_return = array();
        $result_json_to_return['result'] = empty($errors) ? true : false;
        $result_json_to_return['errors'] = $errors;

        return json_encode($result_json_to_return);
    }

    /**
     * BlockHtmlApiService constructor.
     * @param \Scc\AEMTheme\Api\BlockHtmlCacheInterface    $aemThemeBlockHtmlCacheInterface
     * @param \Magento\Framework\App\Cache\Manager         $magentoAppCacheManager
     * @param \Scc\WebApi\Model\LoggerInterface            $sccWebApiLogger
     */
    public function __construct(\Scc\AEMTheme\Api\BlockHtmlCacheInterface $aemThemeBlockHtmlCacheInterface,
                                \Magento\Framework\App\Cache\Manager $magentoAppCacheManager,
                                \Scc\WebApi\Model\LoggerInterface $sccWebApiLogger)
    {
        $this->_aemThemeBlockHtmlCacheInterface = $aemThemeBlockHtmlCacheInterface;
        $this->_sccWebApiLogger = $sccWebApiLogger;
        $this->_magentoAppCacheManager = $magentoAppCacheManager;
    }
}
