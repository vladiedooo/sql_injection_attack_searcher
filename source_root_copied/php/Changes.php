<?php
/**
 * Author: Sean Dunagan (github: dunagan5887)
 * Date: 9/28/16
 */

namespace Dunagan\Base\Helper\Data;

/**
 * Class Changes
 * @package Dunagan\Base\Helper\Data
 */
class Changes
{
    /**
     * @param \Magento\Framework\Model\AbstractModel $modelObject
     * @return bool
     */
    public function doesModelHaveDataChanges(\Magento\Framework\Model\AbstractModel $modelObject)
    {
        $updated_data_fields = $this->getDifferingDataFields($modelObject);
        return (!empty($updated_data_fields));
    }

    /**
     * @param \Magento\Framework\Model\AbstractModel $modelObject
     * @return array
     */
    public function getDifferingDataFields(\Magento\Framework\Model\AbstractModel $modelObject)
    {
        $orig_data_array = $modelObject->getOrigData();
        if (empty($orig_data_array))
        {
            // In this case, this is likely a customer which has just been created. As such, don't queue for transmission
            return null;
        }
        $current_data_array = $modelObject->getData();
        return $this->getDifferingFieldsInCurrentArray($current_data_array, $orig_data_array);
    }

    /**
     * @param array $current_array
     * @param array $original_array
     * @return array
     */
    public function getDifferingFieldsInCurrentArray(array $current_array, array $original_array)
    {
        $common_indexes = array_intersect_key($current_array, $original_array);
        $orig_data_common_fields_array = array_intersect_key($original_array, $common_indexes);
        $current_data_common_fields_array = array_intersect_key($current_array, $common_indexes);

        $differing_array_fields = array_diff_uassoc($current_data_common_fields_array,
                                                    $orig_data_common_fields_array,
                                                    function ($a, $b) { return ($a == $b) ? 0 : 1;}
                                    );

        return $differing_array_fields;
    }
}
