<?php
/**
 * Author: Sean Dunagan (github: dunagan5887)
 * Date: 2/23/17
 */

namespace Scc\PremiumMembership\Observer\CustomerSegment\CustomerGroupTransfer;

use Magento\Framework\Event\ObserverInterface;

/**
 * Class RemoveFromSegment
 * @package Scc\PremiumMembership\Observer\CustomerSegment\CustomerGroupTransfer
 */
class RemoveFromSegment extends CustomerSegmentChangeAbstract implements ObserverInterface
{
    /**
     * @return bool
     */
    public function isAddingToSegment()
    {
        return false;
    }
}
