<?php
/**
 * Author: Sean Dunagan (github: dunagan5887)
 * Date: 4/26/17
 */

namespace Scc\Campaign\Model\Exception\Api;

/**
 * Class ChangeMembershipException
 *
 * @package Scc\Campaign\Model\Exception\Api
 */
class ChangeMembershipException extends \Exception
{

}
