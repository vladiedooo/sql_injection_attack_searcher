<?php
/**
 * Author: Sean Dunagan (github: dunagan5887)
 * Date: 11/1/16
 */

namespace Dunagan\ProcessQueue\Model\Exception;

/**
 * Class Batch
 * @package Dunagan\ProcessQueue\Model\Exception
 */
class Batch extends \Exception
{

}
