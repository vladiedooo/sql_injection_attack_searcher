<?php
/**
 * Author: Sean Dunagan (github: dunagan5887)
 * Date: 3/7/17
 */

namespace Scc\AEMCacheManager\Plugin\AEMTheme\Helper\Renderer;

/**
 * Class AEMThemeRendererServicePlugin
 *
 * @package Scc\AEMCacheManager\Plugin\AEMTheme\Helper\Renderer
 */
class AEMThemeRendererServicePlugin
{
    /**
     * @var \Scc\AEMCacheManager\Helper\Flag
     */
    protected $_sccAemCacheManagerFlagHelperSingleton;

    /**
     * @param \Scc\AEMTheme\Helper\Renderer\Service\Interceptor $interceptor
     * @param \Closure $proceed
     * @param string $block_to_render
     * @param string $website_code
     * @param string $language_code
     * @param null $customer_group_id
     *
     * @return \Scc\AEMWebApi\Model\ResultInterface
     */
    public function aroundGetBlockHtmlViaApiCall($interceptor, $proceed, $block_to_render, $website_code,
                                                 $language_code, $customer_group_id = null)
    {
        $resultObject = $proceed($block_to_render, $website_code, $language_code, $customer_group_id);
        /* @var \Scc\AEMWebApi\Model\ResultInterface $resultObject */
        if (!$resultObject->getWasSuccessful())
        {
            // The call to AEM for the block was not successful. We need to flag the page not to be cached
            $this->_sccAemCacheManagerFlagHelperSingleton->flagPageNotToBeCached();
        }

        return $resultObject;
    }

    /**
     * ThemeRendererServicePlugin constructor.
     *
     * @param \Scc\AEMCacheManager\Helper\Flag $sccAemCacheManagerFlagHelperSingleton
     */
    public function __construct(\Scc\AEMCacheManager\Helper\Flag $sccAemCacheManagerFlagHelperSingleton)
    {
        $this->_sccAemCacheManagerFlagHelperSingleton = $sccAemCacheManagerFlagHelperSingleton;
    }
}
