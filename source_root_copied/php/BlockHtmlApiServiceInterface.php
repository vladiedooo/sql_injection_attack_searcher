<?php
/**
 * Author: Sean Dunagan (github: dunagan5887)
 * Date: 3/2/17
 */

namespace Scc\AEMTheme\Api;

/**
 * Interface BlockHtmlApiServiceInterface
 * @package Scc\AEMTheme\Api
 */
interface BlockHtmlApiServiceInterface
{
    /**
     * This is an API call intended to come from AEM
     * It tells Magento which website_code and language_code have had html blocks updated and that the cache will need to
     *      be flushed
     * CURRENTLY: This method will flush the Magento page and block_html cache in response. It will also flush all entries
     *              in the local AEM html database cache table
     * EVENTUALLY: This method will only flush page and block_html cache entries specific to the website code and language
     *              code passed in. Will also only flush database cache table entries specific to the website_code and
     *              language_code passed in. Will also allow for setting database cache table entries for the website
     *              and language passed in.
     *
     * @param string|null $website_code             - REQUIRED The website code whose content has been updated
     * @param string|null $language_code            - REQUIRED The language code for the locale whose content has been
     *                                                              updated
     * @param string[] $block_name_to_html_mapping  - OPTIONAL Allows for specifying the html to set in the cache for the
     *                                                          block. Will respect the $website_code and $language_code
     *                                                          provided above. (THIS IS NOT YET FUNCTIONAL)
     * @param int|null $customer_group_id           - OPTIONAL Allows for specifying the customer_group to whom this
     *                                                           html should be scoped (THIS IS NOT YET FUNCTIONAL)
     *
     * @return string - An encoded json containing the following fields:
     *                      'result' - true if there were no errors in the method execution, false otherwise
     *                      'errors' - An array. Will contain any errors which occurred; will be empty if none occurred
     */
    public function flushThemeBlockHtmlCache($website_code, $language_code, $block_name_to_html_mapping = array(),
                                             $customer_group_id = null);
}
