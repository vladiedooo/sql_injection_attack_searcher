<?php
/**
 * Author: Sean Dunagan (github: dunagan5887)
 * Date: 5/12/17
 */

namespace Scc\Analytics\Helper\Js\Tracking\Populator;

use Magento\Framework\Exception\NoSuchEntityException;

/**
 * Class CheckoutOnepageSuccessHelper
 * @package Scc\Analytics\Helper\Js\Tracking\Populator
 */
class CheckoutOnepageSuccessHelper
{
    const EXCEPTION_ORDER_DOES_NOT_EXIST = 'No order exists in the system with order increment id %1';

    const QTY_OF_ITEMS_TEMPLATE = '!Quantity!';
    const REVENUE_TEMPLATE = '!Revenue!';
    const PRODUCT_SKUS_TEMPLATE = '!ProductSkus!';
    const ORDER_INCREMENT_ID_TEMPLATE = '!OrderID!';

    /**
     * @var \Psr\Log\LoggerInterface
     */
    protected $_logger;

    /**
     * @var \Magento\Framework\ObjectManagerInterface
     */
    protected $_objectManager;

    /**
     * @param string $js_snippet
     * @param string $order_increment_id - Order Increment Id
     *
     * @return string
     */
    public function populateCheckoutOnepageSuccessJsSnippet($js_snippet, $order_increment_id)
    {
        try
        {
            $orderObject = $this->_objectManager->create('\Magento\Sales\Model\Order')
                                                ->loadByIncrementId($order_increment_id);
            /* @var \Magento\Sales\Model\Order $orderObject */

            if ((!is_object($orderObject)) || (!$orderObject->getId()))
            {
                $error_message = __(self::EXCEPTION_ORDER_DOES_NOT_EXIST, $order_increment_id);
                throw new NoSuchEntityException($error_message);
            }
        }
        catch(NoSuchEntityException $e)
        {
            $error_message = $e->getMessage();
            $this->_logger->error($error_message);
            // Return without modifying the snippet
            return $js_snippet;
        }

        $qty_of_items = $orderObject->getTotalQtyOrdered();
        $js_snippet = str_replace(self::QTY_OF_ITEMS_TEMPLATE, intval($qty_of_items), $js_snippet);

        $revenue = $orderObject->getGrandTotal();
        $js_snippet = str_replace(self::REVENUE_TEMPLATE, $revenue, $js_snippet);

        $items_in_order_array = $orderObject->getItems();

        $array_of_skus = array();
        foreach($items_in_order_array as $orderItem)
        {
            /* @var \Magento\Sales\Model\Order\Item $orderItem */
            $sku = $orderItem->getSku();
            $array_of_skus[] = $sku;
        }

        $items_sku_string = implode(',', $array_of_skus);
        $js_snippet = str_replace(self::PRODUCT_SKUS_TEMPLATE, urlencode($items_sku_string), $js_snippet);

        $js_snippet = str_replace(self::ORDER_INCREMENT_ID_TEMPLATE, $order_increment_id, $js_snippet);

        return $js_snippet;
    }

    /**
     * CheckoutOnepageSuccessHelper constructor.
     *
     * @param \Psr\Log\LoggerInterface                  $logger
     * @param \Magento\Framework\ObjectManagerInterface $objectManager
     */
    public function __construct(\Psr\Log\LoggerInterface $logger, \Magento\Framework\ObjectManagerInterface $objectManager)
    {
        $this->_logger = $logger;
        $this->_objectManager = $objectManager;
    }
}
