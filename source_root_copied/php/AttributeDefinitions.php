<?php

namespace Scc\Customers\Setup\Data;

/**
 * Class AttributeDefinitions
 * @package Ideal\Customers\Setup\Data
 */
class AttributeDefinitions
{
    /**
     * @var \Magento\Framework\ObjectManagerInterface
     */
    protected $_objectManager;

    /**
     * @return array
     */
    public function getCustomerAttributeDefinitionArray()
    {
        $attribute_data_array = array();

        $attribute_data_array['dob_month']
            = array_merge($this->_getCustomerAttributeDataTemplate(),
            array(
                'label' => 'Month of Birth',
                'type' => 'int',
                'input' => 'select',
                'required' => false,
                'visible' => false,
                'system' => false,
                'option' => $this->_objectManager->get('Scc\Customers\Setup\Data\Options\Month')->getOptionArray(),
                'sort_order' => 1000,
                'position' => 1000,
                'source'  => 'Magento\Eav\Model\Entity\Attribute\Source\Table'
            ));
        // We don't want the dob_month attribute to be include in the customer account edit forms
        $attribute_data_array['dob_month']['forms'] = array('customer_account_create','adminhtml_customer','checkout_register');

        $attribute_data_array['type_of_customer']
            = array_merge($this->_getCustomerAttributeDataTemplate(),
            array(
                'label' => 'Type',
                'type'  => 'int',
                'input' => 'select',
                'required' => true,
                'visible' => true,
                'system' => false,
                'option' => $this->_objectManager->get('Scc\Customers\Setup\Data\Options\Customer\Type')->getOptionArray(),
                'sort_order' => 1010,
                'position' => 1010,
                'source'  => 'Magento\Eav\Model\Entity\Attribute\Source\Table'
            ));

        $attribute_data_array['enthusiast_category']
            = array_merge($this->_getCustomerAttributeDataTemplate(),
            array(
                'label' => 'Category',
                'type' => 'int',
                'input' => 'select',
                'required' => false,
                'visible' => true,
                'system' => false,
                'option' => $this->_objectManager->get('Scc\Customers\Setup\Data\Options\Enthusiast\Type')->getOptionArray(),
                'sort_order' => 1020,
                'position' => 1020,
                'source'  => 'Magento\Eav\Model\Entity\Attribute\Source\Table'
            ));

        $attribute_data_array['student_school']
            = array_merge($this->_getCustomerAttributeDataTemplate(),
            array(
                'label' => 'School',
                'input' => 'text',
                'required' => false,
                'visible' => true,
                'system' => false,
                'sort_order' => 1030,
                'position' => 1030,
            ));

        $attribute_data_array['student_school_address_1']
            = array_merge($this->_getCustomerAttributeDataTemplate(),
            array(
                'label' => 'School Address 1',
                'input' => 'text',
                'required' => false,
                'visible' => true,
                'system' => false,
                'sort_order' => 1040,
                'position' => 1040,
            ));

        $attribute_data_array['student_school_address_2']
            = array_merge($this->_getCustomerAttributeDataTemplate(),
            array(
                'label' => 'School Address 2',
                'input' => 'text',
                'required' => false,
                'visible' => true,
                'system' => false,
                'sort_order' => 1050,
                'position' => 1050,
            ));

        $attribute_data_array['student_school_city']
            = array_merge($this->_getCustomerAttributeDataTemplate(),
            array(
                'label' => 'School City',
                'input' => 'text',
                'required' => false,
                'visible' => true,
                'system' => false,
                'sort_order' => 1060,
                'position' => 1060,
            ));

        $attribute_data_array['student_school_state']
            = array_merge($this->_getCustomerAttributeDataTemplate(),
            array(
                'label' => 'School State',
                'input' => 'text',
                'required' => false,
                'visible' => true,
                'system' => false,
                'sort_order' => 1070,
                'position' => 1070,
            ));

        $attribute_data_array['student_school_postal_code']
            = array_merge($this->_getCustomerAttributeDataTemplate(),
            array(
                'label' => 'School Zip',
                'input' => 'text',
                'required' => false,
                'visible' => true,
                'system' => false,
                'sort_order' => 1080,
                'position' => 1080,
            ));

        $attribute_data_array['student_school_instructor_name']
            = array_merge($this->_getCustomerAttributeDataTemplate(),
            array(
                'label' => 'Instructor Name',
                'input' => 'text',
                'required' => false,
                'visible' => true,
                'system' => false,
                'sort_order' => 1090,
                'position' => 1090,
            ));

        $attribute_data_array['student_school_instructor_email']
            = array_merge($this->_getCustomerAttributeDataTemplate(),
            array(
                'label' => 'Instructor Email',
                'input' => 'text',
                'validate_rules' => 'a:1:{s:16:"input_validation";s:5:"email";}',
                'required' => false,
                'visible' => true,
                'system' => false,
                'sort_order' => 1100,
                'position' => 1100,
            ));

        $attribute_data_array['student_school_instructor_telephone']
            = array_merge($this->_getCustomerAttributeDataTemplate(),
            array(
                'label' => 'School Telephone',
                'input' => 'text',
                'required' => false,
                'visible' => true,
                'system' => false,
                'sort_order' => 1110,
                'position' => 1110,
            ));

        $attribute_data_array['student_school_validate_correct']
            = array_merge($this->_getCustomerAttributeDataTemplate(),
            array(
                'label' => 'I certify all information is true and correct to the best of my knowledge.',
                'type' => 'int',
                'input' => 'boolean',
                'required' => false,
                'visible' => true,
                'system' => false,
                'sort_order' => 1120,
                'position' => 1120,
            ));

        $attribute_data_array['professional_industry']
            = array_merge($this->_getCustomerAttributeDataTemplate(),
            array(
                'label' => 'Industry',
                'type' => 'int',
                'input' => 'select',
                'required' => false,
                'visible' => true,
                'system' => false,
                'option' => $this->_objectManager->get('Scc\Customers\Setup\Data\Options\Professional\Industry')->getOptionArray(),
                'sort_order' => 1130,
                'position' => 1130,
                'source'  => 'Magento\Eav\Model\Entity\Attribute\Source\Table'
            ));

        $attribute_data_array['professional_industry_other']
            = array_merge($this->_getCustomerAttributeDataTemplate(),
            array(
                'label' => 'Other',
                'input' => 'text',
                'required' => false,
                'visible' => true,
                'system' => false,
                'note' => 'If the position isn\'t on the list, type in here',
                'sort_order' => 1140,
                'position' => 1140,
            ));
        return $attribute_data_array;
    }

    protected function _getCustomerAttributeDataTemplate()
    {
        return array(
            'type' => 'text',
            'forms' => array('customer_account_edit','customer_account_create','adminhtml_customer','checkout_register'),
            'global' => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_GLOBAL,
            'visible' => true,
            'required' => false,
            'is_user_defined' => true,
            'system' => 0,
            'unique' => false,
            'visible_on_front' => true,
            'source' => '',
            'frontend' => '',
            'note' => ''
        );
    }

    /**
     * AttributeDefinitions constructor.
     * @param \Magento\Framework\ObjectManagerInterface $objectManager
     */
    public function __construct(\Magento\Framework\ObjectManagerInterface $objectManager)
    {
        $this->_objectManager = $objectManager;
    }
}
