<?php
/**
 * Author: Sean Dunagan (github: dunagan5887)
 * Date: 1/26/17
 */

namespace Scc\AEMCacheManager\Api;

/**
 * Interface FlushCacheInterface
 * @package Scc\AEMCacheManager\Api
 */
interface FlushCacheInterface
{
    /**
     * Will flush the full page and block html cache
     *
     * @return void
     */
    public function flushFullPageAndBlockHtmlCache();
}
