<?php
/**
 * Author: Sean Dunagan (github: dunagan5887)
 * Date: 3/11/17
 */

namespace Scc\Analytics\Observer;

use Magento\Framework\Event\ObserverInterface;

/**
 * Class CartAddProductComplete
 * @package Scc\Analytics\Observer
 */
class CartAddProductComplete implements ObserverInterface
{
    /**
     * @var \Scc\Analytics\Helper\Js\TrackingJsHelper
     */
    protected $_sccAnalyticsTrackingJsHelper;

    /**
     * Flag that a customer add-to-cart action event has occurred so that the requisite Analytics tracking snippet is
     *  sent to the frontend (via the customer-data section architecture) to be executed asynchronously
     *
     * @param \Magento\Framework\Event\Observer $observer
     * @return $this
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        $this->_sccAnalyticsTrackingJsHelper->flagActionEventOccurred('checkout_cart_add_product_complete', []);
    }

    /**
     * CheckoutAddProductComplete constructor.
     *
     * @param \Scc\Analytics\Helper\Js\TrackingJsHelper $sccAnalyticsTrackingJsHelper
     */
    public function __construct(\Scc\Analytics\Helper\Js\TrackingJsHelper $sccAnalyticsTrackingJsHelper)
    {
        $this->_sccAnalyticsTrackingJsHelper = $sccAnalyticsTrackingJsHelper;
    }
}
