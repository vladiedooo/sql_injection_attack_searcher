<?php
/**
 * Author: Sean Dunagan (github: dunagan5887)
 * Date: 9/19/16
 */

namespace Scc\WebApi\Model;

/**
 * A result interface specific to Web API calls
 *
 * Interface ResultInterface
 */
interface ResultInterface extends \Scc\Base\Model\ResultInterface
{
    /**
     * Accessor method for the $_api_method instance field
     * For documentation on this field, please see the PHPDoc above the field itself
     *
     * @return null|string
     */
    public function getApiMethod();

    /**
     * Mutator method for the $_api_method instance field
     * For documentation on this field, please see the PHPDoc above the field itself
     *
     * @param string $api_method
     * @return $this
     */
    public function setApiMethod($api_method);

    /**
     * Accessor method for the $_api_method_parameters instance field
     * For documentation on this field, please see the PHPDoc above the field itself
     *
     * @return null|array
     */
    public function getApiMethodParameters();

    /**
     * Mutator method for the $_api_method_parameters instance field
     * For documentation on this field, please see the PHPDoc above the field itself
     *
     * @param array $api_method_parameters
     * @return $this
     */
    public function setApiMethodParameters($api_method_parameters);
}
