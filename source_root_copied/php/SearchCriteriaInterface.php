<?php
/**
 * Author: Sean Dunagan (github: dunagan5887)
 * Date: 12/21/16
 */

namespace Scc\Framework\Api;

/**
 * Interface SearchCriteriaInterface
 * @package Scc\Framework\Api
 */
interface SearchCriteriaInterface extends \Magento\Framework\Api\SearchCriteriaInterface
{
    /**
     * Get a list of select fields
     *
     * @return string[]
     */
    public function getSelectFields();

    /**
     * Set a list of select fields
     *
     * @param array $select_fields
     * @return $this
     */
    public function setSelectFields(array $select_fields = []);
}
