<?php
/**
 * Author: Sean Dunagan (github: dunagan5887)
 * Date: 2/16/17
 */

namespace Ideal\SKProducts\Setup\Data\Options\Wrench;

/**
 * Class Size
 * @package Ideal\SKProducts\Setup\Data\Options\Wrench
 */
class Size
{
    /**
     * @return array
     */
    public function getOptionArray()
    {
        return array('values' => array('1/4"', '9/32"', '5/16"', '11/32"', '3/8"', '7/16"', '1/2"', '9/16"', '5/8"',
                                       '11/16"', '3/4"', '13/16"', '7/8"', '15/16"', '1"', '1-1/16"', '1-1/8"',
                                       '1-3/16"', '1-1/4"', '1-5/16"', '1-3/8"', '1-7/16"', '1-1/2"', '1-5/8"',
                                       '1-11/16"', '1-3/4"', '1-13/16"', '1-7/8"', '2"', '2-1/16"', '2-1/8"', '2-3/16"',
                                       '2-1/4"', '2-3/8"', '2-1/2"', '2-9/16"', '2-5/8"', '2-3/4"', '6mm', '7mm', '8mm',
                                       '9mm', '10mm', '11mm', '12mm', '13mm', '14mm', '15mm', '16mm', '17mm', '18mm',
                                       '19mm', '20mm', '21mm', '22mm', '23mm', '24mm', '25mm', '26mm', '27mm', '29mm',
                                       '30mm', '32mm', '1/4" x 5/16"', '3/8" x 7/16"', '7/16" x 1/2"', '1/2" x 9/16"',
                                       '9/16" x 5/8"', '5/8" x 11/16"', '5/8" x 3/4"', '3/4" x 3/16"', '3/4" x 7/8"',
                                       '1" x 1-1/8"', '6mm x 7mm', '8mm x 9mm', '9mm x 10mm', '9mm x 11mm',
                                       '10mm x 12mm', '11mm x 12mm', '13mm x 14mm', '15mm x 16mm', '15mm x 17mm',
                                       '16mm x 18mm', '17mm x 19mm', '18mm x 20mm', '19mm x 21mm'));
    }
}
