<?php
/**
 * Author: Sean Dunagan (github: dunagan5887)
 * Date: 12/8/16
 */

namespace Scc\Salesrule\Observer\Quotehelper;

use Magento\Framework\Event\ObserverInterface;
use Scc\Salesrule\Helper\QuoteHelper;

/**
 * Class ClearCacheObserver
 * @package Scc\Salesrule\Observer\Quotehelper
 */
class ClearCacheObserver implements ObserverInterface
{
    /**
     * @var QuoteHelper
     */
    protected $_quoteHelper;

    /**
     * @param \Magento\Framework\Event\Observer $observer
     * @return $this
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        $quote = $observer->getData('quote');
        $this->_quoteHelper->clearHighestPricedItemCacheForQuote($quote);
    }

    /**
     * @param QuoteHelper $quoteHelper
     */
    public function __construct(QuoteHelper $quoteHelper)
    {
        $this->_quoteHelper = $quoteHelper;
    }
}
