<?php

namespace Scc\DefaultAnalytics\Model\Config\Source;

class Deployment implements \Magento\Framework\Option\ArrayInterface
{

    /**
     * Return array of deployment options.
     *
     * @return array
     */
    public function toOptionArray()
    {
        return [
            array(
                'value' => 'production',
                'label' => 'Production'
            ),
            array(
                'value' => 'staging',
                'label' => 'Staging'
            )
        ];
    }
}
