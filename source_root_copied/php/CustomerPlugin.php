<?php
/**
 * Author: Sean Dunagan (github: dunagan5887)
 * Date: 2/16/17
 */

namespace Scc\CustomerSegment\Plugin\Model\ResourceModel;

/**
 * Class CustomerPlugin
 * @package Scc\CustomerSegment\Plugin\Model\ResourceModel
 */
class CustomerPlugin
{
    /**
     * @var \Magento\Framework\Event\ManagerInterface
     */
    protected $_eventManager;

    /**
     * @param \Magento\CustomerSegment\Model\ResourceModel\Customer\Interceptor $interceptor
     * @param \Closure $proceed
     * @param int $customerId
     * @param int $websiteId
     * @param array $segmentIds
     * @param string $event_to_dispatch
     * @param bool $is_being_added_to_segments
     *
     * @return \Magento\CustomerSegment\Model\ResourceModel\Customer\Interceptor
     * @throws \Exception
     */
    protected function _dispatchSegmentActionEvent($interceptor, $proceed, $customerId, $websiteId, $segmentIds,
                                                   $event_to_dispatch, $is_being_added_to_segments)
    {
        if (empty($segmentIds))
        {
            // We only want to dispatch the event below if the customer is actually being added to segments
            return $proceed($customerId, $websiteId, $segmentIds);
        }

        $connection = $interceptor->getConnection();

        $connection->beginTransaction();

        try
        {
            // We need to know the delta of segment ids this customer belongs to
            $before_customer_segment_ids = $interceptor->getCustomerWebsiteSegments($customerId, $websiteId);

            $to_return = $proceed($customerId, $websiteId, $segmentIds);

            $after_customer_segment_ids = $interceptor->getCustomerWebsiteSegments($customerId, $websiteId);

            if ($is_being_added_to_segments)
            {
                // If the customer is being added to the segments, we need to see which segment_ids are in
                //  $after_customer_segment_ids and not in $before_customer_segment_ids
                $segment_ids_delta = array_diff($after_customer_segment_ids, $before_customer_segment_ids);
            }
            else
            {
                // If the customer is being removed from the segments, we need to see which segment_ids are in
                //      $before_customer_segment_ids and not in $after_customer_segment_ids
                $segment_ids_delta = array_diff($before_customer_segment_ids, $after_customer_segment_ids);
            }

            // We only want to fire the event below if there is a difference in the segment ids associated with this
            //      customer
            if (!empty($segment_ids_delta))
            {
                $eventParameters = array('customer_id' => $customerId, 'website_id' => $websiteId,
                                         'segment_ids' => $segment_ids_delta);
                $this->_eventManager->dispatch($event_to_dispatch, $eventParameters);
            }
        }
        catch(\Exception $e)
        {
            $connection->rollBack();
            throw $e;
        }

        $connection->commit();

        return $to_return;
    }

    /**
     * @param \Magento\CustomerSegment\Model\ResourceModel\Customer\Interceptor $interceptor
     * @param \Closure $proceed
     * @param int $customerId
     * @param int $websiteId
     * @param array $segmentIds
     *
     * @return \Magento\CustomerSegment\Model\ResourceModel\Customer\Interceptor
     * @throws \Exception
     */
    public function aroundAddCustomerToWebsiteSegments($interceptor, $proceed, $customerId, $websiteId, $segmentIds)
    {
        return $this->_dispatchSegmentActionEvent($interceptor, $proceed, $customerId, $websiteId, $segmentIds,
                                                  'scc_customersegment_add_customer_to_segments', true);
    }

    /**
     * @param \Magento\CustomerSegment\Model\ResourceModel\Customer\Interceptor $interceptor
     * @param \Closure $proceed
     * @param int $customerId
     * @param int $websiteId
     * @param array $segmentIds
     * @return \Magento\CustomerSegment\Model\ResourceModel\Customer\Interceptor
     * @throws \Exception
     */
    public function aroundRemoveCustomerFromWebsiteSegments($interceptor, $proceed, $customerId, $websiteId, $segmentIds)
    {
        return $this->_dispatchSegmentActionEvent($interceptor, $proceed, $customerId, $websiteId, $segmentIds,
                                                  'scc_customersegment_remove_customer_from_segments', false);
    }

    /**
     * CustomerPlugin constructor.
     * @param \Magento\Framework\Event\ManagerInterface $eventManager
     */
    public function __construct(\Magento\Framework\Event\ManagerInterface $eventManager)
    {
        $this->_eventManager = $eventManager;
    }
}
