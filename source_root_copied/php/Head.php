<?php

namespace Scc\DefaultAnalytics\Block;


class Head extends \Magento\Framework\View\Element\Template
{
    /**
     * Construct
     *
     * @param \Magento\Framework\View\Element\Template\Context $context
     * @param \Magento\Customer\Model\Session $customerSession
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        array $data = []
    ) {
        parent::__construct($context, $data);
    }

    public function getScript()
    {
        $type = $this->_scopeConfig->getValue(
            'scc_defaultanalytics_tracking/scc_defaultanalytics_deployment/deployment'
        );

        switch ($type)
        {
            case 'staging':
                return $this->_scopeConfig->getValue(
                    'scc_defaultanalytics_tracking/scc_defaultanalytics_deployment/staging_head'
                );
                break;
            case 'production':
                return $this->_scopeConfig->getValue(
                    'scc_defaultanalytics_tracking/scc_defaultanalytics_deployment/production_head'
                );
            default:

                break;
        }
    }
}