<?php
/**
 * Author: Sean Dunagan (github: dunagan5887)
 * Date: 10/6/16
 */

namespace Scc\Campaign\Model\Api\Service;

/**
 * Class AbstractEmail
 * @package Scc\Campaign\Model\Api\Service\Account
 *
 * @method \Scc\Campaign\Model\Api\Url\SendEmailJssp\FactoryInterface _getUrlFactoryObject()
 */
abstract class AbstractEmail extends Base implements EmailInterface
{
    const ERROR_EMPTY_EMAIL_TEMPLATE_ID = 'No email_template_id was provided to Campaign Email Service %1';
    const ERROR_NO_EMAIL_STORE_ID_DEFINED = 'No store id was provided to Campaign Email Service %1';

    /**
     * {@inheritdoc}
     */
    public function transmitEmailToCampaignSystem($entity, $additional_transmission_arguments)
    {
        return $this->transmitEntityViaWebApi($entity, $additional_transmission_arguments);
    }

    /**
     * All transmissions for subclasses of this class will need to have defined the template_id and store_id
     *  for the transmission
     *
     * {@inheritdoc}
     */
    protected function _validateRequiredTransmissionArguments($additional_transmission_arguments)
    {
        $email_template_id = isset($additional_transmission_arguments['email_template_id'])
                                ? $additional_transmission_arguments['email_template_id'] : null;
        if(empty($email_template_id))
        {
            $error_message = __(self::ERROR_EMPTY_EMAIL_TEMPLATE_ID, get_class($this));
            throw new \Dunagan\WebApi\Model\Exception\Service\Transmission\RequiredArguments($error_message);
        }
        $store_id = isset($additional_transmission_arguments['store_id'])
                        ? $additional_transmission_arguments['store_id'] : null;
        if (empty($store_id))
        {
            $error_message = __(self::ERROR_NO_EMAIL_STORE_ID_DEFINED, get_class($this));
            throw new \Dunagan\WebApi\Model\Exception\Service\Transmission\RequiredArguments($error_message);
        }
    }

    /**
     * Provides the arguments which the Url Factory Object expects
     *
     * @param mixed $entity
     * @param array $additional_transmission_arguments
     *
     * @return array
     */
    protected function _getEmailTransmissionWebApiCallArguments($entity, $additional_transmission_arguments)
    {
        $email_transmission_arguments = array();

        // The existence of the $additional_transmission_arguments['store_id'] value has been confirmed in _validateRequiredTransmissionArguments()
        $store_id_argument = $this->_getUrlFactoryObject()->getExpectedStoreIdArgument();
        $store_id_value = $additional_transmission_arguments['store_id'];
        $email_transmission_arguments[$store_id_argument] = $store_id_value;

        // The existence of the $additional_transmission_arguments['email_template_id'] value has been confirmed in _validateRequiredTransmissionArguments()
        $email_template_id_argument = $this->_getUrlFactoryObject()->getExpectedEmailTemplateIdArgument();
        $email_template_id_value = $additional_transmission_arguments['email_template_id'];
        $email_transmission_arguments[$email_template_id_argument] = $email_template_id_value;

        return $email_transmission_arguments;
    }

    /**
     * {@inheritdoc}
     */
    public function getExpectedRecipientEmailArgument()
    {
        return $this->_getUrlFactoryObject()->getExpectedRecipientEmailArgument();
    }
}
