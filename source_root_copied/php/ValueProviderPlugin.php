<?php
/**
 * Author: Sean Dunagan (github: dunagan5887)
 * Date: 12/7/16
 */

namespace Scc\Salesrule\Plugin\Salesrule\Model\Rule\Metadata;

/**
 * Class ValueProviderPlugin
 * @package Scc\Salesrule\Plugin\Salesrule\Model\Rule\Metadata
 */
class ValueProviderPlugin
{
    /**
     * @var \Magento\Framework\Event\ManagerInterface
     */
    protected $_eventManager;

    /**
     * @var \Magento\Framework\ObjectManagerInterface
     */
    protected $_objectManager;

    /**
     * @param \Magento\SalesRule\Model\Rule\Metadata\ValueProvider\Interceptor $interceptor
     * @param array $returned_metadata_values_array
     * @return array
     */
    public function afterGetMetadataValues($interceptor, $returned_metadata_values_array)
    {
        // Dispatch an event to allow for adding values to the metadata array as appropriate
        // Create a wrapper object to allow for mutating the contents of $returned_metadata_values_array
        $transportObject = $this->_objectManager->create('Magento\Framework\DataObject');
        $transportObject->setMetadataValues($returned_metadata_values_array);
        $eventParameters = array('transport_object' => $transportObject);
        // Dispatch the event
        $this->_eventManager->dispatch('scc_salesrule_after_rule_get_metadata_values', $eventParameters);
        // Get the potentially mutated contents of $returned_metadata_values_array
        $returned_metadata_values_array = $transportObject->getMetadataValues();
        return $returned_metadata_values_array;
    }

    /**
     * ValueProviderPlugin constructor.
     *
     * @param \Magento\Framework\Event\ManagerInterface $eventManager
     * @param \Magento\Framework\ObjectManagerInterface $objectManager
     */
    public function __construct(\Magento\Framework\Event\ManagerInterface $eventManager,
                                \Magento\Framework\ObjectManagerInterface $objectManager)
    {
        $this->_eventManager = $eventManager;
        $this->_objectManager = $objectManager;
    }
}
