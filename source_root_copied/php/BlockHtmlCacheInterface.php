<?php
/**
 * Author: Sean Dunagan (github: dunagan5887)
 * Date: 3/3/17
 */

namespace Scc\AEMTheme\Api;

/**
 * Interface BlockHtmlCacheInterface
 *
 * @package Scc\AEMTheme\Api
 */
interface BlockHtmlCacheInterface
{
    /**
     * Will return the cached entry for the specified block according to website, language and customer group
     *  passed in
     *
     * Will return null if no entry exists in the cache
     *
     * @param string $block_name
     * @param string $website_code
     * @param string $language_code
     * @param int|null $customer_group_id
     *
     * @return string|null - Will return null if no cache entry exists
     */
    public function getHtmlCacheEntry($block_name, $website_code, $language_code, $customer_group_id = null);

    /**
     * Sets an entry in the cache for the block's html according to the parameters given
     *
     * @param string $block_html
     * @param string $block_name
     * @param string $website_code
     * @param string $language_code
     * @param int|null $customer_group_id
     *
     * @return bool - True if success, false otherwise
     */
    public function setHtmlCacheEntry($block_html, $block_name, $website_code, $language_code, $customer_group_id = null);
    
    /**
     * Will flush the html cache for entries related to the parameters passed in
     *
     * @param string        $website_code       - REQUIRED A Magento website code existing in the system
     * @param string        $language_code      - REQUIRED A Magento locale code
     * @param string|null   $block_name         - OPTIONAL The name of a block whose html is being fed from AEM to Magento
     * @param int|null      $customer_group_id  - OPTIONAL The Magento customer group id that this entry is specific to
     *
     * @return void
     */
    public function flushHtmlCacheEntries($website_code, $language_code, $block_name = null, $customer_group_id = null);
}

