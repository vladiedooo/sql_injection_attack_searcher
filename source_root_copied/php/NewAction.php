<?php
/**
 * Author: Sean Dunagan (https://github.com/dunagan5887)
 * Created: 4/14/16
 */

namespace Dunagan\ProcessQueue\Controller\Adminhtml\Task\Unique;

/**
 * Author: Sean Dunagan (https://github.com/dunagan5887)
 * Created: 4/20/16
 *
 * Class NewAction
 * @package Dunagan\ProcessQueue\Controller\Adminhtml\Task\Unique
 *
 * This class provides definitions for the Unique Process Queue task creation form page
 */
class NewAction extends Base implements \Dunagan\Base\Controller\Adminhtml\GridAndFormControllerInterface
{
    /**
     * Execute the action to show the Unique Process Queue task creation form
     *
     * @return void
     */
    public function execute()
    {
        $this->executeNewAction();
    }

    /**
     * {@inheritdoc}
     */
    public function getBreadcrumbsArray()
    {
        return array('Create Unique Task');
    }

    /**
     * {@inheritdoc}
     */
    public function getAclConfigResource()
    {
        return 'Dunagan_ProcessQueue::tasks_edit';
    }
}
