<?php
/**
 * Author: Sean Dunagan (github: dunagan5887)
 * Date: 10/5/16
 */

namespace Scc\Campaign\Plugin\Framework\Mail\Template;

/**
 * Class TransportBuilderPlugin
 * @package Scc\Campaign\Plugin\Framework\Mail\Template
 */
class TransportBuilderPlugin
{
    /**
     * @var null
     */
    protected $_template_id = null;

    /**
     * @var null
     */
    protected $_template_vars = null;

    /**
     * @var null
     */
    protected $_template_options = null;

    /**
     * @return null|string
     */
    public function getTemplateIdentifier()
    {
        return $this->_template_id;
    }

    /**
     * @param \Magento\Framework\Mail\Template\TransportBuilder\Interceptor $interceptor
     * @param string $templateId
     * @return bool
     */
    public function beforeSetTemplateIdentifier($interceptor, $templateId)
    {
        $this->_template_id = $templateId;
        return false;
    }

    /**
     * @return null|array
     */
    public function getTemplateVars()
    {
        return $this->_template_vars;
    }

    /**
     * @param \Magento\Framework\Mail\Template\TransportBuilder\Interceptor $interceptor
     * @param array $templateParams
     * @return bool
     */
    public function beforeSetTemplateVars($interceptor, $templateParams)
    {
        $this->_template_vars = $templateParams;
        return false;
    }

    /**
     * @return null|array
     */
    public function getTemplateOptions()
    {
        return $this->_template_options;
    }

    /**
     * @param \Magento\Framework\Mail\Template\TransportBuilder\Interceptor $interceptor
     * @param array $templateOptions
     * @return bool
     */
    public function beforeSetTemplateOptions($interceptor, $templateOptions)
    {
        $this->_template_options = $templateOptions;
        return false;
    }
}
