<?php
/**
 * Author: Sean Dunagan (github: dunagan5887)
 * Date: 2/16/17
 */

namespace Scc\CustomerSegment\Plugin\Model\ResourceModel;

/**
 * Class SegmentPlugin
 * @package Scc\CustomerSegment\Plugin\Model\ResourceModel
 */
class SegmentPlugin
{
    /**
     * @var \Magento\Framework\Event\ManagerInterface
     */
    protected $_eventManager;

    /**
     * @param \Magento\CustomerSegment\Model\ResourceModel\Segment\Interceptor $interceptor
     * @param \Closure $proceed
     * @param \Magento\CustomerSegment\Model\Segment $segment
     * @return \Magento\CustomerSegment\Model\ResourceModel\Segment\Interceptor
     * @throws \Exception
     */
    public function aroundAggregateMatchedCustomers($interceptor, $proceed, $segment)
    {
        $connection = $interceptor->getConnection();
        
        $connection->beginTransaction();

        try
        {
            $customer_ids_in_segment_before_update = $this->getSegmentCustomerIds($segment->getId(), $connection, $interceptor);

            $to_return = $proceed($segment);

            $customer_ids_in_segment_after_update = $this->getSegmentCustomerIds($segment->getId(), $connection, $interceptor);

            $customer_ids_added_to_segment = array_diff($customer_ids_in_segment_after_update,
                                                        $customer_ids_in_segment_before_update);

            $customer_ids_removed_from_segment = array_diff($customer_ids_in_segment_before_update,
                                                            $customer_ids_in_segment_after_update);

            $eventParameters = array('customer_ids_added' => $customer_ids_added_to_segment,
                                     'customer_ids_removed' => $customer_ids_removed_from_segment,
                                     'segment' => $segment);

            $this->_eventManager->dispatch('scc_customersegment_rematch_customers', $eventParameters);
        }
        catch(\Exception $e)
        {
            $connection->rollBack();
            throw $e;
        }
        
        $connection->commit();

        return $to_return;
    }

    /**
     * Return the ids of the customers in specified segment
     *
     * @param int $segmentId
     * @param \Magento\Framework\DB\Adapter\AdapterInterface $connection
     * @param \Magento\CustomerSegment\Model\ResourceModel\Segment\Interceptor $interceptor
     * @return array
     */
    public function getSegmentCustomerIds($segmentId, $connection, $interceptor)
    {
        $select = $connection
                    ->select()
                    ->from($interceptor->getTable('magento_customersegment_customer'), ['customer_id'])
                    ->where('segment_id = ?', (int)$segmentId);

        return $connection->fetchCol($select);
    }

    /**
     * SegmentPlugin constructor.
     * @param \Magento\Framework\Event\ManagerInterface $eventManager
     */
    public function __construct(\Magento\Framework\Event\ManagerInterface $eventManager)
    {
        $this->_eventManager = $eventManager;
    }
}
