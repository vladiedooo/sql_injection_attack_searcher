<?php
/**
 * Author: Sean Dunagan
 * Created: 10/14/15
 */

namespace Dunagan\ProcessQueue\Model\Exception;

/**
 * Author: Sean Dunagan (https://github.com/dunagan5887)
 * Created: 4/20/16
 *
 * Class Rollback
 * @package Dunagan\ProcessQueue\Model\Exception
 *
 * This class provides functionality for throwing an exception during a Process Queue task's execution that should
 *  result in a database rollback occurring to revert any database queries which were executed during the task's
 *  execution.
 */
class Rollback
    extends \Exception
    implements \Dunagan\ProcessQueue\Model\Task\ResultInterface
{
    // The Status of the task
    // Set to status ERROR by default
    protected $_task_status = \Dunagan\ProcessQueue\Model\Task::STATUS_ERROR;
    // The status messaging of the task
    protected $_task_status_message = null;

    /**
     * Used to pass back any necessary data to the ProcessQueue task helper. Not currently used in the system
     */
    protected $_methodCallbackResult = null;

    /**
     * {@inheritdoc}
     */
    public function shouldTransactionBeRolledBack()
    {
        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function getMethodCallbackResult()
    {
        return $this->_methodCallbackResult;
    }

    /**
     * {@inheritdoc}
     */
    public function setMethodCallbackResult($methodCallbackResult)
    {
        $this->_methodCallbackResult = $methodCallbackResult;
        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function getTaskStatus()
    {
        return $this->_task_status;
    }

    /**
     * {@inheritdoc}
     */
    public function setTaskStatus($task_status)
    {
        $this->_task_status = $task_status;
        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function getTaskStatusMessage()
    {
        return $this->_task_status_message;
    }

    /**
     * {@inheritdoc}
     */
    public function setTaskStatusMessage($task_status_message)
    {
        $this->_task_status_message = $task_status_message;
        return $this;
    }
}
