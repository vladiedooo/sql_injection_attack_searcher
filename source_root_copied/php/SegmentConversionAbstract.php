<?php
/**
 * Author: Sean Dunagan (github: dunagan5887)
 * Date: 4/28/17
 */

namespace Scc\PremiumMembership\Observer\CustomerSegment;

use Magento\Framework\Event\ObserverInterface;

/**
 * Class SegmentConversionAbstract
 * @package Scc\PremiumMembership\Observer\CustomerSegment
 */
abstract class SegmentConversionAbstract implements ObserverInterface
{
    /**
     * @var \Scc\PremiumMembership\Model\Registration
     */
    protected $_premiumMembershipRegistrationSingleton;

    /**
     * @param int $segment_id
     * @param array $customer_ids_array
     * @param bool $is_adding_to_segment
     * @param int $website_id - Some subclasses may need this parameter, others may not. If needed, the subclasses can
     *                              overwrite this method
     */
    abstract protected function _executeCustomerSegmentConversions($segment_id, $customer_ids_array,
                                                                   $is_adding_to_segment, $website_id);

    /**
     * @param int $segment_id
     * @return bool
     */
    protected function _isPremiumCustomerSegmentId($segment_id)
    {
        return $this->_premiumMembershipRegistrationSingleton->isCustomerSegmentPremiumSegment($segment_id);
    }

    /**
     * @param \Scc\PremiumMembership\Model\Registration $premiumMembershipRegistrationSingleton
     */
    public function __construct(\Scc\PremiumMembership\Model\Registration $premiumMembershipRegistrationSingleton)
    {
        $this->_premiumMembershipRegistrationSingleton = $premiumMembershipRegistrationSingleton;
    }
}
