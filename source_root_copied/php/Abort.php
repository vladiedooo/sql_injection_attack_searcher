<?php
/**
 * Author: Sean Dunagan
 * Created: 10/14/15
 */

namespace Dunagan\ProcessQueue\Model\Exception\Rollback;

/**
 * Author: Sean Dunagan (https://github.com/dunagan5887)
 * Created: 4/20/16
 *
 * Class Abort
 * @package Dunagan\ProcessQueue\Model\Exception\Rollback
 *
 * This class represents that a Process Queue task's status should be set to ABORTED as a result of a task execution
 *  that results in a database transaction rollback
 */
class Abort extends \Dunagan\ProcessQueue\Model\Exception\Rollback
{
    protected $_task_status = \Dunagan\ProcessQueue\Model\Task::STATUS_ABORTED;
}
