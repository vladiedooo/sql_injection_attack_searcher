<?php
/**
 * Author: Sean Dunagan (github: dunagan5887)
 * Date: 1/3/17
 */

namespace Scc\AEMCatalog\Model\Product;

/**
 * Class Image
 * @package Scc\Catalog\Model\Product
 */
class Image extends \Magento\Catalog\Model\Product\Image
{
    const RENDITION_TEMPLATE = '%sx%s%s';
    const RENDITION_TEMPLATE_PLACEHOLDER = 'RENDITION_DIMENSION_AND_LETTERHEAD';
    const RENDITION_DEFAULT_VALUE = 'large';

    /**
     * @var \Scc\AEMCatalog\Model\Product\Media\Config
     */
    protected $_aemCatalogProductMediaConfig;

    /**
     * @var \Scc\AEMCatalog\Helper\Url
     */
    protected $_sccAemCatalogUrlHelper;

    /**
     * @var \Magento\Catalog\Helper\ImageFactory
     */
    protected $_imageHelperFactory;

    /**
     * @var \Magento\Catalog\Model\ProductRepository
     */
    protected $_productRepository;

    /**
     * @var array
     */
    protected $_image_helpers_by_sku_image_type_hash = array();

    /**
     * Returns the URL based on the AEM system's path for the image
     *
     * This method is the analog of \Magento\Catalog\Model\Product\Image::getUrl()
     *
     * @return string
     */
    public function getUrl()
    {
        $image_url_base_url = $this->_aemCatalogProductMediaConfig->getImageBaseUrl();
        $image_url_domain_subpath = $this->_aemCatalogProductMediaConfig->getAEMInstanceSubpath();

        $url = $image_url_base_url . $image_url_domain_subpath . '/' . $this->_newFile;

        return $url;
    }

    /**
     * Set filenames for base file and new file respective to the AEM system
     *
     * This method is the analog of \Magento\Catalog\Model\Product\Image::setBaseFile()
     *
     * @param \Magento\Catalog\Model\Product $product
     * @param string $image_type
     * @return $this
     * @throws \Exception
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    public function setAemBaseFile($product, $image_type)
    {
        $this->_isBaseFilePlaceholder = false;
        // Check that we have been passed a product with a sku
        if (is_object($product) && $product->getSku())
        {
            // Get the image helper for this product and image type to obtain the desired dimensions for the image
            $imageHelper = $this->getImageHelperByProductAndImageType($product, $image_type);
            /** @var \Magento\Catalog\Helper\Image $imageHelper */
            $image_width = $imageHelper->getWidth();
            $image_height = $imageHelper->getHeight();
            if ( (!empty($image_width)) && (!empty($image_height)) )
            {
                // Set the dimensions provided by the $imageHelper
                $this->setWidth($image_width);
                $this->setHeight($image_height);
            }
            /*
             * else
             * {
             *      There is a possibility that an $image_type value was provided which does not have a corresponding
             *          view.xml node entry. In this event, it is likely that the calling block has already executed
             *          code which has set the desired width and height on this object for the image dimensions.
             *          As such, we do not want to set the dimensions to null here.
             *          - Sean Dunagan 2017/03/23
             * }
             */
        }
        else
        {
            // If we have not been passed a product with a sku, set the dimensions to be null. This will default to an
            //      'original' image (meaning not resized)
            $this->setWidth(null);
            $this->setHeight(null);
        }

        $baseDir = $this->_aemCatalogProductMediaConfig->getProductMediaPath();

        $product_image_url_template = $this->_aemCatalogProductMediaConfig->getAEMProductImageUrlTemplate();

        $populated_image_url_template
            = $this->_sccAemCatalogUrlHelper->replaceTemplatedUrlValuesForProductSkuUrl($product_image_url_template,
                                                                                        $product);

        $letterhead_value = $this->_keepAspectRatio ? 'letterbox' : '';
        if ((!empty($this->_width)) && (!empty($this->_height)))
        {
            $rendition_value = sprintf(self::RENDITION_TEMPLATE, $this->_width, $this->_height, $letterhead_value);
        }
        else
        {
            $rendition_value = self::RENDITION_DEFAULT_VALUE;
        }

        $image_url = str_replace(self::RENDITION_TEMPLATE_PLACEHOLDER, $rendition_value, $populated_image_url_template);

        if (empty($baseDir))
        {
            $baseFile = $image_url;
        }
        else
        {
            $baseFile = $baseDir . '/' . $image_url;
        }

        $baseFile = trim($baseFile);

        $this->_baseFile = $baseFile;
        $this->_newFile = $baseFile;

        return $this;
    }

    /**
     * @param \Magento\Catalog\Model\Product $product
     * @param string $image_type
     *
     * @return mixed
     */
    public function getImageHelperByProductAndImageType($product, $image_type)
    {
        $product_sku = $product->getSku();
        $hash = $product_sku . $image_type;

        if (!isset($this->_image_helpers_by_sku_image_type_hash[$hash]))
        {
            $imageHelper = $this->_imageHelperFactory->create()->init($product, $image_type);
            $this->_image_helpers_by_sku_image_type_hash[$hash] = $imageHelper;
        }
    
        return $this->_image_helpers_by_sku_image_type_hash[$hash];
    }

    /**
     * TODO Need to return the image size info based on the file which was set above
     *
     * This method is the analog of \Magento\Catalog\Model\Product\Image::getResizedImageInfo()
     *
     * @return array
     */
    public function getResizedImageInfo()
    {
        $fileInfo = null;
        return $fileInfo;
    }

    /**
     * Provided to allow for clearing attributes in the event that we're expecting a full image request
     */
    public function clearAttributes()
    {
        $this->setWidth(null);
        $this->setHeight(null);
    }

    /**
     * @param \Magento\Framework\Model\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     * @param \Magento\Catalog\Model\Product\Media\Config $catalogProductMediaConfig
     * @param \Magento\MediaStorage\Helper\File\Storage\Database $coreFileStorageDatabase
     * @param \Magento\Framework\Filesystem $filesystem
     * @param \Magento\Framework\Image\Factory $imageFactory
     * @param \Magento\Framework\View\Asset\Repository $assetRepo
     * @param \Magento\Framework\View\FileSystem $viewFileSystem
     * @param \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
     * @param \Scc\AEMCatalog\Model\Product\Media\Config $aemCatalogProductMediaConfig
     * @param \Scc\AEMCatalog\Helper\Url $sccAemCatalogUrlHelper
     * @param \Magento\Catalog\Helper\ImageFactory $imageHelperFactory
     * @param \Magento\Catalog\Model\ProductRepository $productRepository
     * @param \Magento\Framework\Model\ResourceModel\AbstractResource $resource
     * @param \Magento\Framework\Data\Collection\AbstractDb $resourceCollection
     * @param array $data
     * @SuppressWarnings(PHPMD.ExcessiveParameterList)
     * @SuppressWarnings(PHPMD.UnusedLocalVariable)
     */
    public function __construct(
        \Magento\Framework\Model\Context $context,
        \Magento\Framework\Registry $registry,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Catalog\Model\Product\Media\Config $catalogProductMediaConfig,
        \Magento\MediaStorage\Helper\File\Storage\Database $coreFileStorageDatabase,
        \Magento\Framework\Filesystem $filesystem,
        \Magento\Framework\Image\Factory $imageFactory,
        \Magento\Framework\View\Asset\Repository $assetRepo,
        \Magento\Framework\View\FileSystem $viewFileSystem,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Scc\AEMCatalog\Model\Product\Media\Config $aemCatalogProductMediaConfig,
        \Scc\AEMCatalog\Helper\Url $sccAemCatalogUrlHelper,
        \Magento\Catalog\Helper\ImageFactory $imageHelperFactory,
        \Magento\Catalog\Model\ProductRepository $productRepository,
        \Magento\Framework\Model\ResourceModel\AbstractResource $resource = null,
        \Magento\Framework\Data\Collection\AbstractDb $resourceCollection = null,
        array $data = []
    ) {
        parent::__construct($context, $registry, $storeManager, $catalogProductMediaConfig, $coreFileStorageDatabase,
                           $filesystem, $imageFactory, $assetRepo, $viewFileSystem, $scopeConfig, $resource,
                           $resourceCollection, $data);

        $this->_aemCatalogProductMediaConfig = $aemCatalogProductMediaConfig;
        $this->_sccAemCatalogUrlHelper = $sccAemCatalogUrlHelper;
        $this->_imageHelperFactory = $imageHelperFactory;
        $this->_productRepository = $productRepository;
    }
}
