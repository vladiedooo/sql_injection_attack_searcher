<?php
/**
 * Author: Sean Dunagan (github: dunagan5887)
 * Date: 1/30/17
 */

namespace Scc\AEMProductVideo\Helper\Renderer\Api\Gallery;

/**
 * Class Videos
 * @package Scc\AEMCatalog\Helper\Renderer\Api\Gallery
 */
class Videos extends \Scc\AEMCatalog\Helper\Renderer\Api\AbstractGallery
{
    /**
     * {@inheritdoc}
     */
    protected function _getCombinedMediaGalleryJsonIndex()
    {
        return 'video_gallery_data';
    }
}
