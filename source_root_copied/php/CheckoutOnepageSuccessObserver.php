<?php
/**
 * Author: Sean Dunagan (github: dunagan5887)
 * Date: 5/12/17
 */

namespace Scc\Analytics\Observer\Tracking\Js\Population;

use Magento\Framework\Event\ObserverInterface;

/**
 * Class CheckoutOnepageSuccessObserver
 * @package Scc\Analytics\Observer\Tracking\Js\Population
 */
class CheckoutOnepageSuccessObserver implements ObserverInterface
{
    /**
     * @var \Scc\Analytics\Helper\Js\Tracking\Populator\CheckoutOnepageSuccessHelper
     */
    protected $_jsTrackingCheckoutOnepageSuccessHelper;

    /**
     * 
     *
     * @param \Magento\Framework\Event\Observer $observer
     * @return $this
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        $transportObject = $observer->getData('transport_object');
        $js_snippet = $transportObject->getJsSnippet();
        $action_event_data = $transportObject->getActionEventData();
        $order_increment_id = isset($action_event_data['order_id']) ? $action_event_data['order_id'] : null;
        if (is_null($order_increment_id))
        {
            // This should never happen, but in the event that it does, do not modify the js snippet
            return;
        }

        // Populate the JS snippet template
        $js_snippet = $this->_jsTrackingCheckoutOnepageSuccessHelper
                           ->populateCheckoutOnepageSuccessJsSnippet($js_snippet, $order_increment_id);
        // Set the JS snippet on the transport object to return the updated snippet to the calling block
        $transportObject->setJsSnippet($js_snippet);
    }

    /**
     * @param \Scc\Analytics\Helper\Js\Tracking\Populator\CheckoutOnepageSuccessHelper $jsTrackingCheckoutOnepageSuccessHelper
     */
    public function __construct(\Scc\Analytics\Helper\Js\Tracking\Populator\CheckoutOnepageSuccessHelper $jsTrackingCheckoutOnepageSuccessHelper)
    {
        $this->_jsTrackingCheckoutOnepageSuccessHelper = $jsTrackingCheckoutOnepageSuccessHelper;
    }
}
