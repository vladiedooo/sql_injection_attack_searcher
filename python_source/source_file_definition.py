# coding=utf-8

"""This module, source_file_definition.py, defines the SourceFile class and related functions."""

# Needed for operations on files + directories.
from python_source import useful_file_operations as ufo
# Needed for getting SQL reserved words.
from python_source import source_file_inspector as sfi
# Needed for advanced IDE typing.
from typing import List


def get_decoded_and_stripped_string(string_to_convert):
	"""Utility function to get code lines without spaces.
	:param string_to_convert : The code line as binary data.
	:return: The cleaned up line."""
	return string_to_convert.decode('utf-8').replace(' ', '').replace('\t', '').replace('\n', '')


class SourceFile:
	"""Maps to a source file in the project to check against."""

	def __init__(self, source_file_path):
		self.file_path           = source_file_path
		self.file_extension      = ufo.get_file_extension(self.file_path).replace('.', '')
		self.file_name           = ufo.get_file_basename(self.file_path)
		self.words               = []
		self._needs_manual_check = False

	def _get_all_words_from_file(self) -> List[str]:
		"""Returns all the words found in the file provided.
		:return          : All the words found in the file as a list of Strings."""
		words = []
		try:
			if ufo.does_file_exist(self.file_path):
				with open(self.file_path, 'rb') as f:

					currently_inside_of_a_comment_block = False

					for line in f:
						this_line_is_a_single_line_comment = False

						the_line = get_decoded_and_stripped_string(line)

						if the_line == '/**':
							currently_inside_of_a_comment_block = True
						elif the_line == '*/':
							currently_inside_of_a_comment_block = False
						elif the_line.startswith('//'):
							this_line_is_a_single_line_comment = True

						if not currently_inside_of_a_comment_block:
							if not this_line_is_a_single_line_comment:
								for word in line.split():
									words.append(word.decode('utf-8'))

			else:
				print('Warning! Can\'t get words from the file{' + str(self.file_path) + '} as that is not a proper path!')
			return words
		except Exception as e:
			print('Exception reading file{' + self.file_name + '} - {' + str(e) + '}')
		finally:
			return words

	def parse(self):
		"""Obtains the information from this source file.
		:return: Void."""
		self.words = self._get_all_words_from_file()

		for i, word in enumerate(self.words):
			# Search for SQL word matches.

			initial_match = False

			# TODO : Try one version that only accepts sql reserved words that start with or with the one of the following {', "}
			# Trying this now.
			the_word = word.upper()
			if the_word.startswith('"') or the_word.startswith('\''):
				the_word = the_word.replace('"', '').replace('\'', '')
			elif the_word.endswith('"') or the_word.endswith('\''):
				the_word = the_word.replace('"', '').replace('\'', '')

			if the_word in sfi.sql_reserved_words:
				initial_match = True

			#if word.upper() in sfi.sql_reserved_words:
			#	initial_match = True

			if initial_match:
				self._needs_manual_check = True

				# Do filtering to remove false-matches.

				# Checks to make sure we didn't just find 'All rights reserved.'
				if len(self.words) - i >= 3:
					if self.words[i] == 'All' and self.words[i + 1] == 'rights' and self.words[i + 2] == 'reserved.':
						self._needs_manual_check = False

				# Checks to make sure we didn't just find 'for license details'.
				if len(self.words) - i >= 3:
					if self.words[i] == 'for' and self.words[i + 1] == 'license' and self.words[i + 2] == 'details.':
						self._needs_manual_check = False

				# Checks against a 'use Magento/...' statement.
				if len(self.words) - i >= 2:
					if self.words[i] == 'use' and self.words[i + 1].startswith('Magento\\'):
						self._needs_manual_check = False

				# Checks to make sure we didn't just find 'Released under LGPL License.'
				#if len(self.words) - i >= 4:
				#	if self.words[i - 1] == 'Released' and self.words[i] == 'under' and self.words[i + 1] == 'LGPL' and self.words[i + 2] == 'License.':
				#		self._needs_manual_check = False

				if self._needs_manual_check:
					print('Found a match for{' + self.file_name + '}, match was : [' + word.upper() + ']')

					# TODO : Instead of breaking keep a total list of matches.
					break

	def needs_manual_check(self) -> bool:
		"""Indicates if this source file needs to be manually checked for SQL injection attacks.
		:return: Boolean indicating if this source file needs to be manually checked for SQL injection attacks."""
		return self._needs_manual_check

	def __str__(self):
		return self.file_name + ' {' + self.file_extension + '} - {' + self.file_path + '}'


