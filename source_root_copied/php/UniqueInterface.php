<?php
/**
 * Author: Sean Dunagan (github: dunagan5887)
 * Date: 8/24/16
 */

namespace Dunagan\ProcessQueue\Model\ResourceModel\Task;

/**
 * The interface for unique task resource models
 *
 * Interface UniqueInterface
 * @package Dunagan\ProcessQueue\Model\ResourceModel\Task
 */
interface UniqueInterface extends \Dunagan\ProcessQueue\Model\ResourceModel\TaskInterface
{
}
