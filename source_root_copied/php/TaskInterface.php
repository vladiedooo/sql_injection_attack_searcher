<?php
/**
 * Author: Sean Dunagan (https://github.com/dunagan5887)
 * Created: 4/15/16
 */

namespace Dunagan\ProcessQueue\Model;

/**
 * Author: Sean Dunagan (https://github.com/dunagan5887)
 * Created: 4/20/16
 *
 * Interface TaskInterface
 * @package Dunagan\ProcessQueue\Model
 *
 * This interface defines the functionality which \Dunagan\ProcessQueue\Helper\Task\Processor::processQueueTask()
 *  expects to exist on a Process Queue task object
 */
interface TaskInterface
{
    /**
     * Should return the primary key value for the database row representing the task
     *
     * @return int
     */
    public function getTaskId();

    /**
     * Should return the current status for the Process Queue task
     *
     * @return int
     */
    public function getStatus();

    /**
     * Should update the task's database row as being in the processing state
     *
     * @return int
     */
    public function attemptUpdatingRowAsProcessing();

    /**
     * Should return a boolean specifying whether the task is currently processing
     *
     * @return bool
     */
    public function isCurrentlyProcessing();

    /**
     * Should select the task's database row for update to ensure that other mysql database connections can not alter
     *  the row's data
     *
     * @return int
     */
    public function selectForUpdate();

    /**
     * Should execute the task's callback method.
     * Should return a Task\ResultInterface object which contains the status that the task should be placed in
     *
     * @return \Dunagan\ProcessQueue\Model\Task\ResultInterface
     */
    public function executeTask();

    /**
     * Should update the task's database row to reflect the execution of the task
     *
     * @param Task\ResultInterface $taskExecutionResult
     */
    public function actOnTaskResult(\Dunagan\ProcessQueue\Model\Task\ResultInterface $taskExecutionResult);

    /**
     * Should update the task's database row to reflect that the task is in the ERROR state
     *
     * @param null|string $error_message
     * @return int
     */
    public function setTaskAsErrored($error_message = null);

}