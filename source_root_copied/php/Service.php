<?php
/**
 * Author: Sean Dunagan (github: dunagan5887)
 * Date: 1/26/17
 */

namespace Scc\AEMTheme\Helper\Renderer;

/**
 * Class Service
 * @package Scc\AEMTheme\Helper\Renderer
 */
class Service
{
    const ERROR_NO_URL_DEFINED = 'No AEM Renderer API url template was defined for the %1 block';
    const ERROR_API_CALL_NOT_SUCCESSFUL = 'The API call to the AEM system to render the %1 block was not successful: %2';
    const EXCEPTION_GET_HTML_CACHE_ENTRY = 'An exception occurred while attempting to get the AEM block cache entry for block `%1`, website `%2`, language `%3` and customer group id `%4`: %5';
    const EXCEPTION_SETTING_HTML_CACHE_ENTRY_FOR_BLOCK = 'An exception occurred while attempting to set the AEM block html cache entry for block `%1`, website `%2`, language `%3` and customer group id `%4`: %5';
    const ERROR_NO_FALLBACK_HTML = 'No AEM block fallback html is defined for block %1 on store view %2';

    const EMERGENCY_FALLBACK_BLOCK_HTML_CONFIG_PATH_TEMPLATE = 'aem_theme/emergency_fallback_block_html/%s';

    /**
     * @var \Scc\AEMTheme\Helper\Renderer\Api\Config
     */
    protected $_rendererApiConfig;

    /**
     * @var \Scc\AEMWebApi\Model\LoggerInterface
     */
    protected $_logger;

    /**
     * @var \Scc\AEMTheme\Model\Client\Curl
     */
    protected $_curlClient;

    /**
     * @var \Scc\AEMWebApi\Helper\Url
     */
    protected $_sccAEMWebApiUrlHelper;

    /**
     * @var \Scc\AEMTheme\Api\BlockHtmlCacheInterface
     */
    protected $_aemThemeBlockHtmlCacheInterface;

    /**
     * @var \Magento\Framework\ObjectManagerInterface
     */
    protected $_objectManager;

    /**
     * @var \Magento\Framework\App\Config\ScopeConfigInterface
     */
    protected $_scopeConfig;

    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $_storeManager;

    /**
     * Will attempt to return html for the specified block according to the following process
     * 1. Attempts to find a cached version of the block in the local Magento database
     * 2. Attempts to make an API call to AEM for the specified block
     *      2. a. If this call is successful, will attempt to cache the html in the local Magento database
     * 3. Will return a static "emergency" html template for this block if one exists
     *      TODO Step 3 this has not yet been implemented
     *
     * @param string $block_name            - REQUIRED The name of the block to get html for
     * @param string|null $website_code     - OPTIONAL Will default to the currently loaded website
     * @param string|null $language_code    - OPTIONAL Will default to the currently loaded locale
     * @param string|null $customer_group_id - OPTIONAL NOTE: This is not yet functional/implemented
     *                                                      Will default to the currently logged in user. If no user is
     *                                                      logged in, will pull the generic cache entry for this website
     *                                                      and language code.
     *
     * @return null|string
     */
    public function getHtmlForAEMBlock($block_name, $website_code = null, $language_code = null,
                                       $customer_group_id = null)
    {
        // If website_code or language_code were null, define them
        $website_code = $this->_getWebsiteCodeValue($website_code);
        $language_code = $this->_getLanguageCodeValue($language_code);
        // First check to see if we have this block cached in our local database
        $block_html = $this->getCachedVersionOfBlockHtml($block_name, $website_code, $language_code,
                                                         $customer_group_id);
        if (!empty($block_html))
        {
            // Return the cached version of the block in the database
            return $block_html;
        }
        // Since no html was cached for this block in the database, make call to AEM for the html
        $resultObject = $this->getBlockHtmlViaApiCall($block_name, $website_code, $language_code, $customer_group_id);
        if ($resultObject->getWasSuccessful())
        {
            $block_html = $resultObject->getResultMessage();
            // Attempt to cache the html for this block
            $this->cacheHtmlForAEMBlock($block_html, $block_name, $website_code, $language_code, $customer_group_id);
            // Return the block's html
            return $block_html;
        }
        // The call to AEM was not successful, use an emergency static block if one exists
        return $this->returnEmergencyStaticFileForBlock($block_name, $website_code, $language_code, $customer_group_id);
    }

    /**
     * Will return the locally db cached version of the AEM block.
     * If none is cached in the database, will return null
     *
     * @param string $block_name
     * @param string $website_code
     * @param string $language_code
     * @param int|null $customer_group_id
     * @return string|null
     */
    public function getCachedVersionOfBlockHtml($block_name, $website_code, $language_code, $customer_group_id = null)
    {
        try
        {
            $cached_html = $this->_aemThemeBlockHtmlCacheInterface
                                ->getHtmlCacheEntry($block_name, $website_code, $language_code, $customer_group_id);
        }
        catch(\Exception $e)
        {
            $customer_group_id_value = is_null($customer_group_id) ? 'null' : $customer_group_id;
            $error_message = __(self::EXCEPTION_GET_HTML_CACHE_ENTRY, $block_name, $website_code, $language_code,
                                $customer_group_id_value, $e->getMessage());
            $this->_logger->error($error_message);

            return null;
        }

        return $cached_html;
    }

    /**
     * TODO Implement returning a static "Emergency" header template
     *
     * @param string $block_name
     * @param string $website_code
     * @param string $language_code
     * @param int|null $customer_group_id
     * @return string
     */
    public function returnEmergencyStaticFileForBlock($block_name, $website_code, $language_code,
                                                      $customer_group_id = null)
    {
        $fallback_html_config_path = sprintf(self::EMERGENCY_FALLBACK_BLOCK_HTML_CONFIG_PATH_TEMPLATE, $block_name);
        $store_view = $this->_storeManager->getStore()->getCode();
        $fallback_html = $this->_scopeConfig->getValue($fallback_html_config_path,
                                                       \Magento\Store\Model\ScopeInterface::SCOPE_STORE, $store_view);
        if (!empty($fallback_html))
        {
            return $fallback_html;
        }

        $error_message = __(self::ERROR_NO_FALLBACK_HTML, $block_name, $store_view);
        $this->_logger->error($error_message);

        return (!empty($fallback_html)) ? $fallback_html : '';
    }

    /**
     * Will attempt to cache the AEM block's html in the local database
     * Will return true if the attempt was successful, false otherwise
     *
     * @param string $block_html
     * @param string $block_name
     * @param string $website_code
     * @param string $language_code
     * @param int|null $customer_group_id
     * @return bool
     */
    public function cacheHtmlForAEMBlock($block_html, $block_name, $website_code, $language_code,
                                         $customer_group_id = null)
    {
        try
        {
            $result = $this->_aemThemeBlockHtmlCacheInterface->setHtmlCacheEntry($block_html, $block_name, $website_code,
                                                                                 $language_code, $customer_group_id);
            return $result ? true : false;
        }
        catch(\Exception $e)
        {
            $customer_group_id_value = is_null($customer_group_id) ? 'null' : $customer_group_id;
            $error_message = __(self::EXCEPTION_SETTING_HTML_CACHE_ENTRY_FOR_BLOCK, $block_name, $website_code,
                                $language_code, $customer_group_id_value, $e->getMessage());
            $this->_logger->error($error_message);
        }

        return false;
    }

    /**
     * Will invoke an API call to the AEM system to render the block specified.
     * If the call was unsuccessful, will log an error stating such
     *
     * @param string $block_to_render
     * @param string $website_code
     * @param string $language_code
     * @param int|null $customer_group_id
     * @return \Scc\AEMWebApi\Model\ResultInterface
     */
    public function getBlockHtmlViaApiCall($block_to_render, $website_code, $language_code, $customer_group_id = null)
    {
        // Need to retrieve the URL associated with this block
        $url_template = $this->_rendererApiConfig->getRendererApiUrl($block_to_render);
        if (empty($url_template))
        {
            // If no $url is defined, log an error and return an empty string
            $error_message = __(self::ERROR_NO_URL_DEFINED, $block_to_render);
            $this->_logger->critical($error_message);
            // Create a result object to return
            $resultObject = $this->_objectManager->create('\Scc\AEMWebApi\Model\ResultInterface');
            /* @var \Scc\AEMWebApi\Model\ResultInterface $resultObject */
            $resultObject->setWasSuccessful(false);
            $resultObject->setResultMessage($error_message);
            $resultObject->setApiMethod("GET");
            $resultObject->setApiMethodParameters([]);
            return $resultObject;
        }

        $url = $this->_sccAEMWebApiUrlHelper->replaceTemplatedUrlValues($url_template, $website_code, $language_code,
                                                                        $customer_group_id);

        // Use the $url to make an API call to the AEM system
        $api_call_params = ['url' => $url];
        $resultObject = $this->_curlClient->callApiMethod("GET", $api_call_params);
        if (!$resultObject->getWasSuccessful())
        {
            // Log an error
            $api_call_result = $resultObject->getResultMessage();
            $error_message = __(self::ERROR_API_CALL_NOT_SUCCESSFUL, $block_to_render, $api_call_result);
            $this->_logger->critical($error_message);
        }

        return $resultObject;
    }

    /**
     * If $website_code is null, will return the website code value that the AEM Web API uses for this store view
     *
     * @param string|null $website_code
     *
     * @return string
     */
    protected function _getWebsiteCodeValue($website_code)
    {
        if(is_null($website_code))
        {
            return $this->_sccAEMWebApiUrlHelper->getWebsiteCodeForAEMCommunications();
        }

        return $website_code;
    }

    /**
     * If $language_code is null, will return the locale value that the AEM Web API uses for this store view
     *
     * @param string|null $language_code
     *
     * @return string
     */
    protected function _getLanguageCodeValue($language_code)
    {
        if(is_null($language_code))
        {
            return $this->_sccAEMWebApiUrlHelper->getLanguageCodeForAEMCommunications();
        }

        return $language_code;
    }

    /**
     * Api constructor.
     * @param Api\Config $rendererApiConfig
     * @param \Scc\AEMTheme\Model\Client\Curl $curlClient
     * @param \Scc\AEMWebApi\Model\LoggerInterface $logger
     * @param \Scc\AEMWebApi\Helper\Url $sccAEMWebApiUrlHelper
     * @param \Scc\AEMTheme\Api\BlockHtmlCacheInterface $aemThemeBlockHtmlCacheInterface
     * @param \Magento\Framework\ObjectManagerInterface $objectManager
     * @param \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     */
    public function __construct(\Scc\AEMTheme\Helper\Renderer\Api\Config $rendererApiConfig,
                                \Scc\AEMTheme\Model\Client\Curl $curlClient,
                                \Scc\AEMWebApi\Model\LoggerInterface $logger,
                                \Scc\AEMWebApi\Helper\Url $sccAEMWebApiUrlHelper,
                                \Scc\AEMTheme\Api\BlockHtmlCacheInterface $aemThemeBlockHtmlCacheInterface,
                                \Magento\Framework\ObjectManagerInterface $objectManager,
                                \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
                                \Magento\Store\Model\StoreManagerInterface $storeManager)
    {
        $this->_rendererApiConfig = $rendererApiConfig;
        $this->_curlClient = $curlClient;
        $this->_logger = $logger;
        $this->_sccAEMWebApiUrlHelper = $sccAEMWebApiUrlHelper;
        $this->_aemThemeBlockHtmlCacheInterface = $aemThemeBlockHtmlCacheInterface;
        $this->_objectManager = $objectManager;
        $this->_scopeConfig = $scopeConfig;
        $this->_storeManager = $storeManager;
    }
}
