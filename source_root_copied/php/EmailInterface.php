<?php
/**
 * Author: Sean Dunagan (github: dunagan5887)
 * Date: 10/6/16
 */

namespace Scc\Campaign\Model\Api\Service;

/**
 * Interface EmailInterface
 * @package Scc\Campaign\Model\Api\Service\Account
 */
interface EmailInterface extends \Dunagan\WebApi\Model\Service\Transmission\EntityTransmissionInterface
{
    /**
     * Transmits the Account Email to the Campaign system
     *
     * @param mixed $entity
     * @param array $additional_transmission_arguments
     * @return \Scc\Campaign\Model\Api\ResultInterface
     */
    public function transmitEmailToCampaignSystem($entity, $additional_transmission_arguments);
}
