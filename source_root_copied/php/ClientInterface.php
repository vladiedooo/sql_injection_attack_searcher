<?php
/**
 * Author: Sean Dunagan (github: dunagan5887)
 * Date: 9/18/16
 */

namespace Scc\WebApi\Model;

/**
 * Interface for an external Web Service API Client
 *
 * Interface ClientInterface
 * @package Scc\WebApi\Model
 */
interface ClientInterface
{
    /**
     * Executes the API call, catching exceptions and logging errors
     *
     * @param string $method
     * @param array $parameters
     * @return \Scc\WebApi\Model\ResultInterface
     */
    public function callApiMethod($method, $parameters);

    /**
     * The following are instance fields which subclasses should override
     *
     *      $_configuration_group_id defines the system.xml group id for the api configuration settings
     *      @var string
     *      protected $_configuration_group_id = 'scc_web_api_configuration';
     *
     *
     * The following are instance fields which subclasses can override
     *
     *      Logger to be used to log errors
     *      @var string
     *      protected $_logger_di_classname = '\Psr\Log\LoggerInterface';
     *
     *
     * The name of this class's module. Potentially used when accessing the wsdl file from the module's etc/ directory
     * Built out to allow subclasses to override the value
     *
     *      @var string
     *      protected $_module_name = 'Scc_WebApi';
     */
}
