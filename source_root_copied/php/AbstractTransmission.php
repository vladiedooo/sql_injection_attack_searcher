<?php
/**
 * Author: Sean Dunagan (github: dunagan5887)
 * Date: 10/6/16
 */

namespace Scc\Campaign\Observer\Queue\Email;

use Magento\Framework\Event\ObserverInterface;

/**
 * Class Transmission
 * @package Scc\Campaign\Observer\Queue\Account
 */
abstract class AbstractTransmission implements ObserverInterface
{
    const EXCEPTION_ATTEMPTING_TO_QUEUE_TASK = 'An uncaught exception occurred while attempting to queue a %1 task via an event observed by %2::%3(): %4';
    const EXCEPTION_EXECUTING_CREATED_TASK = 'An uncaught exception occurred while executing a created %1 task via an event observed by %2::%3(): %4';
    const ERROR_NO_TEMPLATE_VARS_ARRAY = 'No template_vars array was provided in the event observer object passed to %1::%2(). The provided data array keys was: %3';

    /**
     * @return \Scc\Campaign\Model\Task\Queue\Email\TransmissionInterface
     */
    abstract protected function _getApiTransmissionQueueTask();

    /**
     * @var \Scc\Campaign\Model\LoggerInterface
     */
    protected $_logger;

    /**
     * @var \Magento\Framework\ObjectManagerInterface
     */
    protected $_objectManager;

    /**
     * If this is true, this observer will attempt to execute the created task immediately, as opposed to having the
     *  task wait until the next crontab iteration of the ProcessQueue TaskProcessor to execute the task
     *
     * @var bool
     */
    protected $_should_execute_task_upon_creation = false;

    /**
     * Built out to allow subclasses to override with custom functionality
     * Currently none of the Campaign transmission tasks are queued in the Unique Task table
     * If a subclass's tasks were to be queued in the unique table, this should be set to
     *  \Dunagan\ProcessQueue\Helper\Task\Unique\Processor in the subclass
     *
     * @var string
     */
    protected $_task_processor_classname = '\Dunagan\ProcessQueue\Helper\Task\Processor';

    /**
     * @var \Dunagan\ProcessQueue\Helper\Task\Processor
     */
    protected $_taskProcessor;

    /**
     * The $observer object is expected to contain the following three fields for event-observers which
     *  extend this class:
     *      string template_id - The Magento template id for the email which is to be sent out
     *      array template_vars - The TransportBuilder::templateVars array which was defined for the email
     *      array template_options - The TransportBuilder::templateOptions array which was defined for the email
     *
     * @param \Magento\Framework\Event\Observer $observer
     * @return \Dunagan\ProcessQueue\Model\TaskInterface|false
     * @throws \Exception
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        try
        {
            $template_vars_array = $observer->getData('template_vars');
            if ((!is_array($template_vars_array)) || empty($template_vars_array))
            {
                $observer_data_array = $observer->getData();
                $observer_data_array_keys = array_keys($observer_data_array);
                $imploded_data_array_keys = implode(', ', $observer_data_array_keys);
                $error_message = __(self::ERROR_NO_TEMPLATE_VARS_ARRAY, get_class($this), __METHOD__,
                                    $imploded_data_array_keys);
                throw new \Scc\Campaign\Model\Exception\Task\QueueingException($error_message);
            }
            // Allow subclasses to set any custom fields from custom logic
            $additional_arguments_array = $this->_prepareAdditionalArgumentsArray($observer);
            // Create the necessary task
            $createdTask = $this->_getApiTransmissionQueueTask()
                                ->queueEntityTransmissionToExternalSystem($observer, $additional_arguments_array,
                                                                          $this->_should_execute_task_upon_creation);
        }
        catch(\Exception $e)
        {
            $error_message = __(self::EXCEPTION_ATTEMPTING_TO_QUEUE_TASK,
                                $this->_getApiTransmissionQueueTask()->getTaskCode(), get_class($this), __METHOD__,
                                $e->getMessage());
            $exceptionToThrow = new \Scc\Campaign\Model\Exception\Task\QueueingException($error_message);
            // We want exceptions here to be thrown so that native magento email functionality will occur
            throw $exceptionToThrow;
        }

        if ($this->_should_execute_task_upon_creation)
        {
            // Attempts to execute the created task
            try
            {
                $this->_taskProcessor->processQueueTask($createdTask, true);
            }
            catch (\Exception $e)
            {
                // The ProcessQueue Task Processor should never let an uncaught exception occur, but prepare for the
                //      case where it does
                $error_message = __(self::EXCEPTION_EXECUTING_CREATED_TASK, $this->_getApiTransmissionQueueTask()->getTaskCode(), get_class($this), __METHOD__, $e->getMessage());
                $this->_logger->logTransmissionTaskQueueingError($error_message);
            }
        }
    }

    /**
     * If there are any additional arguments which should be set, set them
     *
     * This method is built out to allow subclasses to implement any necessary logic
     *
     * @param \Magento\Framework\Event\Observer $observer
     * @return array
     */
    protected function _prepareAdditionalArgumentsArray(\Magento\Framework\Event\Observer $observer)
    {
        return array();
    }

    /**
     * Transmission constructor.
     * @param \Scc\Campaign\Model\LoggerInterface $logger
     * @param \Magento\Framework\ObjectManagerInterface $objectManager
     */
    public function __construct(\Scc\Campaign\Model\LoggerInterface $logger,
                                \Magento\Framework\ObjectManagerInterface $objectManager)
    {
        $this->_logger = $logger;
        $this->_objectManager = $objectManager;
        // Set the Task Processor
        $this->_taskProcessor = $this->_objectManager->get($this->_task_processor_classname);
    }
}
