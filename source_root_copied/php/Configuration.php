<?php
/**
 * Author: Sean Dunagan (github: dunagan5887)
 * Date: 4/18/17
 */

namespace Ideal\SocketOfTheMonth\Model;

/**
 * Class Configuration
 *
 * @package Ideal\SocketOfTheMonth\Model
 */
class Configuration
{
    const SOCKET_OF_THE_MONTH_CATEGORY_NAME = 'Socket of the Month';
    const SOCKET_OF_THE_MONTH_CATEGORY_URL_KEY = 'socket-of-the-month';
    const SOCKET_OF_THE_MONTH_ELIGIBLE_PRODUCT_ATTRIBUTE_CODE = 'socket_of_the_month_eligible';
}
