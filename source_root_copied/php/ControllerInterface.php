<?php
/**
 * Author: Sean Dunagan (https://github.com/dunagan5887)
 * Created: 4/13/16
 */

namespace Dunagan\Base\Controller\Adminhtml;

/**
 * Author: Sean Dunagan (https://github.com/dunagan5887)
 * Created: 4/19/16
 *
 * Interface ControllerInterface
 * @package Dunagan\Base\Controller\Adminhtml
 *
 * This interface defines functionality which is expected of a basic page controller in the admin panel.
 */
interface ControllerInterface
{
    /**
     * This defines which menu item on the left-hand nav menu in the admin panel will be highlighted
     *
     * @return string|null
     */
    public function getActiveMenu();

    /**
     * This method returns breadcrumb values which would be set on the page. The abstract Magento classes define
     *      functionality for setting these values, but to this point I am unsure how/if they get rendered
     *
     * @return array
     */
    public function getBreadcrumbsArray();

    /**
     * This method should return the title of the page in the admin panel
     *
     * @return string|null
     */
    public function getPageTitle();

    /**
     * This method defines the ACL configuration resource which controls access to the page
     *
     * @return string|null
     */
    public function getAclConfigResource();
}