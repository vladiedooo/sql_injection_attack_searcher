<?php
/**
 * Author: Sean Dunagan (github: dunagan5887)
 * Date: 8/23/16
 */

namespace Dunagan\ProcessQueue\Block\Adminhtml\Widget\Grid;

/**
 * Class Container
 * @package Dunagan\ProcessQueue\Block\Adminhtml\Widget\Grid
 *
 * Class which provides ability to add buttons to the ProcessQueue Index page
 */
class Container extends \Dunagan\Base\Block\Adminhtml\Widget\Grid\Container
{
    /**
     * @var string
     */
    protected $_delete_all_successful_tasks_button_label = 'Delete All Successful Tasks';

    /**
     * @var string
     */
    protected $_delete_all_finalized_tasks_button_label = 'Delete All Finalized Tasks';

    /**
     * @var string
     */
    protected $_expedite_task_execution = 'Expedite Tasks';

    /**
     * @param \Magento\Framework\Registry $registry
     * @param \Magento\Backend\Block\Widget\Context $context
     * @param array $data
     */
    public function __construct(\Magento\Framework\Registry $registry, \Magento\Backend\Block\Widget\Context $context,
                                array $data = [])
    {
        parent::__construct($registry, $context, $data);

        $this->_addTaskActionButtons();
    }

    /**
     * Method to add the task deletion buttons to the ProcessQueue grids
     */
    protected function _addTaskActionButtons()
    {
        $this->addButton(
            'delete_all_successful_tasks',
            [
                'label' => __($this->_delete_all_successful_tasks_button_label),
                'onclick' => 'setLocation(\'' . $this->getUrl('*/*/DeleteAllSuccessfulTasks') . '\')',
                'class' => 'primary',
                'sortOrder' => 10
            ]
        );

        $this->addButton(
            'delete_all_finalized_tasks',
            [
                'label' => __($this->_delete_all_finalized_tasks_button_label),
                'onclick' => 'setLocation(\'' . $this->getUrl('*/*/DeleteAllFinalizedTasks') . '\')',
                'class' => 'primary',
                'sortOrder' => 20
            ]
        );

        $this->addButton(
            'expedite_task_execution',
            [
                'label' => __($this->_expedite_task_execution),
                'onclick' => 'setLocation(\'' . $this->getUrl('*/*/Expedite') . '\')',
                'class' => 'primary',
                'sortOrder' => 30
            ]
        );
    }
}
