<?php
/**
 * Author: Sean Dunagan (github: dunagan5887)
 * Date: 4/30/17
 */

namespace Scc\Campaign\Model\Task\Queue\Account;

/**
 * Interface CreationInterface
 * @package Scc\Campaign\Model\Task\Queue\Account
 */
interface CreationInterface extends \Scc\Campaign\Model\Task\Queue\Email\TransmissionInterface
{
    /**
     * Provides the expected customer rp token argument field
     *
     * @return string
     */
    public function getExpectedCustomerRpTokenArgument();
}
