<?php
/**
 * Author: Sean Dunagan (github: dunagan5887)
 * Date: 11/1/16
 */

namespace Dunagan\ProcessQueue\Model\ResourceModel;

/**
 * Interface TaskCollectionInterface
 * @package Dunagan\ProcessQueue\Model\ResourceModel
 */
interface TaskCollectionInterface
{
    /**
     * Will load the items in the array into the collection
     *
     * @param array $array_of_collection_items
     * @return mixed
     */
    public function loadItemsByDataArray(array $array_of_collection_items);
}
