<?php
namespace Ideal\DTMEvents\Observer;

use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\Event\Observer;
use Magento\Customer\Api\CustomerRepositoryInterface;

class CustomerRegister implements ObserverInterface {

	/** @var \Magento\Framework\Logger\Monolog */
	protected $logger;
	protected $cookieMetadataFactory;
    private $customerRegistry;
    private $customerRepository;
	
	public function __construct(
		\Psr\Log\LoggerInterface $loggerInterface,
		\Magento\Framework\Stdlib\Cookie\CookieMetadataFactory $cookieMetadataFactory,
        CustomerRepositoryInterface $customerRepository
	) {
		$this->logger = $loggerInterface;
		$this->cookieMetadataFactory = $cookieMetadataFactory;
		$this->customerRepository = $customerRepository;
	}

	/**
	 * This is the method that fires when the event runs. 
	 * 
	 * @param Observer $observer
	 */
	public function execute( Observer $observer ) {

		$cookieMetadata = $this->cookieMetadataFactory->createPublicCookieMetadata()
            ->setPath('/')
            ->setDomain('.sktools.com');

		$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
		$cookieManager = $objectManager->get('Magento\Framework\Stdlib\CookieManagerInterface');
		$cookieManager->setPublicCookie('customer_registered_success', '1',$cookieMetadata);

        $model = $observer->getEvent()->getData('customer');
        $customer = $this->customerRepository->getById($model->getId());

		$cookieManager->setPublicCookie('mage-customer', $this->getLoggedinCustomerName($customer),$cookieMetadata);
	}

	protected function getLoggedinCustomerName($customer){

		// $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
		// $customerSession = $objectManager->get('Magento\Customer\Model\Session');
		
        if($customer->getId())
        {
            return $customer->getFirstname() . ' ' . $customer->getLastname();
        }
        else
        {
            return "Guest";
        }
    }
}



