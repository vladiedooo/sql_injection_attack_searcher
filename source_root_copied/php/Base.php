<?php
/**
 * Author: Sean Dunagan (github: dunagan5887)
 * Date: 10/12/16
 */

namespace Scc\Campaign\Model\Api\Service;

/**
 * Class Base
 * @package Scc\Campaign\Model\Api\Service
 */
abstract class Base extends \Dunagan\WebApi\Model\Service\Transmission\EntityTransmission
                    implements \Dunagan\WebApi\Model\Service\Transmission\EntityTransmissionInterface
{
    const ERROR_ARGUMENT_NOT_PROVIDED = 'No %1 value was provided as an argument to %2::%3. This value is required';

    /**
     * @var \Magento\Framework\ObjectManagerInterface
     */
    protected $_objectManager;

    /**
     * @var \Scc\Campaign\Model\Api\Client\Curl
     */
    protected $_sccCampaignCurlClient;

    /**
     * @return \Scc\Campaign\Model\Api\Url\FactoryInterface
     */
    abstract protected function _getUrlFactoryObject();

    /**
     * Returns an array containing all of the email parameters which are required by the Url Factory Object which will
     *  construct the API url to be called
     *
     * @param mixed $entity
     * @param mixed $additional_transmission_arguments
     *
     * @return array
     */
    abstract protected function _getTransmissionArguments($entity, $additional_transmission_arguments);

    /**
     * {@inheritdoc}
     */
    protected function _getWebApiCallParameters($entity, $additional_transmission_arguments)
    {
        // As we are doing these API requests via a GET HTTP request, we only need a url parameter
        $email_transmission_arguments = $this->_getTransmissionArguments($entity, $additional_transmission_arguments);

        $web_api_parameters = array();
        $web_api_parameters['url'] = $this->_getUrlFactoryObject()
                                          ->getUrlForApiCall($email_transmission_arguments);

        return $web_api_parameters;
    }

    /**
     * {@inheritdoc}
     */
    protected function _getWebApiClient()
    {
        return $this->_sccCampaignCurlClient;
    }

    /**
     * All calls made by subclasses of this service will use the default method of the \Scc\Campaign\Model\Api\Client\Curl
     *  client
     *
     * {@inheritdoc}
     */
    protected function _getWebApiCallMethod($entity, $additional_transmission_arguments)
    {
        return $this->_sccCampaignCurlClient->getApiMethod();
    }

    /**
     * Base constructor.
     * @param \Magento\Framework\ObjectManagerInterface $objectManager
     * @param \Scc\Campaign\Model\Api\Client\Curl $sccCampaignCurlClient
     */
    public function __construct(\Magento\Framework\ObjectManagerInterface $objectManager,
                                \Scc\Campaign\Model\Api\Client\Curl $sccCampaignCurlClient)
    {
        $this->_objectManager = $objectManager;
        $this->_sccCampaignCurlClient = $sccCampaignCurlClient;
    }
}
