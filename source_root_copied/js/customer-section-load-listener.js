define([
        'jquery',
        'Magento_Customer/js/customer-data'
    ], function($, customerData) {
        "use strict";

        $(document).ajaxSuccess(function(event, request, settings){
            if (typeof settings.url !== 'undefined')
            {
                var is_customer_section_load_route = settings.url.match('/customer/section/load');
                // Ensure that the ajaxSuccess was in response to a call to reload customer sections
                if (is_customer_section_load_route !== null)
                {
                    // Get the returned value for the `scc-analytics-tracking` customer section
                    var scc_analytics_tracking_data_section = customerData.get('scc-analytics-tracking');
                    // Get the property of the scc_analytics_tracking_data_section Customer-Data section which
                    //      contains the tracking snippets to execute
                    var scc_analytics_tracking_js_tracking_snippets
                        = scc_analytics_tracking_data_section().action_event_js_tracking_snippets;

                    for(var scc_analytics_event_hash_id in scc_analytics_tracking_js_tracking_snippets)
                    {
                        // Ensure that this property is not inherited
                        if(scc_analytics_tracking_js_tracking_snippets.hasOwnProperty(scc_analytics_event_hash_id))
                        {
                            // Get the tracking snippet to execute
                            var scc_analytics_tracking_js_tracking_snippet
                                = scc_analytics_tracking_js_tracking_snippets[scc_analytics_event_hash_id];
                            // Evaluate the tracking snippet
                            eval(scc_analytics_tracking_js_tracking_snippet);
                            /*
                                IDEALLY we would remove this snippet from the customerData section here, so as to
                                    leave as little gap as possible between actual execution of the snippet and
                                    removing the snippet from the customerData section listing the snippets which
                                    still need to be executed. This has not yet been tested
                             */
                            // Remove this tracking snippet from the scc_analytics_tracking_js_tracking_snippets object
                            delete scc_analytics_tracking_js_tracking_snippets[scc_analytics_event_hash_id];
                        }
                    }
                    // Reset the snippets array on the section
                    scc_analytics_tracking_data_section().action_event_js_tracking_snippets
                        = scc_analytics_tracking_js_tracking_snippets;
                    // Reset the section to ensure that the snippets we deleted again are not executed again
                    customerData.set('scc-analytics-tracking', scc_analytics_tracking_data_section);
                }
            }
        });
    }
);
