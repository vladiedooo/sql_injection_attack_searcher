<?php
/**
 * Author: Sean Dunagan (github: dunagan5887)
 * Date: 9/20/16
 */

namespace Dunagan\Base\Helper\Validate;

/**
 * Class to implement functionality for determining whether a value is a long
 * This class has been built out to allow for implementing more robust validation in the future
 *
 * Class Long
 */
class Long
{
    /**
     * This method will validate that the value passed in is a valid Xml Xsd long type value
     * The definition of an xsd long is:
     *      The value space of xsd:long is the set of common double-size integers (64 bits)—the integers between
     *      -9223372036854775808 and 9223372036854775807. Its lexical space allows any number of insignificant leading
     *      zeros.  - http://books.xmlschemata.org/relaxng/ch19-77199.html
     *
     * Given that the max int value for PHP on a 64-bit operating system is 9223372036854775807, we can use is_int()
     *  to test whether this value is an integer as long as we are on a 64 bit operating system
     *      http://php.net/manual/en/language.types.integer.php
     *
     * For now, we will only validate that the value return true for is_int(). We will currently not address the
     *      leading zeros issue
     *
     * @param mixed $value_to_validate
     * @return bool
     */
    public function validateValueIsAnXmlXsdLong($value_to_validate)
    {
        return is_int($value_to_validate);
    }
}
