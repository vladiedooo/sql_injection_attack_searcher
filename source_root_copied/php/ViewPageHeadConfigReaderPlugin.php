<?php
/**
 * Author: Sean Dunagan (github: dunagan5887)
 * Date: 3/13/17
 */

namespace Scc\AEMTheme\Plugin\Framework;

use Magento\Framework\View\Layout\Reader\Context;
use Magento\Framework\View\Layout\Element;
use Magento\Framework\View\Page\Config as PageConfig;

/**
 * Class ViewPageHeadConfigReaderPlugin
 *
 * @package Scc\AEMTheme\Plugin\Framework
 */
class ViewPageHeadConfigReaderPlugin extends \Magento\Framework\View\Page\Config\Reader\Head
{
    /**#@+
     * Supported head elements
     */
    const HEAD_CSS = 'css';
    const HEAD_SCRIPT = 'script';
    const HEAD_LINK = 'link';
    const HEAD_REMOVE = 'remove';
    const HEAD_TITLE = 'title';
    const HEAD_META = 'meta';
    const HEAD_ATTRIBUTE = 'attribute';
    const HEAD_PROCESS_REMOVE_ASSETS = 'processRemoveAssets';

    /**
     * @param         $interceptor
     * @param         $proceed
     * @param Context $readerContext
     * @param Element $headElement
     *
     * @return $this
     */
    public function aroundInterpret($interceptor, $proceed, Context $readerContext, Element $headElement)
    {
        $pageConfigStructure = $readerContext->getPageConfigStructure();
        /** @var \Magento\Framework\View\Layout\Element $node */
        foreach ($headElement as $node) {
            switch ($node->getName()) {
                case self::HEAD_CSS:
                case self::HEAD_SCRIPT:
                case self::HEAD_LINK:
                    $this->addContentTypeByNodeName($node);
                    $pageConfigStructure->addAssets($node->getAttribute('src'), $this->getAttributes($node));
                    break;

                case self::HEAD_REMOVE:
                    $pageConfigStructure->removeAssets($node->getAttribute('src'));
                    break;

                case self::HEAD_TITLE:
                    $pageConfigStructure->setTitle($node);
                    break;

                case self::HEAD_META:
                    $this->setMetadata($pageConfigStructure, $node);
                    break;

                case self::HEAD_ATTRIBUTE:
                    $pageConfigStructure->setElementAttribute(
                        PageConfig::ELEMENT_TYPE_HEAD,
                        $node->getAttribute('name'),
                        $node->getAttribute('value')
                    );
                    break;

                case self::HEAD_PROCESS_REMOVE_ASSETS:
                    $pageConfigStructure->processRemoveAssets();
                    break;

                default:
                    break;
            }
        }
        return $interceptor;
    }

    /**
     * Set metadata
     *
     * @param \Magento\Framework\View\Page\Config\Structure $pageConfigStructure
     * @param \Magento\Framework\View\Layout\Element $node
     * @return void
     */
    private function setMetadata($pageConfigStructure, $node)
    {
        if (!$node->getAttribute('name') && $node->getAttribute('property')) {
            $metadataName = $node->getAttribute('property');
        } else {
            $metadataName = $node->getAttribute('name');
        }

        $pageConfigStructure->setMetaData($metadataName, $node->getAttribute('content'));
    }
}
