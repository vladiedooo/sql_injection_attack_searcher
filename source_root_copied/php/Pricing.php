<?php
/**
 * Author: Sean Dunagan (github: dunagan5887)
 * Date: 4/24/17
 */

namespace Ideal\OfflineShipping\Helper\Flatrate;

/**
 * Class Pricing
 *
 * @package Ideal\OfflineShipping\Helper\Flatrate
 */
class Pricing
{
    const CUSTOMER_GROUP_PRICING_CONFIG_PATH = 'customer_group_specific_flatrate_shipping_pricing';
    const GROUP_CODE_NODE = 'group_code';
    const SHIPPING_PRICE_NODE = 'shipping_price';

    /**
     * @var \Magento\Framework\App\Config\ScopeConfigInterface
     */
    protected $_scopeConfig;

    /**
     * @param string $customer_group_code
     * @return float|null
     */
    public function getSpecialShippingPriceByWebsiteAndCustomerGroup($customer_group_code)
    {
        $customer_group_pricing_config = $this->_scopeConfig->getValue(self::CUSTOMER_GROUP_PRICING_CONFIG_PATH);

        if ((!is_array($customer_group_pricing_config)) || empty($customer_group_pricing_config))
        {
            return null;
        }

        foreach($customer_group_pricing_config as $customer_group_pricing_data)
        {
            $group_code = isset($customer_group_pricing_data[self::GROUP_CODE_NODE])
                            ? $customer_group_pricing_data[self::GROUP_CODE_NODE] : null;
            $shipping_price = isset($customer_group_pricing_data[self::SHIPPING_PRICE_NODE])
                            ? $customer_group_pricing_data[self::SHIPPING_PRICE_NODE] : null;

            if (is_null($group_code) || is_null($shipping_price))
            {
                continue;
            }

            if (!strcmp($group_code, $customer_group_code))
            {
                return $shipping_price;
            }
        }

        return null;
    }

    /**
     * Options constructor.
     * @param \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
     */
    public function __construct(\Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig)
    {
        $this->_scopeConfig = $scopeConfig;
    }
}
