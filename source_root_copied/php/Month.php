<?php

namespace Scc\Customers\Setup\Data\Options;

/**
 * Class Month
 * @package Scc\Customers\Setup\Data\Options\Month
 */
class Month
{
    /**
     * @return array
     */
    public function getOptionArray()
    {
        return array(
            'values' => array(
                'January',
                'February',
                'March',
                'April',
                'May',
                'June',
                'July',
                'August',
                'September',
                'October',
                'November',
                'December'
            )
        );
    }
}
