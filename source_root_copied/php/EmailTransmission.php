<?php
/**
 * Author: Sean Dunagan (github: dunagan5887)
 * Date: 10/11/16
 */

namespace Scc\Campaign\Model\Exception\Api;

/**
 * Class EmailTransmission
 * @package Scc\Campaign\Model\Exception\Api
 */
class EmailTransmission extends \Exception
{

}
