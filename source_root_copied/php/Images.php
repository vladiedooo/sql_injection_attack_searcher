<?php
/**
 * Author: Sean Dunagan (github: dunagan5887)
 * Date: 1/27/17
 */

namespace Scc\AEMCatalog\Helper\Renderer\Api\Gallery;

/**
 * Class Images
 * @package Scc\AEMCatalog\Helper\Renderer\Api\Gallery
 */
class Images extends \Scc\AEMCatalog\Helper\Renderer\Api\AbstractGallery
{
    /**
     * {@inheritdoc}
     */
    protected function _getCombinedMediaGalleryJsonIndex()
    {
        return 'image_gallery_data';
    }
}
