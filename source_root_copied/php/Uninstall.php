<?php
use Scc\Customers\Setup;

class Uninstall implements \Magento\Framework\Setup\UninstallInterface
{

    protected $eavSetupFactory;
    /**
     * @var \Scc\Customers\Setup\Data\AttributeDefinitions
     */
    protected $_customerAttributeDefinitionsData;

    public function __construct(
        \Magento\Eav\Setup\EavSetupFactory $eavSetupFactory,
        \Scc\Customers\Setup\Data\AttributeDefinitions $customerAttributeDefinitionsData
    )
    {
        $this->eavSetupFactory = $eavSetupFactory;
        $this->_customerAttributeDefinitionsData = $customerAttributeDefinitionsData;
    }

    public function uninstall(
        \Magento\Framework\Setup\SchemaSetupInterface $setup,
        \Magento\Framework\Setup\ModuleContextInterface $context
    )
    {
        $setup->startSetup();

        $eavSetup = $this->eavSetupFactory->create();

        $attributes = array_keys($this->_customerAttributeDefinitionsData->getCustomerAttributeDefinitionArray());

        foreach($attributes as $attribute)
        {
            $eavSetup->removeAttribute(\Magento\Customer\Model\Customer::ENTITY, $attribute);
        }

        $setup->endSetup();

    }
}