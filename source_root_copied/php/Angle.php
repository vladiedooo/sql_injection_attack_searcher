<?php
/**
 * Author: Sean Dunagan (github: dunagan5887)
 * Date: 2/16/17
 */

namespace Ideal\SKProducts\Setup\Data\Options\Pliers;

/**
 * Class Angle
 * @package Ideal\SKProducts\Setup\Data\Options\Pliers
 */
class Angle
{
    /**
     * @return array
     */
    public function getOptionArray()
    {
        return array('values' => array('0°', '18°', '45°', '90°'));
    }
}
