<?php
/**
 * Author: Sean Dunagan (github: dunagan5887)
 * Date: 8/24/16
 */

namespace Dunagan\ProcessQueue\Controller\Adminhtml\Task\Unique;

/**
 * Class DeleteAllSuccessfulTasks
 * @package Dunagan\ProcessQueue\Controller\Adminhtml\Task\Unique
 *
 * This class executes the action to delete all successful unique tasks in the system
 */
class DeleteAllSuccessfulTasks extends \Dunagan\ProcessQueue\Controller\Adminhtml\Task\DeleteAllSuccessfulTasks
                               implements \Dunagan\Base\Controller\Adminhtml\GridAndFormControllerInterface
{
    /**
     * {@inheritdoc}
     */
    protected $_task_resource_interface_name = '\Dunagan\ProcessQueue\Model\ResourceModel\Task\UniqueInterface';

    /**
     * {@inheritdoc}
     */
    public function getAclConfigResource()
    {
        return 'Dunagan_ProcessQueue::delete_all_successful_unique_tasks';
    }
}
