<?php
/**
 * Author: Sean Dunagan (github: dunagan5887)
 * Date: 2/16/17
 */

namespace Ideal\SKProducts\Setup\Data\Options\Wrench;

/**
 * Class Length
 * @package Ideal\SKProducts\Setup\Data\Options\Wrench
 */
class Length
{
    /**
     * @return array
     */
    public function getOptionArray()
    {
        return array('values' => array('10" and Less', 'Greater than 10"'));
    }
}
