<?php
/**
 * Author: Sean Dunagan (github: dunagan5887)
 * Date: 1/27/17
 */

namespace Scc\AEMCatalog\Helper\Renderer\Api;

/**
 * Class AbstractGallery
 * @package Scc\AEMCatalog\Helper\Renderer\Api
 */
abstract class AbstractGallery
{
    const ERROR_MISSING_VALUE = 'Gallery json field %1 is not defined for image %2';
    const ERROR_INVALID_GALLERY_JSON = 'The following errors were detected in the gallery json for product %1: %2';

    const AEM_COMMUNICATION_FAILURE_TYPE = 'Catalog Gallery';

    /**
     * @var string|null
     */
    protected $_combined_media_gallery_json = null;

    /**
     * @var \Scc\AEMCatalog\Model\Api\Service\Product\Media\Gallery
     */
    protected $_aemCatalogProductGalleryServiceSingleton;

    /**
     * @var \Scc\AEMCatalog\Model\Product\Media\Config
     */
    protected $_sccAEMProductMediaConfig;

    /**
     * @var \Scc\AEMWebApi\Model\LoggerInterface
     */
    protected $_logger;

    /**
     * Returns the index of the combined media gallery json which holds the data necessary for this particular gallery
     *  json
     *
     * @return string
     */
    abstract protected function _getCombinedMediaGalleryJsonIndex();

    /**
     * @var array
     */
    protected $_expected_gallery_image_url_fields = array('thumb', 'img', 'full');

    /**
     * @var array
     */
    protected $_expected_gallery_non_image_url_fields = array('position', 'isMain');

    /**
     * Will return a json string representing the gallery to be rendered for the product
     *
     * @param \Magento\Catalog\Model\Product $product
     * @return string
     */
    public function getGalleryJsonForProduct($product)
    {
        $combined_media_json = $this->_getCombinedGalleryJsonForProduct($product);
        if (empty($combined_media_json))
        {
            return '';
        }

        $decoded_json = json_decode($combined_media_json, TRUE);
        if (is_null($decoded_json))
        {
            // The response was not a valid json
            return '';
        }

        $base_product_gallery_image_url = $this->_sccAEMProductMediaConfig->getAEMProductGalleryBaseUrl();

        $all_expected_fields = array_merge($this->_expected_gallery_image_url_fields,
                                           $this->_expected_gallery_non_image_url_fields);

        $errors_array = array();
        foreach ($decoded_json as $index => $image_data)
        {
            foreach ($all_expected_fields as $field)
            {
                $value = isset($image_data[$field]) ? $image_data[$field] : null;
                if(is_null($value))
                {
                    $error_message = __(self::ERROR_MISSING_VALUE, $field, $index);
                    $errors_array[] = $error_message;
                }
                elseif (in_array($field, $this->_expected_gallery_image_url_fields))
                {
                    $new_value = $base_product_gallery_image_url . $value;
                    $decoded_json[$index][$field] = $new_value;
                }
            }
        }

        if (!empty($errors_array))
        {
            $error_message = __(self::ERROR_INVALID_GALLERY_JSON, $product->getSku(), implode(', ', $errors_array));
            $this->_logger->error($error_message);
            return '';
        }

        $combined_media_json = json_encode($decoded_json);
        return $combined_media_json;
    }

    /**
     * We need to cache the return from AEM because we don't want to make a separate call for the Image and the Video
     *  Gallery Json
     *
     * @param \Magento\Catalog\Model\Product $product
     * @return string
     */
    protected function _getCombinedGalleryJsonForProduct($product)
    {
        $resultObject = $this->_aemCatalogProductGalleryServiceSingleton
                             ->getCombinedMediaGalleryApiResultObject($product);
        return $resultObject->getWasSuccessful() ? $resultObject->getResultMessage() : '';
    }

    /**
     * AbstractGallery constructor.
     *
     * @param \Scc\AEMCatalog\Model\Api\Service\Product\Media\Gallery $aemCatalogProductGalleryServiceSingleton
     * @param \Scc\AEMCatalog\Model\Product\Media\Config              $sccAEMProductMediaConfig
     * @param \Scc\AEMWebApi\Model\LoggerInterface                    $logger
     */
    public function __construct(\Scc\AEMCatalog\Model\Api\Service\Product\Media\Gallery $aemCatalogProductGalleryServiceSingleton,
                                \Scc\AEMCatalog\Model\Product\Media\Config $sccAEMProductMediaConfig,
                                \Scc\AEMWebApi\Model\LoggerInterface $logger)
    {
        $this->_aemCatalogProductGalleryServiceSingleton = $aemCatalogProductGalleryServiceSingleton;
        $this->_sccAEMProductMediaConfig = $sccAEMProductMediaConfig;
        $this->_logger = $logger;
    }
}
