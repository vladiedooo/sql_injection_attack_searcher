<?php
/**
 * Author: Sean Dunagan (github: dunagan5887)
 * Date: 4/25/17
 */

namespace Scc\Campaign\Model\Task\Queue\Membership;

/**
 * Interface ChangeMembershipInterface
 *
 * @package Scc\Campaign\Model\Task\Queue\Membership
 */
interface ChangeMembershipInterface extends \Dunagan\WebApi\Model\Task\Api\TransmissionTaskInterface
{
    /**
     * When a customer's membership has changed (e.g. signed up for Premium Membership, no longer a Premium member)
     *  queue up a task to make an API call to Campaign
     *
     * @param int $customer_id
     * @param int $membership_value - Currently, 1 is for Premium, 0 is for Basic - Sean Dunagan 2017/04/26
     * @param int $website_id
     *
     * @return \Scc\Campaign\Model\Task\Queue\Membership\ChangeMembershipInterface
     */
    public function queueChangeMembershipTask($customer_id, $membership_value, $website_id);
}
