<?php
/**
 * Author: Sean Dunagan (github: dunagan5887)
 * Date: 9/22/16
 */

namespace Dunagan\WebApi\Observer\Queue\Entity;

use Magento\Framework\Event\ObserverInterface;
use Dunagan\WebApi\Model\Task\Api\TransmissionTaskInterface;

/**
 * This class implements functionality common to all event observers which queue up a task to transmit an entity
 *  to an external service via a WebApi
 *
 * Class Transmission
 * @package Dunagan\WebApi\Observer\Queue\Entity
 */
abstract class Transmission implements ObserverInterface
{
    const EXCEPTION_QUEUEING_TRANSMISSION_TO_EXTERNAL_SYSTEM = 'An exception occurred while trying to queue up transmission of %1 with %2 %3 to %4: %5';

    /**
     * Needs to return the Transmission Task which will invoke the Transmission service and invoke the transmission
     *
     * @return TransmissionTaskInterface
     */
    abstract public function getApiTransmissionQueueTask();

    /**
     * Should return an English description of the entity being transmitted by this task
     * E.g. Order, Customer, Shipment
     *
     * @return string
     */
    abstract public function getEntityDescription();

    /**
     * Should return an English description of the external system which the entity is being transmitted to
     * E.g. Adobe-Campaign, Adobe-Analytics, SAP
     *
     * @return string
     */
    abstract public function getExternalSystemDescription();

    /**
     * Needs to return the argument to $observer->getData() which will return the entity to be queued
     *
     * @return string
     */
    abstract public function entityIndexInObserverDataArray();

    /**
     * Can be overridden by subclasses to define system-specific logs files
     * Whatever class or interface is defined here needs to implement \Dunagan\WebApi\Model\LoggerInterface, or an
     *  interface which extends \Dunagan\WebApi\Model\LoggerInterface
     *
     * @var string
     */
    protected $_logger_classname = '\Dunagan\WebApi\Model\LoggerInterface';

    /**
     * Logger instantiated as an object of type $this->_logger_classname
     *
     * @var \Dunagan\WebApi\Model\LoggerInterface
     */
    protected $_logger;

    /**
     * A description of the entity's id. This is built out to allow subclasses to modify error and success messaging
     * E.g. for orders, this might be changed to increment_id
     * E.g. for shipments, this might be changed to tracking number
     *
     * @var string
     */
    protected $_id_description = 'entity_id';

    /**
     * @var \Magento\Framework\ObjectManagerInterface
     */
    protected $_objectManager;

    /**
     * Oftentimes we will only want the transmission to be queued if the entity in question is new in the system
     *
     * @var bool
     */
    protected $_only_queue_if_entity_is_new = false;

    /**
     * Oftentimes we will only want the transmission to be queued if the entity in question is NOT new in the system
     *
     * @var bool
     */
    protected $_only_queue_if_entity_is_not_new = false;

    /**
     * @var bool
     */
    protected $_pass_observer_object_as_entity = false;

    /**
     * @param \Magento\Framework\Event\Observer $observer
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        if (!$this->_pass_observer_object_as_entity)
        {
            $entity_index_in_observer_data_array = $this->entityIndexInObserverDataArray();
            $entity = $observer->getData($entity_index_in_observer_data_array);

            // Check to see if the entity is new
            $entity_is_new = $this->_isEntityNew($entity);
            if (!$entity_is_new)
            {
                // The entity is not new
                if ($this->_only_queue_if_entity_is_new)
                {
                    // Since we only want to queue if the entity is new, do not queue the entity for transmission
                    return;
                }
            }
            // The entity is new
            elseif($this->_only_queue_if_entity_is_not_new)
            {
                // Since we only want to queue if the entity is not new, do not queue the entity for transmission
                return;
            }
        }
        else
        {
            // In this event, pass the observer as the entity
            $entity = $observer;
        }

        try
        {
            $this->_queueEntityTransmissionToExternalSystem($entity);
        }
        catch(\Exception $e)
        {
            $entity_unique_identifier = $this->_getEntityUniqueIdentifier($entity);
            // Log the exception, but don't interrupt execution of the calling block
            $error_message = __(self::EXCEPTION_QUEUEING_TRANSMISSION_TO_EXTERNAL_SYSTEM, $this->getEntityDescription(),
                                $this->_id_description, $entity_unique_identifier,
                                $this->getExternalSystemDescription(), $e->getMessage());
            // Log the error
            $this->_logger->logEntityTransmissionQueueingError($error_message);
        }
    }

    /**
     * Built out to allow subclasses to override the functionality
     *
     * @param $entity
     * @return \Dunagan\ProcessQueue\Model\TaskInterface
     */
    protected function _queueEntityTransmissionToExternalSystem($entity)
    {
        // Queue up the entity transmission
        return $this->getApiTransmissionQueueTask()->queueEntityTransmissionToExternalSystem($entity);
    }

    /**
     * This method is built out to allow subclasses to implement custom functionality
     * This method assumes that the entity passed in is of type \Magento\Framework\Model\AbstractModel, but does not
     *      require it to be so
     *
     * @param \Magento\Framework\Model\AbstractModel $entity
     * @return int
     */
    protected function _getEntityUniqueIdentifier($entity)
    {
        return $entity->getId();
    }

    /**
     * This method built out to allow subclasses to override with custom functionality
     *
     * This implementation of this method assumes that $entity is a subclass of \Magento\Framework\Model\AbstractModel
     *      Subclasses may not be working with a subclass of AbstractModel, so this method is built out to allow for
     *      custom functionality/logic
     *
     * @param \Magento\Framework\Model\AbstractModel $entity
     * @return bool
     */
    protected function _isEntityNew($entity)
    {
        return $entity->isObjectNew();
    }

    /**
     * Transmission constructor.
     * @param \Magento\Framework\ObjectManagerInterface $objectManager
     */
    public function __construct(\Magento\Framework\ObjectManagerInterface $objectManager)
    {
        $this->_objectManager = $objectManager;
        $this->_logger = $this->_objectManager->get($this->_logger_classname);
    }
}
