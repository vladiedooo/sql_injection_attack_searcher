<?php
/**
 * Author: Sean Dunagan (github: dunagan5887)
 * Date: 4/3/17
 */

namespace Scc\Catalog\Plugin\Model\Config\Source;

/**
 * Class ListSortPlugin
 *
 * @package Scc\Catalog\Plugin\Model\Config\Source
 */
class ListSortPlugin
{
    /**
     * @param \Magento\Catalog\Model\Config\Interceptor $interceptor
     * @param array $options
     *
     * @return array
     */
    public function afterToOptionArray($interceptor, $options)
    {
        $options[] = ['label' => __('Availability'), 'value' => 'is_salable'];
        return $options;
    }
}
