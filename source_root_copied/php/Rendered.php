<?php
/**
 * Author: Sean Dunagan (github: dunagan5887)
 * Date: 1/26/17
 */

namespace Scc\AEMTheme\Block\AEM\Api;

use Magento\Framework\View\Element\Template\Context;

/**
 * Class Rendered
 * @package Scc\AEMTheme\Block\AEM\Api
 */
abstract class Rendered extends \Magento\Framework\View\Element\Template
{
    /**
     * @var \Scc\AEMTheme\Helper\Renderer\Service
     */
    protected $_aemThemeRendererServiceHelper;

    /**
     * Needs to return the name of the block to be rendered
     *
     * @return string
     */
    abstract protected function _getNameOfBlockToRender();
    
    /**
     * {@inheritdoc}
     */
    protected function _toHtml()
    {
        $name_of_block_to_render = $this->_getNameOfBlockToRender();
        return $this->_aemThemeRendererServiceHelper->getHtmlForAEMBlock($name_of_block_to_render);
    }

    /**
     * Rendered constructor.
     * @param \Scc\AEMTheme\Helper\Renderer\Service $aemThemeRendererServiceHelper
     */
    public function __construct(Context $context,
                                \Scc\AEMTheme\Helper\Renderer\Service $aemThemeRendererServiceHelper,
                                array $data = [])
    {
        parent::__construct($context, $data);

        $this->_aemThemeRendererServiceHelper = $aemThemeRendererServiceHelper;
    }
}
