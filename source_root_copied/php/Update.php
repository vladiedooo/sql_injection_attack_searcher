<?php
/**
 * Author: Sean Dunagan (github: dunagan5887)
 * Date: 4/24/17
 */

namespace Scc\Customers\Observer\Dob;

use Magento\Framework\Event\ObserverInterface;

/**
 * Class Update
 * @package Scc\Customers\Observer\Dob
 */
class Update implements ObserverInterface
{
    /**
     * @var \Magento\Eav\Model\Config
     */
    protected $_eavConfig;

    /**
     * @var \Magento\Customer\Model\ResourceModel\Group\Collection
     */
    protected $_groupCollection;

    /**
     * @var \Magento\Eav\Model\ResourceModel\Entity\Attribute\Option\Collection
     */
    private $attributeCollection;

    /**
     * @var \Magento\Customer\Api\CustomerRepositoryInterface
     */
    protected $_customerRepository;

    /**
     * @var \Scc\Customers\Helper\Type
     */
    protected $_sccCustomerTypeHelper;

    /**
     * @var \Scc\PremiumMembership\Helper\Customer
     */
    protected $_sccPremiumMembershipCustomerHelper;

    /**
     * @var array
     */
    protected $_dob_month_int_to_string_mapping = array(
        '01' => 'January',
        '02' => 'February',
        '03' => 'March',
        '04' => 'April',
        '05' => 'May',
        '06' => 'June',
        '07' => 'July',
        '08' => 'August',
        '09' => 'September',
        '10' => 'October',
        '11' => 'November',
        '12' => 'December',
    );

    /**
     * If the customer's dob_month has not been set, set it
     *
     * @param \Magento\Framework\Event\Observer $observer
     * @return $this
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        $customer = $observer->getData('customer');
        /* @var /Magento/Customer/Model/Customer $customer */
        $dob_month = $customer->getDobMonth();
        if (empty($dob_month))
        {
            $dob = $customer->getDob();
            $exploded_dob = explode('-', $dob);
            $dob_month_int = isset($exploded_dob[1]) ? $exploded_dob[1] : null;
            if (is_null($dob_month_int))
            {
                // Should never occur, but account for the case where it does
                return;
            }
            $dob_month_as_string = isset($this->_dob_month_int_to_string_mapping[$dob_month_int])
                                    ? $this->_dob_month_int_to_string_mapping[$dob_month_int] : null;
            if (is_null($dob_month_as_string))
            {
                // Should never occur, but account for the case where it does
                return;
            }

            $dobMonthAttributeObject = $this->_eavConfig->getAttribute('customer', 'dob_month');
            $dob_month_options = $dobMonthAttributeObject->getSource()->getAllOptions();
            foreach($dob_month_options as $option)
            {
                $label = isset($option['label']) ? $option['label'] : null;
                $value = isset($option['value']) ? $option['value'] : null;
                if (is_null($label) || is_null($value))
                {
                    // Should never happen, but account for case where it does
                    continue;
                }
                if (!strcmp($dob_month_as_string, $label))
                {
                    $customer->setDobMonth($value);
                    break;
                }
            }
        }

        if ($customer->getId())
        {
            $customerData = $this->_customerRepository->getById($customer->getId());
            $typeOfCustomer = $customerData->getCustomAttribute('type_of_customer');
            $group_id = 0;

            if ($typeOfCustomer)
            {
                $typeOfCustomerLabel = strtolower($this->_sccCustomerTypeHelper->getCustomerTypeLabelByCustomer($customer));
                if ( ! empty($typeOfCustomerLabel))
                {
                    $store_id = $customer->getStoreId();
                    $customer_id = $customer->getId();
                    $is_premium = $this->_sccPremiumMembershipCustomerHelper
                                       ->isCustomerAPremiumMemberByStoreId($customer_id, $store_id);

                    $groupName = $is_premium ? $this->_getPremiumGroupNameByCustomerTypeLabel($typeOfCustomerLabel)
                                             : $this->_getBasicGroupNameByCustomerTypeLabel($typeOfCustomerLabel);

                    $groups = $this->_groupCollection->toOptionArray();
                    foreach($groups as $group)
                    {
                        if (strpos($group['label'], $groupName) !== FALSE)
                        {
                            $group_id = $group['value'];
                            break;
                        }
                    }

                    $customer->setGroupId($group_id);
                }
            }
        }
    }

    /**
     * @param string $customer_type_label
     *
     * @return string
     */
    protected function _getPremiumGroupNameByCustomerTypeLabel($customer_type_label)
    {
        $groupName = 'Enthusiast - Premium';

        switch ($customer_type_label)
        {
            case 'student':
                $groupName = 'VOTEC/Students - Premium';
                break;
            case 'professional':
                $groupName = 'Professional - Premium';
                break;
            case 'enthusiast':
            default:
        }

        return $groupName;
    }

    /**
     * @param string $customer_type_label
     *
     * @return string
     */
    protected function _getBasicGroupNameByCustomerTypeLabel($customer_type_label)
    {
        $groupName = 'Enthusiast - Basic';

        switch ($customer_type_label)
        {
            case 'student':
                $groupName = 'VOTEC/Students - Basic';
                break;
            case 'professional':
                $groupName = 'Professional - Basic';
                break;
            case 'enthusiast':
            default:
        }

        return $groupName;
    }

    /**
     * Update constructor.
     *
     * @param \Magento\Eav\Model\Config                                           $eavConfig
     * @param \Magento\Customer\Model\ResourceModel\Group\Collection              $groupCollection
     * @param \Magento\Eav\Model\ResourceModel\Entity\Attribute\Option\Collection $attributeCollection
     * @param \Magento\Customer\Api\CustomerRepositoryInterface                   $customerRepository
     * @param \Scc\Customers\Helper\Type                                          $sccCustomerTypeHelper
     * @param \Scc\PremiumMembership\Helper\Customer $sccPremiumMembershipCustomerHelper
     */
    public function __construct(
        \Magento\Eav\Model\Config $eavConfig,
        \Magento\Customer\Model\ResourceModel\Group\Collection $groupCollection,
        \Magento\Eav\Model\ResourceModel\Entity\Attribute\Option\Collection $attributeCollection,
        \Magento\Customer\Api\CustomerRepositoryInterface $customerRepository,
        \Scc\Customers\Helper\Type $sccCustomerTypeHelper,
        \Scc\PremiumMembership\Helper\Customer $sccPremiumMembershipCustomerHelper
    )
    {
        $this->_eavConfig = $eavConfig;
        $this->_groupCollection = $groupCollection;
        $this->attributeCollection = $attributeCollection;
        $this->_customerRepository = $customerRepository;
        $this->_sccCustomerTypeHelper = $sccCustomerTypeHelper;
        $this->_sccPremiumMembershipCustomerHelper = $sccPremiumMembershipCustomerHelper;
    }
}
