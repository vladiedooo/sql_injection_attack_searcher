<?php
/**
 * Author: Sean Dunagan (github: dunagan5887)
 * Date: 9/21/16
 */

namespace Dunagan\WebApi\Model\Service\Transmission;

/**
 * Interface defining functionality for a Transmission Service
 *
 * Interface EntityTransmissionInterface
 * @package Dunagan\WebApi\Model\Service\Transmission
 */
interface EntityTransmissionInterface
{
    /**
     * Transmits an entity to an external resource via a web api client
     *
     * @param $entity mixed
     * @param array $additional_transmission_arguments
     * @return \Dunagan\WebApi\Model\ResultInterface
     */
    public function transmitEntityViaWebApi($entity, $additional_transmission_arguments);
}
