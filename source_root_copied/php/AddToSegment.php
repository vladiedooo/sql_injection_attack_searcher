<?php
/**
 * Author: Sean Dunagan (github: dunagan5887)
 * Date: 2/17/17
 */

namespace Scc\PremiumMembership\Observer\CustomerSegment\CustomerGroupTransfer;

use Magento\Framework\Event\ObserverInterface;

/**
 * Class AddToSegment
 * @package Scc\PremiumMembership\Observer\CustomerSegment\CustomerGroupTransfer\Customer
 */
class AddToSegment extends CustomerSegmentChangeAbstract implements ObserverInterface
{
    /**
     * @return bool
     */
    public function isAddingToSegment()
    {
       return true;
    }
}
