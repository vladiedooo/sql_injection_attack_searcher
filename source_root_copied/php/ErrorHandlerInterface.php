<?php
/**
 * Author: Sean Dunagan (github: dunagan5887)
 * Date: 9/18/16
 */

namespace Scc\WebApi\Model\Logger;

/**
 * Interface ErrorHandlerInterface
 * @package Scc\WebApi\Model\Logger
 */
interface ErrorHandlerInterface extends \Monolog\Handler\HandlerInterface
{

}
