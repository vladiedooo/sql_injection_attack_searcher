<?php
/**
 * Author: Sean Dunagan (github: dunagan5887)
 * Date: 12/7/16
 */

namespace Scc\Salesrule\Model\Rule\Action\Discount;

use Magento\SalesRule\Model\Rule\Action\Discount\ByPercent;
use Magento\SalesRule\Model\Rule\Action\Discount\DiscountInterface;
use Magento\SalesRule\Model\Rule\Action\Discount\DataFactory;
use Magento\SalesRule\Model\Validator;
use Magento\Framework\Pricing\PriceCurrencyInterface;
use Scc\Salesrule\Helper\QuoteHelper;

/**
 * This class executes the actual discounting of the single qty of a single item
 *
 * Class MostExpensiveItemByPercent
 * @package Scc\Salesrule\Model\Rule\Action\Discount
 */
class MostExpensiveItemByPercent extends ByPercent implements DiscountInterface
{
    const MOST_EXPENSIVE_ITEM_PERCENT_DISCOUNT_ACTION = 'most_expensive_item_by_percent';
    const ONE_ITEM_QTY = 1;

    /**
     * @var QuoteHelper
     */
    protected $_quoteHelper;

    /**
     * For this discount action, we need to ensure that this discount is only applied to the most expensive item in the
     *  quote (measured by price)
     *
     * {@inheritdoc}
     */
    public function calculate($rule, $item, $qty)
    {
        $quote = $item->getQuote();
        // We need to get the highest priced item in the quote which this rule applies to
        $highest_price_item_id_in_quote = $this->_quoteHelper->getHighestPriceItemInQuoteEligibleForSalesrule($quote,
                                                                                                              $rule);
        $item_id = $item->getId();
        if (
            // If no items in the cart are eligible for the salesrule, return the no discount data object
            (is_null($highest_price_item_id_in_quote))
            ||
            // If this item is not the highest priced eligible item in the cart, return the no discount data object
            (intval($highest_price_item_id_in_quote) != intval($item_id))
        )
        {
            // Return no discount for this item since it is not the most expensive item in the cart
            return $this->_getNoDiscountDataObject();
        }
        // Calculate the discount for this item since it must be the most expensive item in the cart
        $rulePercent = min(100, $rule->getDiscountAmount());
        return $this->_calculate($rule, $item, $qty, $rulePercent);
    }

    /**
     * Returns a Discount\Data object denoting that no discount should be applied to this item
     *
     * @return \Magento\SalesRule\Model\Rule\Action\Discount\Data
     */
    protected function _getNoDiscountDataObject()
    {
        $discountData = $this->discountFactory->create();
        /** @var \Magento\SalesRule\Model\Rule\Action\Discount\Data $discountData */
        $discountData->setAmount(0);
        $discountData->setBaseAmount(0);
        $discountData->setOriginalAmount(0);
        $discountData->setBaseOriginalAmount(0);

        return $discountData;
    }

    /**
     * {@inheritdoc}
     */
    public function fixQuantity($qty, $rule)
    {
        // We shouldn't have a qty of 0 here but check just in case
        if ($qty < 1)
        {
            return 0;
        }
        // This rule is designed to only allow 1 qty of 1 item to be discounted
        return self::ONE_ITEM_QTY;
    }

    /**
     * @param Validator $validator
     * @param DataFactory $discountDataFactory
     * @param PriceCurrencyInterface $priceCurrency
     * @param QuoteHelper $quoteHelper
     */
    public function __construct(Validator $validator, DataFactory $discountDataFactory,
                                PriceCurrencyInterface $priceCurrency, QuoteHelper $quoteHelper)
    {
        parent::__construct($validator, $discountDataFactory, $priceCurrency);

        $this->_quoteHelper = $quoteHelper;
    }
}
