<?php
/**
 * Author: Sean Dunagan (github: dunagan5887)
 * Date: 3/29/17
 */

namespace Scc\Deployment\Console\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Command\Command;

/**
 * Class ApplyHotFixes
 *
 * @package Scc\Deployment\Console\Command
 */
class ApplyHotFixes extends Command
{
    const SUCCESS_APPLIED_PATCH = 'Applied patch file %1';
    const ERROR_PATCH_NOT_APPLIED = 'Following patch file failed to apply, assuming already applied: %1';

    /**
     * @var \Scc\Deployment\Model\Environment
     */
    protected $_magentoEnvironment;

    /**
     * {@inheritdoc}
     * @throws \InvalidArgumentException
     */
    protected function configure()
    {
        $this->setName('scc-deploy:apply-hotfixes')
             ->setDescription('Applies hotfix patches provided for the Magento framework');
        parent::configure();
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->applyMagentoTwoHotfixPatches();
    }

    /**
     * Apply patches distributed through the magento-cloud-configuration file
     */
    public function applyMagentoTwoHotfixPatches()
    {
        $magento_root_dir = $this->_magentoEnvironment->getMagentoRootDirectory();
        $hotfix_patches_dir = $magento_root_dir . '/m2-hotfixes/';

        if (is_dir($hotfix_patches_dir)) {
            $files = glob($hotfix_patches_dir . "*");
            sort($files);
            foreach ($files as $patch_file) {
                $patch_command = 'patch -p1 -N -r - < '  . $patch_file;
                $resultObject = $this->_magentoEnvironment->executeEnvironmentCommand($patch_command);
                if ($resultObject->getWasSuccessful())
                {
                    $success_message = __(self::SUCCESS_APPLIED_PATCH, $patch_file);
                    $this->_magentoEnvironment->logMessage($success_message);
                }
                else
                {
                    $error_message = __(self::ERROR_PATCH_NOT_APPLIED, $patch_file);
                    $this->_magentoEnvironment->logMessage($error_message);
                }
            }
        }
    }

    /**
     * ApplyHotFixes constructor.
     *
     * @param \Scc\Deployment\Model\Environment $magentoEnvironment
     */
    public function __construct(\Scc\Deployment\Model\Environment $magentoEnvironment)
    {
        $this->_magentoEnvironment = $magentoEnvironment;

        parent::__construct();
    }
}
