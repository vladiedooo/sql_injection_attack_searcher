<?php
/**
 * Author: Sean Dunagan (github: dunagan5887)
 * Date: 4/30/17
 */

namespace Scc\Campaign\Model\Exception\Task;

/**
 * Class QueueingException
 * @package Scc\Campaign\Model\Exception\Task
 */
class QueueingException extends \Exception
{

}