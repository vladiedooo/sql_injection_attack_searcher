<?php
/**
 * Author: Sean Dunagan (github: dunagan5887)
 * Date: 2/9/17
 */

namespace Scc\AEMCookie\Observer\Quote;

use Magento\Framework\Event\ObserverInterface;

/**
 * Class Destroy
 * @package Scc\AEMCookie\Observer\Quote
 */
class Destroy implements ObserverInterface
{
    /**
     * @var \Scc\AEMCookie\Cookie\Cart
     */
    protected $_aemCartCookie;

    /**
     * @param \Scc\AEMCookie\Cookie\Cart $aemCartCookie
     */
    public function __construct(\Scc\AEMCookie\Cookie\Cart $aemCartCookie)
    {
        $this->_aemCartCookie = $aemCartCookie;
    }

    /**
     *
     *
     * @param \Magento\Framework\Event\Observer $observer
     * @return $this
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        $this->_aemCartCookie->delete();
    }
}
