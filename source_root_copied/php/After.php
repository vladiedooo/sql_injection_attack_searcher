<?php
/**
 * Author: Sean Dunagan (github: dunagan5887)
 * Date: 2/9/17
 */

namespace Scc\AEMCookie\Observer\Checkout\Cart\Save;

use Magento\Framework\Event\ObserverInterface;

/**
 * Class After
 * @package Scc\AEMCookie\Observer\Checkout\Cart\Save
 */
class After implements ObserverInterface
{
    /**
     * @var \Scc\AEMCookie\Cookie\Cart
     */
    protected $_aemCartCookie;

    /**
     * Sets the AEM Cart Cookie whenever the cart is updated
     *
     * @param \Magento\Framework\Event\Observer $observer
     * @return $this
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        $this->_aemCartCookie->set();
    }

    /**
     * After constructor.
     * @param \Scc\AEMCookie\Cookie\Cart $aemCartCookie
     */
    public function __construct(\Scc\AEMCookie\Cookie\Cart $aemCartCookie)
    {
        $this->_aemCartCookie = $aemCartCookie;
    }
}
