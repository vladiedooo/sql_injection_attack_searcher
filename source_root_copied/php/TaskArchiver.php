<?php
/**
 * Author: Sean Dunagan (github: dunagan5887)
 * Date: 10/26/16
 */

namespace Dunagan\ProcessQueue\Helper;

/**
 * Class TaskArchiver
 * @package Dunagan\ProcessQueue\Helper
 */
class TaskArchiver implements TaskArchiverInterface
{
    /**
     * @var \Dunagan\ProcessQueue\Model\Logger\Archiver
     */
    protected $_archiveLogger;

    /**
     * @var \Magento\Framework\ObjectManagerInterface
     */
    protected $_objectManager;

    /**
     * @var \Dunagan\ProcessQueue\Model\Task
     */
    protected $_taskSingleton;

    /**
     * @var int
     */
    protected $_archive_batch_size = 100;

    /**
     * THE CALLING BLOCK IS EXPECTED TO CATCH ALL EXCEPTIONS THROWN BY THIS METHOD
     */
    public function archiveAllCompletedTasks()
    {
        $task_collections_to_archive = array(
            '\Dunagan\ProcessQueue\Model\ResourceModel\Task\Collection',
            '\Dunagan\ProcessQueue\Model\ResourceModel\Task\Unique\Collection'
        );

        $completed_task_status = array(\Dunagan\ProcessQueue\Model\Task::STATUS_COMPLETE);

        foreach($task_collections_to_archive as $task_collection_classname)
        {
            $this->archiveTasks($task_collection_classname, $completed_task_status);
        }
    }

    /**
     * THE CALLING BLOCK IS EXPECTED TO CATCH ALL EXCEPTIONS THROWN BY THIS METHOD
     *
     * @param string $collection_classname
     * @param array $task_statuses_to_archive
     */
    public function archiveTasks($collection_classname, array $task_statuses_to_archive)
    {
        $collectionOfTasks = $this->_objectManager->create($collection_classname);
        /* @var \Dunagan\ProcessQueue\Model\ResourceModel\Task\Collection $collectionOfTasks */
        $collectionOfTasks->addStatusFilter($task_statuses_to_archive)
                          ->setPageSize($this->_archive_batch_size);
        $collection_is_empty = false;
        while(!$collection_is_empty)
        {
            $task_count = $collectionOfTasks->count();
            $collection_is_empty = $task_count < 1;
            if (!$collection_is_empty)
            {
                $this->archiveTaskCollection($collectionOfTasks);
                $collectionOfTasks->clear();
            }
        }
    }

    /**
     * THE CALLING BLOCK IS EXPECTED TO CATCH ALL EXCEPTIONS THROWN BY THIS METHOD
     *
     * This function will archive the tasks in the collection to a file and then delete all of the tasks in the collection
     *
     * {@inheritdoc}
     */
    public function archiveTaskCollection(\Dunagan\ProcessQueue\Model\ResourceModel\Task\Collection $taskCollectionToArchive)
    {
        // Build the string of data to archive
        $data_string_to_archive = '';
        $array_of_task_ids_to_delete = array();
        foreach($taskCollectionToArchive->getItems() as $taskToArchive)
        {
            /* @var $taskToArchive \Dunagan\ProcessQueue\Model\Task */
            $task_data_array = $taskToArchive->getData();
            $serialized_task_data = serialize($task_data_array);
            $data_string_to_archive .= $serialized_task_data . "\n";
            $array_of_task_ids_to_delete[] = $taskToArchive->getId();
        }
        // Archive the task data
        $this->_archiveLogger->logArchiveTaskData($data_string_to_archive);
        
        // Delete the tasks
        $taskResource = $taskCollectionToArchive->getResource();
        $number_of_deleted_tasks = $taskResource->deleteTasksById($array_of_task_ids_to_delete);
        return $number_of_deleted_tasks;
    }
    
    /**
     * TaskArchiver constructor.
     * @param \Dunagan\ProcessQueue\Model\Logger\Archiver $archiveLogger
     * @param \Magento\Framework\ObjectManagerInterface $objectManager
     * @param \Dunagan\ProcessQueue\Model\Task $taskSingleton
     */
    public function __construct(\Dunagan\ProcessQueue\Model\Logger\Archiver $archiveLogger,
                                \Magento\Framework\ObjectManagerInterface $objectManager,
                                \Dunagan\ProcessQueue\Model\Task $taskSingleton)
    {
        $this->_archiveLogger = $archiveLogger;
        $this->_objectManager = $objectManager;
        $this->_taskSingleton = $taskSingleton;
    }

    /**
     * @return int
     */
    public function getArchiveBatchSize()
    {
        return $this->_archive_batch_size;
    }

    /**
     * @param int $archive_batch_size
     * @return $this
     */
    public function setArchiveBatchSize($archive_batch_size)
    {
        $this->_archive_batch_size = $archive_batch_size;
        return $this;
    }
}
