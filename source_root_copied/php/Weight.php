<?php
/**
 * Author: Sean Dunagan (github: dunagan5887)
 * Date: 2/16/17
 */

namespace Ideal\SKProducts\Setup\Data\Options\Hammer;

/**
 * Class Weight
 * @package Ideal\SKProducts\Setup\Data\Options\Hammer
 */
class Weight
{
    /**
     * @return array
     */
    public function getOptionArray()
    {
        return array('values' => array('1 Lb and less', '1-2 Lb', 'Greater than 2 Lb'));
    }
}
