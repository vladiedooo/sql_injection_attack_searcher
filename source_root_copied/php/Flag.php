<?php
/**
 * Author: Sean Dunagan (github: dunagan5887)
 * Date: 3/5/17
 */

namespace Scc\AEMCacheManager\Helper;

/**
 * Class Flag
 *
 * @package Scc\AEMCacheManager\Helper
 */
class Flag
{
    const DO_NOT_CACHE_PAGE_FLAG_NAME = 'scc_aem_cache_manager_do_not_cache_page';
    const CACHE_PREVENTION_ENABLED_CONFIG_PATH = 'scc_aem_integration_catalog/cache_management/prevent_caching_on_gallery_json_api_failure';

    /**
     * @var array
     */
    protected $_aem_cache_flag = array();

    /**
     * @var \Magento\Framework\App\Response\Http
     */
    protected $_responseObjectSingleton;

    /**
     * @var \Magento\Framework\App\Config\ScopeConfigInterface
     */
    protected $_scopeConfig;

    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $_storeManager;

    /**
     * @var null|boolean
     */
    protected $_is_cache_prevention_enabled = null;

    /**
     * If a call to AEM for html fails, we will not want to cache the page which is being rendered while the call fails.
     * As such, set the Cache-Control header to indicate that we do not want this page cached
     *
     * @return void
     */
    public function flagPageNotToBeCached()
    {
        // Need to set the return to be a 202 Accepted to prevent Varnish from Caching
        $this->_responseObjectSingleton->setStatusHeader(202, null, 'Accepted');
        $this->_responseObjectSingleton->setHeader('Cache-Control', 'must-revalidate, post-check=0, pre-check=0', true);
        // Mark that we set the Do Not Cache Page flag
        $this->setDoNotCachePageFlag(true);
    }

    /**
     * Set the value of whether the page should not be cached
     *
     * @param mixed $value
     *
     * @return Flag
     */
    public function setDoNotCachePageFlag($value)
    {
        return $this->_setCacheFlagValue(self::DO_NOT_CACHE_PAGE_FLAG_NAME, $value);
    }

    /**
     * Get whether the flag to not cache the page has been sent
     *
     * @return mixed|null
     */
    public function getDoNotCachePageFlag()
    {
        return $this->_getCacheFlagValue(self::DO_NOT_CACHE_PAGE_FLAG_NAME);
    }

    /**
     * Returns whether we have disabled the ability to prevent page caching
     *
     * @return boolean
     */
    public function isCachePreventionDueToGalleryAPIFailureEnabled()
    {
        if (is_null($this->_is_cache_prevention_enabled))
        {
            $store_view_code = $this->_storeManager->getStore()->getCode();
            $is_enabled = $this->_scopeConfig->getValue(self::CACHE_PREVENTION_ENABLED_CONFIG_PATH,
                                                        \Magento\Store\Model\ScopeInterface::SCOPE_STORE,
                                                        $store_view_code);
            $this->_is_cache_prevention_enabled = (bool)$is_enabled;
        }

        return $this->_is_cache_prevention_enabled;
    }

    /**
     * @param string $flag
     * @param mixed $value
     * @return $this
     */
    protected function _setCacheFlagValue($flag, $value)
    {
        $this->_aem_cache_flag[$flag] = $value;
        return $this;
    }

    /**
     * @param string $flag
     *
     * @return mixed|null
     */
    protected function _getCacheFlagValue($flag)
    {
        if (!isset($this->_aem_cache_flag[$flag]))
        {
            return null;
        }

        return $this->_aem_cache_flag[$flag];
    }

    /**
     * Flag constructor.
     *
     * @param \Magento\Framework\App\Response\Http $responseObjectSingleton
     * @param \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     */
    public function __construct(\Magento\Framework\App\Response\Http $responseObjectSingleton,
                                \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
                                \Magento\Store\Model\StoreManagerInterface $storeManager)
    {
        $this->_responseObjectSingleton = $responseObjectSingleton;
        $this->_scopeConfig = $scopeConfig;
        $this->_storeManager = $storeManager;
    }
}
