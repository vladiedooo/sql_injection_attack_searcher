<?php
/**
 * Author: Sean Dunagan (github: dunagan5887)
 * Date: 3/29/17
 */

namespace Scc\Catalog\Plugin\Model;

/**
 * Class ConfigPlugin
 *
 * @package Scc\Catalog\Plugin\Model
 */
class ConfigPlugin
{
    /**
     * @param \Magento\Catalog\Model\Config\Interceptor $interceptor
     * @param array $options
     *
     * @return array
     */
    public function afterGetAttributeUsedForSortByArray($interceptor, $options)
    {
        $options['is_salable'] = __('Availability');
        return $options;
    }
}
