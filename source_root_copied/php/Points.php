<?php
/**
 * Author: Sean Dunagan (github: dunagan5887)
 * Date: 2/16/17
 */

namespace Ideal\SKProducts\Setup\Data\Options;

/**
 * Class Points
 * @package Ideal\SKProducts\Setup\Data\Options
 */
class Points
{
    /**
     * @return array
     */
    public function getOptionArray()
    {
        return array('values' => array('5', '6', '12'));
    }
}
