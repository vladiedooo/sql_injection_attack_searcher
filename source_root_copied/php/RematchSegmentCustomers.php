<?php
/**
 * Author: Sean Dunagan (github: dunagan5887)
 * Date: 2/23/17
 */

namespace Scc\PremiumMembership\Observer\CustomerSegment\CustomerGroupTransfer;

use Magento\Framework\Event\ObserverInterface;

/**
 * Class RematchSegmentCustomers
 * @package Scc\PremiumMembership\Observer\CustomerSegment\CustomerGroupTransfer
 */
class RematchSegmentCustomers
        extends \Scc\PremiumMembership\Observer\CustomerSegment\RematchSegmentCustomersAbstract
        implements ObserverInterface
{
    /**
     * @var \Scc\PremiumMembership\Helper\CustomerSegment\CustomerGroupConversions
     */
    protected $_customerGroupConversionsHelper;

    /**
     * @param \Magento\Framework\Event\Observer $observer
     * @return $this
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        $customerSegment = $observer->getData('segment');
        /* @var \Magento\CustomerSegment\Model\Segment $customerSegment */
        $segment_id = $customerSegment->getId();
        if ($this->_isPremiumCustomerSegmentId($segment_id))
        {
            // The $customerSegment can technically be scoped to multiple websites. As such, we need to iterate over
            //      the website ids specific on the object
            $website_ids = $customerSegment->getWebsiteIds();
            foreach($website_ids as $website_id)
            {
                $customer_ids_added = $observer->getData('customer_ids_added');
                $this->_executeCustomerSegmentConversions($segment_id, $customer_ids_added, true, $website_id);
                $customer_ids_removed = $observer->getData('customer_ids_removed');
                $this->_executeCustomerSegmentConversions($segment_id, $customer_ids_removed, false, $website_id);
            }
        }
    }

    /**
     * {@inheritdoc}
     */
    protected function _executeCustomerSegmentConversions($segment_id, $customer_ids_array, $is_adding_to_segment,
                                                          $website_id)
    {
        $this->_customerGroupConversionsHelper
             ->executeCustomerSegmentConversions($segment_id, $customer_ids_array, $is_adding_to_segment);
    }

    /**
     * @param \Scc\PremiumMembership\Model\Registration $premiumMembershipRegistrationSingleton
     * @param \Scc\PremiumMembership\Helper\CustomerSegment\CustomerGroupConversions $customerGroupConversionsHelper
     */
    public function __construct(\Scc\PremiumMembership\Model\Registration $premiumMembershipRegistrationSingleton,
                                \Scc\PremiumMembership\Helper\CustomerSegment\CustomerGroupConversions $customerGroupConversionsHelper)
    {
        parent::__construct($premiumMembershipRegistrationSingleton);

        $this->_customerGroupConversionsHelper = $customerGroupConversionsHelper;
    }
}
