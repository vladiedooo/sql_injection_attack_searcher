<?php
/**
 * Author: Sean Dunagan (github: dunagan5887)
 * Date: 5/2/17
 */

namespace Scc\Campaign\Model\Api\Url\SendEmailJssp\Factory;

/**
 * Interface AccountInterface
 * @package Scc\Campaign\Model\Api\Url\SendEmailJssp\Factory
 */
interface AccountInterface extends \Scc\Campaign\Model\Api\Url\SendEmailJssp\FactoryInterface
{
    /**
     * Provide the calling block the name of the customer argument expected by this url factory
     *
     * @return string
     */
    public function getExpectedCustomerArgument();
}