<?php
/**
 * Author: Sean Dunagan (github: dunagan5887)
 * Date: 8/24/16
 */

namespace Dunagan\ProcessQueue\Controller\Adminhtml\Task\Unique;

/**
 * Class Expedite
 * @package Dunagan\ProcessQueue\Controller\Adminhtml\Task\Unique
 *
 * Controller class to expedite execution of unique tasks in the system
 */
class Expedite extends \Dunagan\ProcessQueue\Controller\Adminhtml\Task\Expedite
               implements \Dunagan\Base\Controller\Adminhtml\GridAndFormControllerInterface
{
    /**
     * Defines the task processor to be used to expedite tasks
     *
     * @var string
     */
    protected $_task_processor = '\Dunagan\ProcessQueue\Helper\Task\Unique\Processor';
}
