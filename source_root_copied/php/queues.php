<?php
/**
 * Author: Sean Dunagan (github: dunagan5887)
 * Date: 9/28/16
 */

namespace Dunagan\Base\Helper\Cache\Account;

/**
 * The purpose of this class is to keep track of whether a customer's account has already been queued for an update
 *  request to Campaign during this execution thread
 *
 * Class Queues
 * @package Dunagan\Base\Helper\Cache\Account
 */
class Queues
{
    /**
     * @var bool[int]
     */
    protected $_customers_queued_for_update = array();

    /**
     * @var bool[int]
     */
    protected $_customer_addresses_queued_for_update = array();

    /**
     * Returns whether a customer account has been queued for update
     *
     * @param int $customer_entity_id
     * @return bool
     */
    public function hasCustomerBeenQueued($customer_entity_id)
    {
        return isset($this->_customers_queued_for_update[$customer_entity_id]);
    }

    /**
     * Flags a customer account as having been queued for updates
     *
     * @param int $customer_entity_id
     */
    public function customerHasBeenQueued($customer_entity_id)
    {
        $this->_customers_queued_for_update[$customer_entity_id] = true;
    }

    /**
     * Returns whether a customer address has been queued for update
     *
     * @param int $customer_address_entity_id
     * @return bool
     */
    public function hasCustomerAddressBeenQueued($customer_address_entity_id)
    {
        return isset($this->_customer_addresses_queued_for_update[$customer_address_entity_id]);
    }

    /**
     * Flags a customer account as already having been queued for update
     *
     * @param int $customer_address_entity_id
     */
    public function customerAddressHasBeenQueued($customer_address_entity_id)
    {
        $this->_customer_addresses_queued_for_update[$customer_address_entity_id] = true;
    }
}
