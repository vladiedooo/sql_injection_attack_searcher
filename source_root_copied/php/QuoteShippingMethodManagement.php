<?php

namespace Scc\Shipping\Plugin\Model;


class QuoteShippingMethodManagement
{
    /**
     * Scope Config Interface
     * @var \Magento\Framework\App\Config\ScopeConfigInterface $_scopeConfig
     */
    protected $_scopeConfig;

    /**
     * @param \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig $_scopeConfig Scope Config
     */
    public function __construct(
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
    ) {
        $this->_scopeConfig = $scopeConfig;
    }

    /**
     * Allow empty quote for calculating the shipping methods / costs
     *
     * Plugin for: @see \Magento\Quote\Model\ShippingMethodManagement::estimateByAddress
     *
     * @param $interceptor
     * @param $rates
     */
    public function afterEstimateByAddress(
        $interceptor,
        $rates
    ) {
        return $this->_markRates($rates);
    }

    /**
     * Allow empty quote for calculating the shipping methods / costs
     *
     * Plugin for: @see Magento\Quote\Api\GuestShipmentEstimationInterface::estimateByExtendedAddress
     *
     * @param $interceptor
     * @param $rates
     */
    public function afterEstimateByExtendedAddress($interceptor, $rates)
    {
        return $this->_markRates($rates);
    }

    /**
     * Marks rates
     *
     * @param $rates
     */
    protected function _markRates($rates)
    {
        $methodsToMark = explode(
            ',',
            $this->_scopeConfig->getValue(
                'shipping/scc_shipping_shipdate_message/used_methods',
                \Magento\Store\Model\ScopeInterface::SCOPE_STORE
            )
        );

        foreach ($rates as &$rate)
        {
            if ($rate)
            {
                $fullMethodCode = $rate->getCarrierCode() . '_' . $rate->getMethodCode();
                if (in_array($fullMethodCode, $methodsToMark))
                {
                    $title = $rate->getMethodTitle();
                    if ($title && strpos($title, '*') === FALSE)
                    {
                        $rate->setMethodTitle($rate->getMethodTitle() . '*');
                    }
                }
            }
        }

        return $rates;
    }
}