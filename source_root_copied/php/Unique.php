<?php
/**
 * Author: Sean Dunagan (https://github.com/dunagan5887)
 * Created: 4/12/16
 */

namespace Dunagan\ProcessQueue\Model\Task;

/**
 * Author: Sean Dunagan (https://github.com/dunagan5887)
 * Created: 4/20/16
 *
 * Class Unique
 * @package Dunagan\ProcessQueue\Model\Task
 *
 * This is the ORM model class for the Unique Process Queue task table
 *
 * @method \Dunagan\ProcessQueue\Model\ResourceModel\Task\Unique getResource()
 */
class Unique extends \Dunagan\ProcessQueue\Model\Task
{
    const UNIQUE_ID_FIELD = 'unique_id';

    /**
     * Initialize resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('Dunagan\ProcessQueue\Model\ResourceModel\Task\Unique');
    }
}
