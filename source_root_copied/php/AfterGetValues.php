<?php
/**
 * Author: Sean Dunagan (github: dunagan5887)
 * Date: 12/7/16
 */

namespace Scc\Salesrule\Observer\Rule\Metadata;

use Magento\Framework\Event\ObserverInterface;

/**
 * Class AfterGetValues
 * @package Scc\Salesrule\Observer\Rule\Metadata
 */
class AfterGetValues implements ObserverInterface
{
    /**
     * @var \Scc\Salesrule\Helper\Rule\Action\Apply\Options
     */
    protected $_ruleActionApplyOptionsHelper;

    /**
     * @param \Scc\Salesrule\Helper\Rule\Action\Apply\Options $ruleActionApplyOptionsHelper
     */
    public function __construct(\Scc\Salesrule\Helper\Rule\Action\Apply\Options $ruleActionApplyOptionsHelper)
    {
        $this->_ruleActionApplyOptionsHelper = $ruleActionApplyOptionsHelper;
    }

    /**
     *
     *
     * @param \Magento\Framework\Event\Observer $observer
     * @return $this
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        $transportObject = $observer->getData('transport_object');
        // Ensure that the transport object was set on the $observer
        if(!is_object($transportObject))
        {
            // This should never happen, but account for the fact where it does
            return;
        }
        // Get the metadata values array
        $returned_metadata_values_array = $transportObject->getMetadataValues();
        // Ensure that this is an array
        if (!is_array($returned_metadata_values_array))
        {
            // This should never happen, but account for the fact where it does
            return;
        }
        // Add the custom action Apply options to the metadata array
        $returned_metadata_values_array =
            $this->_ruleActionApplyOptionsHelper
                 ->addAdditionalActionApplyOptionsToMetadataValuesArray($returned_metadata_values_array);
        // Set the potentially mutated metadata values array back on the $transportObject
        $transportObject->setMetadataValues($returned_metadata_values_array);
    }
}
