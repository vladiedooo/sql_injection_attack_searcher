<?php
/**
 * Author: Sean Dunagan (https://github.com/dunagan5887)
 * Created: 4/21/16
 */

namespace Dunagan\ProcessQueue\Controller\Adminhtml\Task\Unique;

/**
 * Author: Sean Dunagan (https://github.com/dunagan5887)
 * Created: 4/22/16
 *
 * Class Execute
 * @package Dunagan\ProcessQueue\Controller\Adminhtml\Task\Unique
 *
 * This controller class takes care of implementing functionality to manually trigger execution of a unique task
 *  regardless of its last_executed_at value
 */
class Execute
    extends \Dunagan\ProcessQueue\Controller\Adminhtml\Task\Execute
    implements \Dunagan\Base\Controller\Adminhtml\GridAndFormControllerInterface
{
    /**
     * {@inheritdoc}
     */
    public function getModelClass()
    {
        return 'Dunagan\ProcessQueue\Model\Task\Unique';
    }

    /**
     * {@inheritdoc}
     */
    public function getTaskProcessorClass()
    {
        return '\Dunagan\ProcessQueue\Helper\Task\Unique\Processor';
    }
}
