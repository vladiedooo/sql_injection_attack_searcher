<?php
/**
 * Author: Sean Dunagan (github: dunagan5887)
 * Date: 2/16/17
 */

namespace Ideal\SKProducts\Setup\Data\Options\Socket\Type;

/**
 * Class Specialty
 * @package Ideal\SKProducts\Setup\Data\Options\Socket\Speciality
 */
class Specialty
{
    /**
     * @return array
     */
    public function getOptionArray()
    {
        return array('values' => array('Ball Joint', 'Bit Holder', 'Bud Wheel', 'Female Torx', 'Hex', 'Hex Ball',
                                       'Phillips', 'Pipe Plug', 'Pozidriv', 'Slotted', 'Spanner', 'Spark Plug',
                                       'Tamper Proof Torx', 'Tamper-Proof Hex', 'Tie', 'Rod', 'Torx', 'Torx Plus',
                                       'Triple Square', 'Turbo Socket', 'Universal Joint', 'Clutch', 'Robertson',
                                       'Turbo', 'Drive Wheel', 'Hex', 'Tie Rod'));
    }
}
