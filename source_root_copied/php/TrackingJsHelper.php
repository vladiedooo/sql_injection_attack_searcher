<?php
/**
 * Author: Sean Dunagan (github: dunagan5887)
 * Date: 3/8/17
 */

namespace Scc\Analytics\Helper\Js;

/**
 * Class TrackingJsHelper
 *
 * @package Scc\Analytics\Helper\Js
 */
class TrackingJsHelper
{
    const REGISTERED_ANALYTICS_INTEGRATION_PREFIXES_CONFIG_PATH = 'scc_analytics/registered_analytics_integration_prefixes';
    const ACTION_EVENT_JS_TRACKING_SNIPPETS_SESSION_VARIABLE = 'scc_analytics_action_event_js_tracking_snippets';
    const ACTION_EVENT_JS_SNIPPET_CONFIG_PATH_TEMPLATE = '%s_scc_analytics_tracking/page_js_snippets/%s';

    /**
     * @var \Magento\Framework\App\Config\ScopeConfigInterface
     */
    protected $_scopeConfig;

    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $_storeManager;

    /**
     * @var \Magento\Customer\Model\Session
     */
    protected $_customerSession;

    /**
     * @var \Magento\Framework\Event\ManagerInterface
     */
    protected $_eventManager;

    /**
     * @var \Magento\Framework\ObjectManagerInterface
     */
    protected $_objectManager;

    /**
     * @var array
     */
    protected $_action_event_data = array();

    /**
     * Flag that an action event which needs to be tracked by Analytics occurred
     *
     * @param string $action_event      - Name of the event which occurred
     * @param array  $action_event_data - Any data specific to this particular instance of the event
     *                                      CURRENTLY: This is not yet used, but someday would be used to pass in data
     *                                                  such as the customer who just registered, or the item which was
     *                                                  just added to the cart - Sean Dunagan 2017/03/12
     */
    public function flagActionEventOccurred($action_event, $action_event_data = array())
    {
        // Get the javascript snippets which should be executed for tracking this event
        $javascript_snippets_to_execute = $this->getJsSnippetsToFlagForEvent($action_event, $action_event_data);

        foreach ($javascript_snippets_to_execute as $integration_prefix => $javascript_snippet_to_execute)
        {
            // Compute a hashed id for this particular instance of this event
            $event_hashed_id = $this->getEventHashedId($action_event, $integration_prefix, $action_event_data);
            // Set the javascript snippet in the customer session so that the next call to customer/section/load returns
            //      this javascript snippet to the frontend
            $this->setEventJsTrackingSnippetInCustomerSession($event_hashed_id, $javascript_snippet_to_execute);
        }
    }

    /**
     * CURRENTLY: For each analytics integration which has defined the need for functionality on this event, return a
     *              javascript snippet defined for this event. There is no data particular to this instance of the event
     *              currently entered into the snippet. The snippet is returned as it is
     *              entered into the admin panel configuration setting field
     *              - Sean Dunagan 2017/05/12
     *
     * EVENTUALLY: Will take the $action_event_data parameter (which contains data particular to this instance of the
     *              action event) and enter its prominent data points into the javascript tracking snippets
     *              - Sean Dunagan 2017/03/12
     *
     * @param string $action_event      - Name of the event which occurred
     * @param array  $action_event_data - Any data specific to this particular instance of the event
     *
     * @return array
     */
    public function getJsSnippetsToFlagForEvent($action_event, $action_event_data = array())
    {
        $js_snippets_to_return = array();

        // Get the analytics prefixes which have been registered in the system
        $registered_prefixes = $this->getRegisteredAnalyticsIntegrationPrefixes();
        foreach ($registered_prefixes as $analytics_integration_prefix)
        {
            // For each analytics prefix registered in the system, get the respective analytics tracking snippet
            $config_path = sprintf(self::ACTION_EVENT_JS_SNIPPET_CONFIG_PATH_TEMPLATE, $analytics_integration_prefix,
                                   $action_event);

            $store_view_code = $this->_storeManager->getStore()->getCode();

            $js_snippet = $this->_scopeConfig->getValue($config_path, \Magento\Store\Model\ScopeInterface::SCOPE_STORE,
                                                        $store_view_code);

            // If the $action_event_data parameter has been defined here, we need to populate the templated values in
            //      the JS snippet with the data points provided in the $action_event_data array
            // We will do this via firing an event since not all tracked events have templated values
            $transportObject = $this->_objectManager->create('\Magento\Framework\DataObject');
            $transportObject->setActionEvent($action_event);
            $transportObject->setActionEventData($action_event_data);
            $transportObject->setJsSnippet($js_snippet);
            $transportObject->setStoreViewCode($store_view_code);

            $eventParameters = array('transport_object' => $transportObject);
            $event_name = sprintf('scc_analytics_tracking_populate_snippet_%s', $action_event);
            $this->_eventManager->dispatch($event_name, $eventParameters);
            $js_snippet = $transportObject->getJsSnippet();

            if (!empty($js_snippet))
            {
                $js_snippets_to_return[$analytics_integration_prefix] = $js_snippet;
            }
        }

        return $js_snippets_to_return;
    }

    /**
     * CURRENTLY: Returns a hash describing the action event. This is simply a concatenation of the integration_prefix,
     *              action_event and the current microtime
     *              - Sean Dunagan 2017/05/12
     *
     * EVENTUALLY: This will take the identifying data from $action_event_data parameter (e.g. customer entity id or
     *                  quote item id) and create a hash which would be unique to this instance of this event. In the
     *                  event that a tracking snippet is not successfully executed on the frontend for any reason, the
     *                  frontend could transmit this hash id to the backend to note that the tracking did not occur, and
     *                  potentially re-send the js tracking snippet to the frontend
     *                  - Sean Dunagan 2017/05/12
     *
     * @param string $action_event       - Name of the event which occurred
     * @param string $integration_prefix -
     * @param array  $action_event_data  - Any data specific to this particular instance of the event
     *
     * @return string
     */
    public function getEventHashedId($action_event, $integration_prefix, $action_event_data = array())
    {
        return $integration_prefix . '_' . $action_event . '_' . microtime(true);
    }

    /**
     * Update the customer session to reflect that an action event occurred which we need to execute a tracking snippet
     *  for. These tracking snippets will live in the customer session until a call to /customer/section/load has been
     *  made to retrieve these snippets (should be part of the same post-submit-and-302-redirect request processing
     *  which results in these events being recorded in the customer session to begin with)
     */
    protected function setEventJsTrackingSnippetInCustomerSession($event_hashed_id, $javascript_snippet_to_execute)
    {
        // Check to see if there is already an event in the session
        $action_event_tracking_snippets_in_session_as_array = $this->getActionEventJsTrackingSnippets(true);
        if (!is_array($action_event_tracking_snippets_in_session_as_array))
        {
            $action_event_tracking_snippets_in_session_as_array = array();
        }

        $action_event_tracking_snippets_in_session_as_array[$event_hashed_id] = $javascript_snippet_to_execute;

        $action_event_tracking_snippets_in_session_as_json
            = json_encode($action_event_tracking_snippets_in_session_as_array);
        $this->_customerSession->setData(self::ACTION_EVENT_JS_TRACKING_SNIPPETS_SESSION_VARIABLE,
                                         $action_event_tracking_snippets_in_session_as_json);
    }

    /**
     * Return the JS Tracking snippets which have been flagged in the customer session for events which require
     *  Analytics tracking.
     * This method will also clear all set action event flags to prevent them from being passed to the frontend again
     *  in the future
     *
     * @return string
     */
    public function processActionEventFlagJsTrackingSnippets()
    {
        $action_event_tracking_snippets_in_session_as_array = $this->getActionEventJsTrackingSnippets(true);
        $this->clearActionEventFlags();
        return $action_event_tracking_snippets_in_session_as_array;
    }

    /**
     * Returns the JS tracking snippets which have been flagged as needing to be executed
     *
     * @param bool $as_array - If true, will return the JS tracking snippets as an array
     *                         If false, will return the JS tracking snippets as a string (presumed to be a json)
     *
     * @return string|array
     */
    public function getActionEventJsTrackingSnippets($as_array = false)
    {
        $action_event_tracking_snippets_in_session_as_json
            = $this->_customerSession->getData(self::ACTION_EVENT_JS_TRACKING_SNIPPETS_SESSION_VARIABLE);

        if (empty($action_event_tracking_snippets_in_session_as_json))
        {
            $action_event_tracking_snippets_in_session_as_json = '[]';
        }

        if ($as_array)
        {
            $action_event_tracking_snippets_in_session_as_json
                = json_decode($action_event_tracking_snippets_in_session_as_json, true);

            return $action_event_tracking_snippets_in_session_as_json;
        }

        return $action_event_tracking_snippets_in_session_as_json;
    }

    /**
     * Clear the action event flags from the customer session. It is assumed that this will only be done when the
     *  js snippets are communicated to the frontend
     */
    public function clearActionEventFlags()
    {
        // Update the session variable
        $this->_customerSession->setData(self::ACTION_EVENT_JS_TRACKING_SNIPPETS_SESSION_VARIABLE, '[]');
    }

    /**
     * Returns the Analytics Integrations which have been registered in modules' config.xml files
     *
     * @return array
     */
    public function getRegisteredAnalyticsIntegrationPrefixes()
    {
        $registered_analytics_prefixes_as_array_keys
            = $this->_scopeConfig->getValue(self::REGISTERED_ANALYTICS_INTEGRATION_PREFIXES_CONFIG_PATH);

        return array_keys($registered_analytics_prefixes_as_array_keys);
    }

    /**
     * Options constructor.
     * @param \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     * @param \Magento\Customer\Model\Session $customerSession
     * @param \Magento\Framework\Event\ManagerInterface $eventManager
     * @param \Magento\Framework\ObjectManagerInterface $objectManager
     */
    public function __construct(\Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
                                \Magento\Store\Model\StoreManagerInterface $storeManager,
                                \Magento\Customer\Model\Session $customerSession,
                                \Magento\Framework\Event\ManagerInterface $eventManager,
                                \Magento\Framework\ObjectManagerInterface $objectManager)
    {
        $this->_scopeConfig = $scopeConfig;
        $this->_storeManager = $storeManager;
        $this->_customerSession = $customerSession;
        $this->_eventManager = $eventManager;
        $this->_objectManager = $objectManager;
    }
}
