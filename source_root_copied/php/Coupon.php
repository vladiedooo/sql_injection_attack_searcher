<?php

namespace Scc\Customers\Block;

use \Magento\Framework\Api\SearchCriteriaBuilder;
use \Magento\Customer\Api\CustomerRepositoryInterface;
use \Magento\Framework\Api\FilterBuilder;
use \Magento\Framework\Api\SortOrder;
use \Magento\Framework\Api\SortOrderBuilder;


class Coupon extends \Magento\Framework\View\Element\Template
{
    protected $customerSession;
    private $customerRepository;
    /** @var SearchCriteriaBuilder */
    private $searchCriteriaBuilder;
    private $filterBuilder;
    private $ruleRepository;
    private $sortOrderBuilder;
    private $orderCollection;
    private $orderItemCollection;
    private $ruleInfo;
    private $orderRuleInfo;
    private $date;
    private $objectManager;

    /**
     * @var \Magento\Eav\Model\Config
     */
    protected $_eavConfig;

    /**
     * Construct
     *
     * @param \Magento\Framework\View\Element\Template\Context $context
     * @param \Magento\Customer\Model\Session $customerSession
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Magento\Customer\Model\Session $customerSession,
        \Magento\Customer\Api\CustomerRepositoryInterface $customerRepository,
        \Magento\SalesRule\Model\RuleRepository $ruleRepository,
        \Magento\Framework\Api\SearchCriteriaBuilder $searchCriteriaBuilder,
        \Magento\Framework\Api\FilterBuilder $filterBuilder,
        \Magento\Framework\Api\SortOrderBuilder $sortOrderBuilder,
        \Magento\Eav\Model\Config $eavConfig,
        \Magento\Sales\Model\ResourceModel\Order\Collection $orderCollection,
        \Magento\Sales\Model\ResourceModel\Order\Item\Collection $orderItemCollection,
        \Magento\Framework\ObjectManagerInterface $objectManager,
        \Magento\Framework\Stdlib\DateTime\DateTime $date,
        array $data = []
    ) {
        parent::__construct($context, $data);
        $this->customerSession = $customerSession;
        $this->customerRepository = $customerRepository;
        $this->searchCriteriaBuilder = $searchCriteriaBuilder;
        $this->filterBuilder = $filterBuilder;
        $this->ruleRepository = $ruleRepository;
        $this->sortOrderBuilder = $sortOrderBuilder;
        $this->_eavConfig = $eavConfig;
        $this->orderCollection = $orderCollection;
        $this->orderItemCollection = $orderItemCollection;
        $this->date = $date;
        $this->objectManager = $objectManager;
    }

    public function getRules()
    {
        $customer = $this->customerRepository->getById($this->customerSession->getCustomer()->getId());
        $dobMonth = $customer->getCustomAttribute('dob_month');
        $dobMonthAttributeObject = $this->_eavConfig->getAttribute('customer', 'dob_month');
        $dobMonthAttributeOptions = $dobMonthAttributeObject->getSource()->getAllOptions();
        $groupId = $customer->getGroupId();
        $dobMonthLabel = '';
        $ruleIds = $this->_getRuleIdsUsedForCustomer();

        foreach ($dobMonthAttributeOptions as $option)
        {
            if ($option['value'] == $dobMonth->getValue())
            {
                $dobMonthLabel = strtolower($option['label']);
            }
        }

        $this->searchCriteriaBuilder->addFilter('is_active', 1, 'eq');
        $this->searchCriteriaBuilder->addFilter(
            'coupon_type',
            \Magento\SalesRule\Model\Rule::COUPON_TYPE_NO_COUPON,
            'neq'
        );

        $sortOrders = [
            $this->sortOrderBuilder
                ->setField('sort_order')
                ->setDirection(\Magento\Framework\Api\SortOrder::SORT_ASC)->create()
        ];
        $this->searchCriteriaBuilder->setSortOrders($sortOrders);

        $searchCriteria = $this->searchCriteriaBuilder->create();
        $searchResults = $this->ruleRepository->getList($searchCriteria)->getItems();
        $items = array();
        $coupons = array();

        foreach ($searchResults as $rule)
        {
            if (in_array($groupId, $rule->getCustomerGroupIds()))
            {
                if ($rule->getFromDate())
                {
                    if ($rule->getFromDate() > $this->date->date('Y-m-d'))
                    {
                        continue;
                    }
                }

                if ($rule->getToDate())
                {
                    if ($rule->getToDate() < $this->date->date('Y-m-d'))
                    {
                        continue;
                    }
                }

                $coupon = null;

                $collection = $this->objectManager->create('\Magento\SalesRule\Model\ResourceModel\Coupon\Collection')
                    ->addFieldToFilter('rule_id', array('eq' => $rule->getRuleId()))
                    ->addFieldToFilter('is_primary', array('eq' => 1));

                foreach ($collection as $object)
                {
                    if ($object->getExpirationDate())
                    {
                        /*if ($object->getExpirationDate() < $this->date->date('Y-m-d 00:00:00'))
                        {
                            continue;
                        }*/
                    }
                    $coupon = $object;
                    break(1);
                }

                if ($rule->getUsesPerCustomer())
                {
                    if (
                        array_key_exists($rule->getRuleId(), $ruleIds)
                        AND
                        $rule->getUsesPerCustomer() >= count($this->orderRuleInfo[$rule->getRuleId()])
                    )
                    {
                        continue;
                    }
                }

                if ($rule->getUsesPerCoupon())
                {
                    if ($coupon)
                    {
                        if (
                            $rule->getTimesUsed()
                            AND
                            $rule->getTimesUsed() >= $rule->getUsesPerCoupon())
                        {
                            continue;
                        }
                    }
                }

                if ($dobMonthLabel)
                {
                    if (strpos(strtolower($rule->getName()), 'birthday month coupon') !== FALSE)
                    {
                        if (strpos(strtolower($rule->getName()), $dobMonthLabel) === 0)
                        {
                            if (array_key_exists($rule->getRuleId(), $ruleIds))
                            {
                                continue;
                            }
                        }
                        else
                        {
                            continue;
                        }
                    }
                }
                else
                {
                    if (strpos(strtolower($rule->getName()), 'birthday month coupon') !== FALSE)
                    {
                        //Don't show birthday month coupons if they're not available
                        continue;
                    }
                }

                $items[] = $rule;
                $coupons[$rule->getRuleId()] = $coupon;
                unset($collection);
            }
        }

        return array('items' => $items, 'coupons' => $coupons);

    }

    protected function _getRuleIdsUsedForCustomer()
    {
        if (empty($this->ruleInfo))
        {
            $this->ruleInfo = [];
            $this->orderRuleInfo = [];
            $collection = $this->orderCollection
                ->addFieldToSelect('entity_id')
                ->addFieldToFilter('customer_id', ['eq' => $this->customerSession->getCustomer()->getId()]);
            $orderIds = [];

            if ($collection->getSize())
            {
                foreach ($collection as $order)
                {
                    $orderIds[] = $order->getId();
                }

                if (count($orderIds))
                {
                    $itemCollection = $this->orderItemCollection
                        ->addFieldToSelect('applied_rule_ids')
                        ->addFieldToFilter('applied_rule_ids', ['notnull' => ''])
                        ->addFieldToFilter('order_id', ['in' => $orderIds]);

                    if ($itemCollection->getSize())
                    {
                        foreach ($itemCollection as $item)
                        {
                            $itemRuleIds = explode(',', $item->getAppliedRuleIds());

                            foreach ($itemRuleIds as $id)
                            {
                                if ( ! in_array($id, $this->ruleInfo))
                                {
                                    $this->ruleInfo[$id] = 1;
                                    $this->orderRuleInfo[$id][] = $id;
                                }
                                else
                                {
                                    $this->ruleInfo[$id] += 1;
                                    $this->orderRuleInfo[$id][] = $id;
                                }
                            }
                        }
                    }
                }

            }
        }

        return $this->ruleInfo;
    }
}