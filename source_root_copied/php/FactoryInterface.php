<?php
/**
 * Author: Sean Dunagan (github: dunagan5887)
 * Date: 4/25/17
 */

namespace Scc\Campaign\Model\Api\Url\SendEmailJssp;

/**
 * The SendEmail JSSP expects an HTTP Get request. As such, we will implement these API calls using a cURL client
 *
 * Interface FactoryInterface
 *
 * @package Scc\Campaign\Model\Api\Url\SendEmailJssp
 */
interface FactoryInterface extends \Scc\Campaign\Model\Api\Url\FactoryInterface
{
    /**
     * Allows the Api Service to reference the proper recipient email field to set in $email_transmission_parameters
     *  argument in call to getUrlForApiCall($email_transmission_parameters)
     *
     * @return string
     */
    public function getExpectedRecipientEmailArgument();

    /**
     * Allows the Api Service to reference the proper email template id field to set in $email_transmission_parameters
     *  argument in call to getUrlForApiCall($email_transmission_parameters)
     *
     * @return string
     */
    public function getExpectedEmailTemplateIdArgument();
}