<?php
/**
 * Author: Sean Dunagan (github: dunagan5887)
 * Date: 2/16/17
 */

namespace Ideal\SKProducts\Setup\Data\Options\Socket;

/**
 * Class Depth
 * @package Scc\SKProducts\Setup\Data\Options\Socket
 */
class Depth
{
    /**
     * @return array
     */
    public function getOptionArray()
    {
        return array('values' => array('Deep', 'Extra Deep', 'Semi Deep', 'Standard', 'Drive Line'));
    }
}
