<?php
/**
 * Author: Sean Dunagan (https://github.com/dunagan5887)
 * Created: 4/13/16
 */

namespace Dunagan\Base\Controller\Adminhtml;

/**
 * Author: Sean Dunagan (https://github.com/dunagan5887)
 * Created: 4/19/16
 *
 * Class BaseController
 * @package Dunagan\Base\Controller\Adminhtml
 *
 * This class provides very abstract functionality to set up an admin panel page
 */
abstract class BaseController extends \Magento\Backend\App\Action implements ControllerInterface
{
    const GENERIC_CUSTOMER_FACING_MESSAGE = 'There was an error with your request. Please try again';

    /**
     * Core registry
     *
     * @var \Magento\Framework\Registry
     */
    protected $_coreRegistry = null;

    /**
     * @param \Magento\Backend\App\Action\Context $context
     * @param \Magento\Framework\Registry $registry
     */
    public function __construct(\Magento\Backend\App\Action\Context $context, \Magento\Framework\Registry $registry)
    {
        $this->_coreRegistry = $registry;
        parent::__construct($context);
    }

    /**
     * Initializes the action and renders layout. This method isn't anticipated to be used too often
     */
    public function executeBaseAction()
    {
        $this->_initAction();
        $this->_view->renderLayout();
    }

    /**
     * This method provides abstract functionality to set up the page title and header. Also sets the active menu
     *      value
     */
    protected function _initAction()
    {
        $this->_coreRegistry->register('current_action_controller', $this);

        $this->_view->loadLayout();
        // Set the active menu
        $active_menu = $this->getActiveMenu();
        if (!empty($active_menu))
        {
            $this->_setActiveMenu($active_menu);
        }
        // Set the necessary breadcrumbs
        $breadcrumbs_array = $this->getBreadcrumbsArray();
        foreach($breadcrumbs_array as $breadcrumb)
        {
            $this->_addBreadcrumb(__($breadcrumb), __($breadcrumb));
        }
        // Set the page title values to prepend
        $page_title = $this->getPageTitle();
        if (!empty($page_title))
        {
            $this->_view->getPage()->getConfig()->getTitle()->prepend(__($page_title));
        }
    }

    /**
     * Subclasses are expected to override this method to declare the active menu value for the page.
     * This defines which menu item on the left-hand nav menu in the admin panel will be highlighted
     *
     * @return string|null
     */
    public function getActiveMenu()
    {
        return null;
    }

    /**
     * Subclasses are expected to override this method
     * This method returns breadcrumb values which would be set on the page. The abstract Magento classes define
     *      functionality for setting these values, but to this point I am unsure how/if they get rendered
     *
     * @return array
     */
    public function getBreadcrumbsArray()
    {
        return array();
    }

    /**
     * Subclasses are expected to override this method
     * This method should return the title of the page in the admin panel
     *
     * @return string|null
     */
    public function getPageTitle()
    {
        return null;
    }

    /**
     * Subclasses are expected to override this method
     * This method defines the ACL configuration resource which controls access to the page
     *
     * @return string|null
     */
    public function getAclConfigResource()
    {
        return null;
    }

    /**
     * This method provides functionality to determine whether the current admin user should have access to view the
     *  page
     *
     * @return bool
     */
    protected function _isAllowed()
    {
        $acl_config = $this->getAclConfigResource();
        if (!empty($acl_config))
        {
            return $this->_authorization->isAllowed($acl_config);
        }
        return parent::_isAllowed();
    }
}
