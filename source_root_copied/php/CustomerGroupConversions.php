<?php
/**
 * Author: Sean Dunagan (github: dunagan5887)
 * Date: 2/17/17
 */

namespace Scc\PremiumMembership\Helper\CustomerSegment;

/**
 * Class CustomerGroupConversions
 * @package Scc\PremiumMembership\Helper\CustomerSegment
 */
class CustomerGroupConversions
{
    const CUSTOMER_SEGMENT_GROUP_CONVERSIONS_CONFIG_PATH = 'premium_customer_segment_customer_group_conversions';

    /**
     * @var null|array
     */
    protected $_customer_segment_group_conversions = null;

    /**
     * @var null|array
     */
    protected $_customer_group_id_to_name_mapping = null;

    /**
     * @var \Magento\Framework\App\Config\ScopeConfigInterface
     */
    protected $_scopeConfig;

    /**
     * @var \Scc\CustomerSegment\Model\ResourceModel\Segment
     */
    protected $_customerSegmentResourceSingleton;

    /**
     * @var \Scc\Customer\Model\ResourceModel\Group
     */
    protected $_sccCustomerGroupResourceModel;

    /**
     * @var \Scc\Customer\Model\ResourceModel\Customer
     */
    protected $_sccCustomerResourceSingleton;

    /**
     * @param int $segment_id
     * @param array $customer_ids
     * @param bool $adding_to_segment
     */
    public function executeCustomerSegmentConversions($segment_id, $customer_ids, $adding_to_segment = true)
    {
        if (empty($customer_ids))
        {
            return;
        }

        $customer_segment_conversions = $this->_getCustomerSegmentGroupConversions();
        if (empty($customer_segment_conversions))
        {
            return;
        }

        $customer_segment_name = $this->_customerSegmentResourceSingleton->getSegmentNameById($segment_id);
        if (empty($customer_segment_name))
        {
            return;
        }

        foreach($customer_segment_conversions as $customer_segment_conversion)
        {
            $conversion_segment_name = isset($customer_segment_conversion['segment_name'])
                                        ? $customer_segment_conversion['segment_name'] : null;
            // Check to see if this customer_segment_conversion applies to the customer segment passed in
            if (strcmp($customer_segment_name, $conversion_segment_name))
            {
                continue;
            }

            $segment_conversions_array = isset($customer_segment_conversion['conversions'])
                                            ? $customer_segment_conversion['conversions'] : null;
            if (empty($segment_conversions_array))
            {
                continue;
            }

            foreach($segment_conversions_array as $conversion_code => $conversion_array)
            {
                $this->_executeSegmentConversions($conversion_array, $customer_ids, $adding_to_segment);
            }
        }
    }

    /**
     * @param array $segment_conversions_array
     * @param array $customer_ids
     * @param bool $adding_to_premium_segment
     */
    protected function _executeSegmentConversions($segment_conversions_array, $customer_ids,
                                                  $adding_to_premium_segment = true)
    {
        $conversion_to_index = $adding_to_premium_segment ? 'premium_group' : 'basic_group';
        $conversion_from_index = $adding_to_premium_segment ? 'basic_group' : 'premium_group';
        $conversion_to_group_name = isset($segment_conversions_array[$conversion_to_index])
                                        ? $segment_conversions_array[$conversion_to_index] : null;
        $conversion_from_group_name = isset($segment_conversions_array[$conversion_from_index])
                                        ? $segment_conversions_array[$conversion_from_index] : null;

        if ((empty($conversion_to_group_name)) || (empty($conversion_from_group_name)))
        {
            return;
        }

        $customer_group_ids_to_names_mapping = $this->_getCustomerGroupIdToNameMapping();
        $conversion_to_group_id = array_search($conversion_to_group_name, $customer_group_ids_to_names_mapping);
        $conversion_from_group_id = array_search($conversion_from_group_name, $customer_group_ids_to_names_mapping);
        if (($conversion_to_group_id === FALSE) || ($conversion_from_group_id === FALSE))
        {
            return;
        }

        // Execute a query to affect the customer group conversions
        $this->_convertCustomersToGroup($customer_ids, $conversion_to_group_id, $conversion_from_group_id);
    }

    /**
     * @param array $customer_ids
     * @param int $customer_group_id_to_convert_to
     * @param int $customer_group_id_to_convert_from
     */
    protected function _convertCustomersToGroup($customer_ids, $customer_group_id_to_convert_to,
                                                $customer_group_id_to_convert_from)
    {
        $this->_sccCustomerResourceSingleton
             ->convertCustomerGroupIdsByCustomerIds($customer_ids, $customer_group_id_to_convert_to,
                                                    $customer_group_id_to_convert_from);
    }

    /**
     * @return array
     */
    protected function _getCustomerGroupIdToNameMapping()
    {
        if (is_null($this->_customer_group_id_to_name_mapping))
        {
            $this->_customer_group_id_to_name_mapping
                = $this->_sccCustomerGroupResourceModel->getCustomerGroupIdToNameMappingArray();
        }
    
        return $this->_customer_group_id_to_name_mapping;
    }
    
    /**
     * @return array
     */
    protected function _getCustomerSegmentGroupConversions()
    {
        if (is_null($this->_customer_segment_group_conversions))
        {
            $this->_customer_segment_group_conversions
                = $this->_scopeConfig->getValue(self::CUSTOMER_SEGMENT_GROUP_CONVERSIONS_CONFIG_PATH);
        }
    
        return $this->_customer_segment_group_conversions;
    }

    /**
     * Options constructor.
     * @param \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
     * @param \Scc\CustomerSegment\Model\ResourceModel\Segment $customerSegmentResourceSingleton
     * @param \Scc\Customer\Model\ResourceModel\Group $sccCustomerGroupResourceModel
     * @param \Scc\Customer\Model\ResourceModel\Customer $sccCustomerResourceSingleton
     */
    public function __construct(\Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
                                \Scc\CustomerSegment\Model\ResourceModel\Segment $customerSegmentResourceSingleton,
                                \Scc\Customer\Model\ResourceModel\Group $sccCustomerGroupResourceModel,
                                \Scc\Customer\Model\ResourceModel\Customer $sccCustomerResourceSingleton)
    {
        $this->_scopeConfig = $scopeConfig;
        $this->_customerSegmentResourceSingleton = $customerSegmentResourceSingleton;
        $this->_sccCustomerGroupResourceModel = $sccCustomerGroupResourceModel;
        $this->_sccCustomerResourceSingleton = $sccCustomerResourceSingleton;
    }
}
