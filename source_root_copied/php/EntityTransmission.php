<?php
/**
 * Author: Sean Dunagan (github: dunagan5887)
 * Date: 9/21/16
 */

namespace Dunagan\WebApi\Model\Service\Transmission;

/**
 * Abstract class to define functionality for Web API calls which transmit data to an external system
 *
 * Class EntityTransmission
 * @package Dunagan\WebApi\Model\Service\Transmission
 */
abstract class EntityTransmission implements EntityTransmissionInterface
{
    const ERROR_INVALID_ENTITY = 'Entity passed to %1::%2 is not a loaded object from the Magento database: %3';

    /**
     * Validates that all necessary transmission arguments are set on the $additional_transmission_arguments
     *  parameter. Should throw an exception if any required arguments are not provided
     * If no exception is thrown by this method, it should be assumed that $additional_transmission_arguments
     *  contains all necessary arguments
     *
     * @param array|mixed $additional_transmission_arguments - Most likely, this will be an array, but we don't require
     *                                                          it to be an array to allow for greater flexibility for
     *                                                          subclasses
     * @return void
     * @throws \Dunagan\WebApi\Model\Exception\Service\Transmission\RequiredArguments
     */
    abstract protected function _validateRequiredTransmissionArguments($additional_transmission_arguments);

    /**
     * Returns the Web Api method which should be used based on the entity and $additional_transmission_arguments
     *  provided
     *
     * @param mixed $entity
     * @param mixed $additional_transmission_arguments
     * @return string
     */
    abstract protected function _getWebApiCallMethod($entity, $additional_transmission_arguments);

    /**
     * Returns the parameters which should be used for the Web API call based on the $entity and
     *  $additional_transmission_arguments arguments
     *
     * @param mixed $entity
     * @param mixed $additional_transmission_arguments
     * @return mixed
     */
    abstract protected function _getWebApiCallParameters($entity, $additional_transmission_arguments);

    /**
     * Returns the web api client which should be used for the Web API call
     *
     * @return \Dunagan\WebApi\Model\ClientInterface
     */
    abstract protected function _getWebApiClient();

    /**
     * THE CALLING BLOCK IS EXPECTED TO CATCH ALL EXCEPTIONS THROWN BY THIS METHOD
     *
     * {@inheritdoc}
     *
     * @throws \Dunagan\WebApi\Model\Exception\Service\Transmission\RequiredArguments
     */
    public function transmitEntityViaWebApi($entity, $additional_transmission_arguments)
    {
        // Validate the required data points were passed in
        $this->_validateEntityToTransmit($entity);
        $this->_validateRequiredTransmissionArguments($additional_transmission_arguments);
        // Get the Web API call method and parameters to use
        $web_api_method = $this->_getWebApiCallMethod($entity, $additional_transmission_arguments);
        $web_api_parameters = $this->_getWebApiCallParameters($entity, $additional_transmission_arguments);
        // Get the Web API client to be used to execute the Transmission
        $webApiClient = $this->_getWebApiClient();
        // Execute the transmission
        return $webApiClient->callApiMethod($web_api_method, $web_api_parameters);
    }

    /**
     * Subclasses may want to override this method to implement customer functionality
     *
     * @param \Magento\Framework\Model\AbstractModel $entity
     * @return mixed
     * @throws \Dunagan\WebApi\Model\Exception\Service\Transmission\RequiredEntity
     */
    protected function _validateEntityToTransmit($entity)
    {
        // For the base method of this version, simply validate that the entity is a loaded object
        if ((!is_object($entity)) || (!$entity->getId()))
        {
            $error_message = __(self::ERROR_INVALID_ENTITY, get_class($this), __METHOD__, print_r($entity));
            throw new \Dunagan\WebApi\Model\Exception\Service\Transmission\RequiredEntity($error_message);
        }
    }
}
