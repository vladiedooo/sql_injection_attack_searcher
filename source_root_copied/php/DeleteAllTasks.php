<?php
/**
 * Author: Sean Dunagan (github: dunagan5887)
 * Date: 8/24/16
 */

namespace Dunagan\ProcessQueue\Controller\Adminhtml\Task;

/**
 * Class DeleteAllTasks
 * @package Dunagan\ProcessQueue\Controller\Adminhtml\Task
 *
 * An abstract Controller class to implement deleting ProcessQueue tasks
 */
abstract class DeleteAllTasks extends Base implements \Dunagan\Base\Controller\Adminhtml\GridAndFormControllerInterface
{
    /**
     * Template for the message to be shown when task deletion occurs without exception
     *
     * @var string
     */
    protected $_tasks_deleted_success_message_template = '%1 tasks were successfully deleted.';

    /**
     * Template for the message to be shown when task deletion throws an Exception
     *
     * @var string
     */
    protected $_tasks_deleted_exception_message_template = 'An exception occurred while attempting to delete all tasks from the system: %1';

    /**
     * Interface for the resource model to be used to delete tasks
     *
     * @var string
     */
    protected $_task_resource_interface_name = '\Dunagan\ProcessQueue\Model\ResourceModel\TaskInterface';

    /**
     * Task statuses to filter on in regards to task deletion
     *
     * @var array
     */
    protected $_task_statuses_to_filter_on_deletion = array();

    /**
     * The Task Resource model used to delete tasks
     *
     * @var \Dunagan\ProcessQueue\Model\ResourceModel\TaskInterface
     */
    protected $_taskResourceModel;

    /**
     * @param \Magento\Backend\App\Action\Context $context
     * @param \Magento\Framework\Registry $registry
     */
    public function __construct(\Magento\Backend\App\Action\Context $context, \Magento\Framework\Registry $registry)
    {
        parent::__construct($context, $registry);
        $objectManager = $context->getObjectManager();
        $this->_taskResourceModel = $objectManager->get($this->_task_resource_interface_name);
    }

    /**
     * Executes the action of deleting tasks from the system
     */
    public function executeDeleteAllTasksAction()
    {
        try {
            $number_of_tasks_deleted = $this->_taskResourceModel->deleteAllTasks($this->_task_statuses_to_filter_on_deletion);
            $this->messageManager->addSuccess(__($this->_tasks_deleted_success_message_template, $number_of_tasks_deleted));
        } catch (\Exception $e) {
            $error_message = __($this->_tasks_deleted_exception_message_template, $e->getMessage());
            $this->messageManager->addError($error_message);
            $this->_objectManager->get('Psr\Log\LoggerInterface')->critical($error_message);
        }

        $this->_redirect('*/*/index');
    }
}