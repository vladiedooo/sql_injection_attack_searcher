<?php
/**
 * Author: Sean Dunagan (github: dunagan5887)
 * Date: 9/14/16
 */

namespace Scc\Campaign\Observer\Queue\Account;

use Magento\Framework\Event\ObserverInterface;

/**
 * Observer class to queue up a transmission of the customer account creation to the Campaign system
 *
 * Class Creation
 * @package Scc\Campaign\Observer\Queue\Account
 */
class Creation extends \Scc\Campaign\Observer\Queue\Email\AbstractTransmission implements ObserverInterface
{
    /**
     * We want transmission tasks for the Account Creation email to be executed immediately, not on the next crontab
     *  iteration
     *
     * @var bool
     */
    protected $_should_execute_task_upon_creation = true;

    /**
     * @var \Scc\Campaign\Helper\Flags
     */
    protected $_flagsHelper;

    /**
     * We need to flag whether this customer account creation is the result of an admin creating an account in the
     *  admin panel
     *
     * @param \Magento\Framework\Event\Observer $observer
     * @return array
     */
    protected function _prepareAdditionalArgumentsArray(\Magento\Framework\Event\Observer $observer)
    {
        $admin_is_saving_customer_data = $this->_flagsHelper->getAdminIsSavingCustomerData();
        return array('scc_campaign_admin_is_saving_customer_data' => $admin_is_saving_customer_data);
    }

    /**
     * @inheritdoc
     *
     * @return \Scc\Campaign\Model\Task\Queue\Account\CreationInterface
     */
    protected function _getApiTransmissionQueueTask()
    {
        return $this->_objectManager->get('\Scc\Campaign\Model\Task\Queue\Account\CreationInterface');
    }

    /**
     * Creation constructor.
     * @param \Scc\Campaign\Model\LoggerInterface $logger
     * @param \Magento\Framework\ObjectManagerInterface $objectManager
     * @param \Scc\Campaign\Helper\Flags $flagsHelper
     */
    public function __construct(\Scc\Campaign\Model\LoggerInterface $logger,
                                \Magento\Framework\ObjectManagerInterface $objectManager,
                                \Scc\Campaign\Helper\Flags $flagsHelper)
    {
        parent::__construct($logger, $objectManager);
        $this->_flagsHelper = $flagsHelper;
    }

}
