<?php
/**
 * Author: Sean Dunagan (github: dunagan5887)
 * Date: 5/1/17
 */

namespace Scc\Campaign\Model\Task\Queue\Account;

/**
 * Interface CreationNoPasswordInterface
 * @package Scc\Campaign\Model\Task\Queue\Account
 */
interface CreationNoPasswordInterface extends CreationInterface
{

}
