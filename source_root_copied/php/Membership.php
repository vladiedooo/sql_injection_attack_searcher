<?php
/**
 * Author: Sean Dunagan (github: dunagan5887)
 * Date: 4/26/17
 */

namespace Scc\Campaign\Helper;

/**
 * Class Membership
 *
 * @package Scc\Campaign\Helper
 */
class Membership
{
    const EXCEPTION_ATTEMPTING_TO_QUEUE_TASK = 'An exception occurred while trying to queue a task to transmit a Premium Membership %1 to Campaign for customer %2 on website %3: %4 ';

    const BASIC_MEMBERSHIP_VALUE_IN_CAMPAIGN = 1;
    const PREMIUM_MEMBERSHIP_VALUE_IN_CAMPAIGN = 2;

    /**
     * @var \Magento\Framework\ObjectManagerInterface
     */
    protected $_objectManager;

    /**
     * @var \Scc\Campaign\Model\LoggerInterface
     */
    protected $_logger;

    /**
     * @var array
     */
    protected $_transmitted_membership_change_customer_ids = array();

    /**
     * This method will queue up a task to send a Membership Change API call to Campaign.
     * The created task will be returned.
     *
     * In the event that a task has already been queued in this execution thread, no task will be queued and null will
     *      be returned.
     *
     * @param array $customer_ids_array
     * @param bool $is_adding_to_segment
     * @param int $website_id
     *
     * @return \Scc\Campaign\Model\Task\Queue\Membership\ChangeMembershipInterface|null
     * @throws \Scc\Campaign\Model\Exception\Api\ChangeMembershipException
     */
    public function queueChangeMembershipCampaignAPICall($customer_ids_array, $is_adding_to_segment, $website_id)
    {
        $customer_id = reset($customer_ids_array);

        // It is often the case that a customer will be "added" to a segment multiple times in a single request if they
        //      are being added. We need to make sure that we're not making multiple requests for a customer
        if ($this->hasMembershipChangeBeenTransmittedForCustomer($customer_id))
        {
            return null;
        }

        try
        {
            $membership_value = $is_adding_to_segment
                                    ? $this->getPremiumMembershipValueInCampaign()
                                    : $this->getBasicMembershipValueInCampaign();

            // Create the necessary task
            $createdTask = $this->_getApiTransmissionQueueTask()
                                ->queueChangeMembershipTask($customer_id, $membership_value, $website_id);

            $this->membershipChangeHasBeenTransmittedForCustomer($customer_id);
        }
        catch(\Exception $e)
        {
            $action = $is_adding_to_segment ? 'addition' : 'removal';
            $error_message = __(self::EXCEPTION_ATTEMPTING_TO_QUEUE_TASK, $action, $customer_id, $website_id, $e->getMessage());
            throw new \Scc\Campaign\Model\Exception\Api\ChangeMembershipException($error_message);
        }

        return $createdTask;
    }

    /**
     * @param int $customer_id
     *
     * @return bool
     */
    public function hasMembershipChangeBeenTransmittedForCustomer($customer_id)
    {
        return isset($this->_transmitted_membership_change_customer_ids[$customer_id]);
    }

    /**
     * @param int $customer_id
     */
    public function membershipChangeHasBeenTransmittedForCustomer($customer_id)
    {
        $this->_transmitted_membership_change_customer_ids[$customer_id] = true;
    }

    /**
     * @return \Scc\Campaign\Model\Task\Queue\Membership\ChangeMembershipInterface
     */
    protected function _getApiTransmissionQueueTask()
    {
        return $this->_objectManager->create('Scc\Campaign\Model\Task\Queue\Membership\ChangeMembershipInterface');
    }

    /**
     * Returns true if the membership value passed in relates to the Premium Membership
     * Returns false otherwise
     *
     * @param int $membership_value
     *
     * @return bool
     */
    public function isPremiumMembershipValue($membership_value)
    {
        return (intval($membership_value) == self::PREMIUM_MEMBERSHIP_VALUE_IN_CAMPAIGN);
    }

    /**
     * Return the value that Campaign uses to denote a Premium Membership
     *
     * @return int
     */
    public function getPremiumMembershipValueInCampaign()
    {
        return self::PREMIUM_MEMBERSHIP_VALUE_IN_CAMPAIGN;
    }

    /**
     * Return the value that Campaign uses to denote a Basic Membership
     *
     * @return int
     */
    public function getBasicMembershipValueInCampaign()
    {
        return self::BASIC_MEMBERSHIP_VALUE_IN_CAMPAIGN;
    }

    /**
     * Membership constructor.
     *
     * @param \Magento\Framework\ObjectManagerInterface $objectManager
     * @param \Scc\Campaign\Model\LoggerInterface       $logger
     */
    public function __construct(\Magento\Framework\ObjectManagerInterface $objectManager,
                                \Scc\Campaign\Model\LoggerInterface $logger)
    {
        $this->_objectManager = $objectManager;
        $this->_logger = $logger;
    }
}