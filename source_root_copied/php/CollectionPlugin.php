<?php
/**
 * Author: Sean Dunagan (github: dunagan5887)
 * Date: 3/29/17
 */

namespace Scc\Catalog\Plugin\Model\ResourceModel\Product;

use Magento\Catalog\Model\ResourceModel\Product\Collection;

/**
 * Class CollectionPlugin
 *
 * @package Scc\Catalog\Plugin\Model\ResourceModel\Product
 */
class CollectionPlugin
{
    /**
     * @param \Magento\Catalog\Model\ResourceModel\Product\Collection\Interceptor $interceptor
     * @param \Closure $proceed
     * @param string $attribute
     * @param string $dir
     *
     * @return \Magento\Catalog\Model\ResourceModel\Product\Collection\Interceptor
     */
    public function aroundAddAttributeToSort($interceptor, $proceed, $attribute, $dir = Collection::SORT_ORDER_ASC)
    {
        if ($attribute == 'is_salable')
        {
            $interceptor->getSelect()->order('is_salable ' . $dir);
            return $interceptor;
        }
        return $proceed($attribute, $dir);
    }
}
