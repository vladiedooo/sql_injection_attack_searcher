<?php
/**
 * Author: Sean Dunagan (github: dunagan5887)
 * Date: 2/16/17
 */

namespace Ideal\SKProducts\Setup\Data\Options\Drive\Size;

/**
 * Class Size
 * @package Ideal\SKProducts\Setup\Data\Options\Drive
 */
class Crowfoot
{
    /**
     * @return array
     */
    public function getOptionArray()
    {
        return array('values' => array('3/8"', '1/2"'));
    }
}
