<?php
/**
 * Author: Sean Dunagan (github: dunagan5887)
 * Date: 1/27/17
 */

namespace Scc\AEMWebApi\Helper;

/**
 * Class Url
 * @package Scc\AEMWebApi\Helper
 */
class Url
{
    const BUSINESS_UNIT_TEMPLATE = 'BUSINESS_UNIT_CODE';
    const LANGUAGE_CODE_TEMPLATE = 'LANGUAGE_CODE';

    const DEFAULT_LANGUAGE_CODE = 'en_US';

    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $_storeManager;

    /**
     * @var \Magento\Framework\Locale\Resolver
     */
    protected $_localeResolver;

    /**
     * Replaces templated url values with the proper value regarding the current  website and locale
     *
     * @param string $url_template
     * @param null|string $website_code
     * @param null|string $language_code
     * @param int|null $customer_group_id - CURRENTLY THIS IS NOT FUNCTIONAL/IMPLEMENTED
     * @return string
     */
    public function replaceTemplatedUrlValues($url_template, $website_code = null, $language_code = null,
                                              $customer_group_id = null)
    {
        // Replace the BUSINESS_UNIT_CODE with the current website code
        $website_code = is_null($website_code) ? $this->getWebsiteCodeForAEMCommunications() : $website_code;
        $url = str_replace(self::BUSINESS_UNIT_TEMPLATE, $website_code, $url_template);
        // Replace the LANGUAGE_CODE with the current locale code
        $language_code = is_null($language_code) ? $this->getLanguageCodeForAEMCommunications() : $language_code;
        $url = str_replace(self::LANGUAGE_CODE_TEMPLATE, $language_code, $url);
        return $url;
    }

    /**
     * Returns the website code to be used for communications with AEM for the currently loaded store view
     *
     * @return string
     */
    public function getWebsiteCodeForAEMCommunications()
    {
        return $this->_storeManager->getWebsite()->getCode();
    }

    /**
     * Returns the language code to be used for communications with AEM for the currently loaded store view
     *
     * @return string
     */
    public function getLanguageCodeForAEMCommunications()
    {
        $locale = $this->_localeResolver->getLocale();
        return (!empty($locale)) ? $locale : self::DEFAULT_LANGUAGE_CODE;
    }

    /**
     * Url constructor.
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     * @param \Magento\Framework\Locale\Resolver $localResolver
     */
    public function __construct(\Magento\Store\Model\StoreManagerInterface $storeManager,
                                \Magento\Framework\Locale\Resolver $localResolver)
    {
        $this->_storeManager = $storeManager;
        $this->_localeResolver = $localResolver;
    }
}

