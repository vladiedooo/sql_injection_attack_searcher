<?php
/**
 * Author: Sean Dunagan (github: dunagan5887)
 * Date: 9/18/16
 */

namespace Scc\WebApi\Model\Logger;

/**
 * Interface DebugHandlerInterface
 * @package Scc\WebApi\Model\Logger
 */
interface DebugHandlerInterface extends \Monolog\Handler\HandlerInterface
{

}
