<?php
/**
 * Author: Sean Dunagan (github: dunagan5887)
 * Date: 4/26/17
 */

namespace Scc\Campaign\Model\Api\Url;

/**
 * Class ChangeMembershipJsspFactory
 *
 * @package Scc\Campaign\Model\Api\Url
 */
class ChangeMembershipJsspFactory extends Factory implements ChangeMembershipJsspFactoryInterface
{
    const CUSTOMER_ID_PARAMETER = 'customerId';
    const MEMBERSHIP_VALUE_PARAMETER = 'membership';
    const CUSTOMER_EMAIL_ADDRESS_PARAMETER = 'confirmationEmailAddress';
    const CAMPAIGN_EMAIL_TEMPLATE_PARAMETER = 'confirmationTemplate';
    const WEBSITE_ID_INDEX = 'website_id';

    /**
     * Constructs a URL of the form below
     *
     * https://idealindustries.campaign.adobe.com/jssp/ideal/changeCustomerMembership.jssp?
     *          customerId=12345&
     *          membership=1&
     *          skey=1234567890123456789012345678901234567890&
     *          confirmationTemplate=SKToolsPremiumConfirmation&        // Only included in a Premium Upgrade call
     *          confirmationEmailAddress=customer@store.com             // Only included in a Premium Upgrade call
     *
     * @param array $email_transmission_arguments
     *
     * @return string
     */
    public function getUrlForApiCall($email_transmission_arguments)
    {
        $url_params = array();

        $url_params[self::CUSTOMER_ID_PARAMETER] = $email_transmission_arguments[self::CUSTOMER_ID_PARAMETER];
        $membership_value = $email_transmission_arguments[self::MEMBERSHIP_VALUE_PARAMETER];
        $url_params[self::MEMBERSHIP_VALUE_PARAMETER] = $membership_value;
        $url_params[self::SKEY_PARAMETER] = $this->getSkey();

        // If this is a Premium Upgrade API call, we need to include the confirmation template and email address
        if ($this->_sccCampaignMembershipHelper->isPremiumMembershipValue($membership_value))
        {
            $url_params[self::CUSTOMER_EMAIL_ADDRESS_PARAMETER]
                = $email_transmission_arguments[self::CUSTOMER_EMAIL_ADDRESS_PARAMETER];

            $website_id = $email_transmission_arguments[self::WEBSITE_ID_INDEX];
            $campaign_email_template = $this->_sccJsspCampaignConfigHelper
                                            ->getChangeMembershipCampaignEmailTemplate($website_id);
            $url_params[self::CAMPAIGN_EMAIL_TEMPLATE_PARAMETER] = $campaign_email_template;
        }

        $url_param_string = $this->_getImplodedUrlParamString($url_params);
        $base_change_membership_jssp_url = $this->_sccJsspCampaignConfigHelper->getChangeMembershipJsspUrl();
        $url_to_call = $this->_getFullUrlString($base_change_membership_jssp_url, $url_param_string);
        return $url_to_call;
    }

    /**
     * {@inheritdoc}
     */
    public function getExpectedCustomerEmailAddressIndex()
    {
        return self::CUSTOMER_EMAIL_ADDRESS_PARAMETER;
    }

    /**
     * {@inheritdoc}
     */
    public function getExpectedCustomerIdIndex()
    {
        return self::CUSTOMER_ID_PARAMETER;
    }

    /**
     * {@inheritdoc}
     */
    public function getExpectedMembershipValueIndex()
    {
        return self::MEMBERSHIP_VALUE_PARAMETER;
    }

    /**
     * {@inheritdoc}
     */
    public function getExpectedWebsiteIdIndex()
    {
        return self::WEBSITE_ID_INDEX;
    }
}
