<?php
/**
 * Author: Sean Dunagan (github: dunagan5887)
 * Date: 10/3/16
 */

namespace Dunagan\Base\Block\Address\Renderer;

use Magento\Customer\Model\Address\AddressModelInterface;
use Magento\Customer\Model\Address\Mapper;
use Magento\Customer\Model\Metadata\ElementFactory;

/**
 * Class FieldsRenderer
 * @package Dunagan\Base\Block\Address\Renderer
 */
class FieldsRenderer extends \Magento\Framework\View\Element\AbstractBlock
{
    /**
     * Format type object
     *
     * @var \Magento\Framework\DataObject
     */
    protected $_type;

    /**
     * @var ElementFactory
     */
    protected $_elementFactory;

    /**
     * @var \Magento\Directory\Model\CountryFactory
     */
    protected $_countryFactory;

    /**
     * @var \Magento\Customer\Api\AddressMetadataInterface
     */
    protected $_addressMetadataService;

    /**
     * @var \Magento\Framework\ObjectManagerInterface
     */
    protected $_objectManager;

    /**
     * @var Mapper
     */
    protected $addressMapper;

    /**
     * {@inheritdoc}
     */
    public function render(array $address_data_array)
    {
        switch ($this->getType()->getCode()) {
            case 'html':
                $dataFormat = ElementFactory::OUTPUT_FORMAT_HTML;
                break;
            case 'pdf':
                $dataFormat = ElementFactory::OUTPUT_FORMAT_PDF;
                break;
            case 'oneline':
                $dataFormat = ElementFactory::OUTPUT_FORMAT_ONELINE;
                break;
            default:
                $dataFormat = ElementFactory::OUTPUT_FORMAT_TEXT;
                break;
        }

        $attributesMetadata = $this->_addressMetadataService->getAllAttributesMetadata();
        $data = [];
        foreach ($attributesMetadata as $attributeMetadata) {
            if (!$attributeMetadata->isVisible()) {
                continue;
            }
            $attribute_code = $attributeMetadata->getAttributeCode();
            $attribute_label = $attributeMetadata->getData('frontend_label');
            // If the $attribute_label is not defined, default to the attribute code
            $attribute_label = (!empty($attribute_label)) ? $attribute_label : $attribute_code;
            $attribute_label = __($attribute_label)->render();
            if ($attribute_code == 'country_id' && isset($address_data_array['country_id'])) {
                $data[$attribute_label] = $this->_countryFactory->create()->loadByCode(
                    $address_data_array['country_id']
                )->getName();
            } elseif ($attribute_code == 'region' && isset($address_data_array['region'])) {
                $data[$attribute_label] = __($address_data_array['region']);
            } elseif (isset($address_data_array[$attribute_code])) {
                $value = $address_data_array[$attribute_code];
                $dataModel = $this->_elementFactory->create($attributeMetadata, $value, 'customer_address');
                $value = $dataModel->outputValue($dataFormat);
                if ($attributeMetadata->getFrontendInput() == 'multiline') {
                    $values = $dataModel->outputValue(ElementFactory::OUTPUT_FORMAT_ARRAY);
                    // explode lines
                    foreach ($values as $k => $v) {
                        $key = sprintf('%s%d', $attribute_label, $k + 1);
                        $data[$key] = $v;
                    }
                }
                $data[$attribute_label] = $value;
            }
        }
        if ($this->getType()->getEscapeHtml()) {
            foreach ($data as $key => $value) {
                $data[$key] = $this->escapeHtml($value);
            }
        }

        $output_string = '';
        foreach($data as $attribute_label => $value)
        {
            $output_string .= ($attribute_label . ': ' . $value);
        }

        return $output_string;
    }

    /**
     * Set format type object
     *
     * @param \Magento\Framework\DataObject $type
     * @return void
     */
    public function setType(\Magento\Framework\DataObject $type)
    {
        $this->_type = $type;
    }

    /**
     * Retrieve format type object
     *
     * @return \Magento\Framework\DataObject
     */
    public function getType()
    {
        return $this->_type;
    }

    /**
     * Constructor
     *
     * @param \Magento\Framework\View\Element\Context $context
     * @param ElementFactory $elementFactory
     * @param \Magento\Directory\Model\CountryFactory $countryFactory
     * @param \Magento\Customer\Api\AddressMetadataInterface $metadataService
     * @param Mapper $addressMapper
     * @param \Magento\Framework\ObjectManagerInterface $objectManager
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\View\Element\Context $context,
        ElementFactory $elementFactory,
        \Magento\Directory\Model\CountryFactory $countryFactory,
        \Magento\Customer\Api\AddressMetadataInterface $metadataService,
        Mapper $addressMapper,
        \Magento\Framework\ObjectManagerInterface $objectManager,
        array $data = []
    ) {
        $this->_elementFactory = $elementFactory;
        $this->_countryFactory = $countryFactory;
        $this->_addressMetadataService = $metadataService;
        $this->addressMapper = $addressMapper;

        parent::__construct($context, $data);

        $this->_objectManager = $objectManager;

        // Set the type defaults
        $typeDefaultsObject = $this->_objectManager->create('\Magento\Framework\DataObject');
        $typeDefaultsObject->setCode('html'); // Render html by default
        $typeDefaultsObject->setEscapeHtml(true); // Escape html by default
        $this->setType($typeDefaultsObject);
    }
}
