<?php
/**
 * Author: Sean Dunagan (github: dunagan5887)
 * Date: 5/3/17
 */

namespace Scc\PremiumMembership\Model\Cronjob;

/**
 * Class RefreshPremiumAndBasicSegments
 * @package Scc\PremiumMembership\Model\Cronjob
 */
class RefreshPremiumAndBasicSegments
{
    const EXCEPTION_RUNNING_CRONJOB = 'An exception occurred while running the Premium Customers Segments refresh cronjob: %1';

    /**
     * @var \Scc\PremiumMembership\Helper\Segment
     */
    protected $_sccPremiumMembershipSegmentHelper;

    /**
     * @var \Psr\Log\LoggerInterface
     */
    protected $_logger;

    /**
     * Run cronjob to refresh the Premium and Basic customer segments
     */
    public function execute()
    {
        try
        {
            $premiumCustomerSegment = $this->_sccPremiumMembershipSegmentHelper->getPremiumCustomerSegment();
            $premiumCustomerSegment->matchCustomers();

            $basicCustomerSegment = $this->_sccPremiumMembershipSegmentHelper->getBasicCustomerSegment();
            $basicCustomerSegment->matchCustomers();
        }
        catch(\Exception $e)
        {
            $error_message = __(self::EXCEPTION_RUNNING_CRONJOB, $e->getMessage());
            $this->_logger->error($error_message);
        }
    }

    /**
     * RefreshPremiumAndBasicSegments constructor.
     *
     * @param \Scc\PremiumMembership\Helper\Segment $sccPremiumMembershipSegmentHelper
     * @param \Psr\Log\LoggerInterface              $logger
     */
    public function __construct(\Scc\PremiumMembership\Helper\Segment $sccPremiumMembershipSegmentHelper,
                                \Psr\Log\LoggerInterface $logger)
    {
        $this->_sccPremiumMembershipSegmentHelper = $sccPremiumMembershipSegmentHelper;
        $this->_logger = $logger;
    }
}
