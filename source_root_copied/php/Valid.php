<?php
namespace Scc\Customers\Controller\Customer;

use Magento\Customer\Api\CustomerRepositoryInterface;
use Magento\Framework\Api\DataObjectHelper;
use Magento\Customer\Model\Session;
use Magento\Framework\View\Result\PageFactory;
use Magento\Framework\App\Action\Context;
use Magento\Framework\Controller\Result\Forward;

/**
 * Class ItemsCount
 * @package Aheadworks\Layerednav\Controller\Ajax
 */
class Valid extends \Magento\Customer\Controller\AbstractAccount
{
    /** @var CustomerRepositoryInterface  */
    protected $customerRepository;

    /** @var DataObjectHelper */
    protected $dataObjectHelper;

    /**
     * @var Session
     */
    protected $session;

    /**
     * @var PageFactory
     */
    protected $resultPageFactory;

    /** @var \Magento\Framework\Controller\Result\Forward $resultForward */
    protected $resultForward;

    /**
     * @param Context $context
     * @param Session $customerSession
     * @param PageFactory $resultPageFactory
     * @param CustomerRepositoryInterface $customerRepository
     * @param DataObjectHelper $dataObjectHelper
     */
    public function __construct(
        Context $context,
        Session $customerSession,
        PageFactory $resultPageFactory,
        CustomerRepositoryInterface $customerRepository,
        DataObjectHelper $dataObjectHelper,
        \Magento\Framework\Controller\Result\Forward $resultForward
    ) {
        $this->session = $customerSession;
        $this->resultPageFactory = $resultPageFactory;
        $this->customerRepository = $customerRepository;
        $this->dataObjectHelper = $dataObjectHelper;
        $this->resultForward = $resultForward;
        parent::__construct($context);
    }

    /**
     * {@inheritdoc}
     */
    public function execute()
    {
        if ($this->session->isLoggedIn())
        {
            /** @var \Magento\Framework\View\Result\Page $resultPage */
            $resultPage = $this->resultPageFactory->create();

            $block = $resultPage->getLayout()->getBlock('customer_coupon');
            if ($block) {
                $block->setRefererUrl($this->_redirect->getRefererUrl());
            }

            $customerId = $this->session->getCustomerId();
            $customerDataObject = $this->customerRepository->getById($customerId);
            if (!empty($data)) {
                $this->dataObjectHelper->populateWithArray(
                    $customerDataObject,
                    $data,
                    '\Magento\Customer\Api\Data\CustomerInterface'
                );
            }

            $resultPage->getConfig()->getTitle()->set(__('Coupons'));
            $resultPage->getLayout()->getBlock('messages')->setEscapeMessageFlag(true);
            return $resultPage;
        }
        else
        {
            $resultForward = $this->resultForwardFactory->create();
            $resultForward->forward('defaultNoRoute'); // Call the another action of your module
            return $resultForward;
        }

    }
}
