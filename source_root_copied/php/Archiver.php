<?php
/**
 * Author: Sean Dunagan (github: dunagan5887)
 * Date: 10/26/16
 */

namespace Dunagan\ProcessQueue\Model\Logger\Handler;

use Monolog\Logger;

/**
 * Class Archiver
 * @package Dunagan\ProcessQueue\Model\Logger\Handler
 */
class Archiver extends \Magento\Framework\Logger\Handler\Base
{
    /**
     * Logging level
     * @var int
     */
    protected $loggerType = Logger::DEBUG;

    /**
     * File name
     * @var string
     */
    protected $fileName = '/var/log/dunagan_process_queue_archive.log';
}
