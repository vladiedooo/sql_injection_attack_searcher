<?php
/**
 * Author: Sean Dunagan (github: dunagan5887)
 * Date: 12/21/16
 */

namespace Scc\Catalog\Api;

use Scc\Framework\Api\SearchCriteriaInterface;

/**
 * Interface ProductRepositoryInterface
 * @package Scc\Catalog\Api
 */
interface ProductRepositoryInterface extends \Magento\Catalog\Api\ProductRepositoryInterface
{
    /**
     * Get product list of selected fields
     *
     * @param SearchCriteriaInterface $searchCriteria
     * @return \Magento\Catalog\Api\Data\ProductSearchResultsInterface
     */
    public function getSelectList(SearchCriteriaInterface $searchCriteria);
}
