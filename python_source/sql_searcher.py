# coding=utf-8

"""This module, sql_searcher.py, will search for SQL related code in code bases."""

# Used for useful file + directory operations.
from python_source import useful_file_operations as ufo
# Needed for running source file inspections on potential SQL injection attack spots.
from python_source import source_file_inspector as sfi
# Needed for representing source files as objects.
from python_source import source_file_definition as sfd

# Utility variables.
project_root = '/Users/utarsuno/git_repos/jira_tickets/sf-11/'


# TODO : Good portion of this source code is not used, most work was done manually.


class SQLSearcher:
	"""This objects conducts searches for SQL related code inside of code bases."""

	def __init__(self, project_source_root):
		self.source_root_original  = project_source_root
		self.source_root_copied    = project_root + 'utility_script/source_root_copied'
		self.source_root_to_check  = project_root + 'utility_script/source_root_to_check'
		self.source_files_original = {}
		self.source_files_copied   = {}
		self.source_files_to_check = {}

	def prepare_source_root_copied(self):
		"""Utility function to download all source code files from the project and place them inot the 'all_source_files' directory."""
		# Get all the original project files.
		for f in ufo.get_all_file_paths_from_directory(self.source_root_original):
			source_file = sfd.SourceFile(f)
			self.source_files_original[source_file.file_name] = source_file
		# Get all the copied project files.
		for f in ufo.get_all_file_paths_from_directory(self.source_root_copied):
			source_file = sfd.SourceFile(f)
			self.source_files_copied[source_file.file_name] = source_file

		# Remove any copied files that are not found in the original project files.
		files_to_remove = []
		for sfc in self.source_files_copied:
			if sfc not in self.source_files_original:
				files_to_remove.append(sfc)
		for ftr in files_to_remove:
			ufo.delete_file(self.source_files_copied[ftr].file_path)
			del self.source_files_copied[ftr]

		# Add any original project files that are not found in the copied files.
		for sfo in self.source_files_original:
			original_source_file = self.source_files_original[sfo]
			if sfo not in self.source_files_copied:
				path_to_copied_source_file = self.source_root_copied + '/' + original_source_file.file_extension + '/' + original_source_file.file_name
				self._copy_file(sfo, original_source_file.file_path, path_to_copied_source_file)
				self.source_files_copied[original_source_file.file_name] = sfd.SourceFile(path_to_copied_source_file)

	def prepare_source_root_to_check(self):
		"""Utility function to transfer files from 'source_root_copied' to 'source_root_to_check' and then remove any un-needed files."""
		ufo.delete_directory(self.source_root_to_check)
		ufo.make_any_missing_directories(self.source_root_to_check)

		# Copy all files from 'source_root_copied' into 'source_root_to_check'.
		for sfc in self.source_files_copied:
			source_file = self.source_files_copied[sfc]
			# Only copy if its not a directory to ignore.
			if source_file.file_extension not in sfi.ignore_directories:
				# Only copy if its not a file to ignore.
				if source_file.file_name not in sfi.ignore_files:
					path_for_to_check_source_file = self.source_root_to_check + '/' + source_file.file_extension + '/' + source_file.file_name
					self._copy_file(sfc, source_file.file_path, path_for_to_check_source_file)
					self.source_files_to_check[source_file.file_name] = sfd.SourceFile(path_for_to_check_source_file)

		del self.source_files_to_check['shop']

		# Parse the information needed from the files in 'source_root_to_check'
		for sf in self.source_files_to_check:
			self.source_files_to_check[sf].parse()

		number_of_files_to_manually_check = 0
		for sf in self.source_files_to_check:
			if self.source_files_to_check[sf].needs_manual_check():
				number_of_files_to_manually_check += 1
			else:
				try:
					#print('Deleting the following file : {' + str(self.source_files_to_check[sf].file_path) + '}')
					ufo.delete_file(self.source_files_to_check[sf].file_path)
				except Exception as e:
					#print('Exception on {' + self.source_files_to_check[sf].file_path + '} - {' + str(e) + '}')
					y = 2

		print('There are ' + str(len(self.source_files_to_check)) + ' source files left to check!')
		print('There are now ' + str(number_of_files_to_manually_check) + ' source files left to manually check!')

	def _copy_file(self, file_name, file_source, file_destination):
		"""Utility function."""
		try:
			ufo.copy_file(file_source, file_destination)
		except OSError as e:
			print('Exception on {' + file_name + '} - {' + str(e) + '}')

	def print_needed_full_paths(self):
		"""Utility function to print the full paths of the source files that have SQL content."""

		sql_files = [
			'AdminAccount.php',
			'AdminAccountTest.php',
			'AdminCredentialsValidator.php',
			'DbValidator.php',
			'OrdersEEFixture.php',
			'OrdersFixture.php',
			'staging_entity.php',
			'staging_entity_rollback.php',
			'staging_update.php',
			'staging_update_rollback.php',
			'UpgradeData.php',
			'Installer.php',
			'MatchTest.php',
			'AbstractState.php',
			'SalesRuleTest.php',
			'FullTest.php',
			'RowsTest.php',
			'delete_salesrule_filters.php',
			'FilterTest.php',
			'IndexHandlerTest.php',
			'RuleTest.php',
			'Sequence.php',
			'MysqlTest.php',
			'ProfileTest.php'
		]

		'''
		staging_entity.php
		staging_entity_rollback.php
		staging_update.php
		staging_update_rollback.php
		'''

		print('\nPrinting matched files :\n')
		for f in sql_files:
			if f in self.source_files_original:
				#print('Found match for {' + f + '} - {' + self.source_files_original[f].file_path)
				print(self.source_files_original[f].file_path.replace('/Users/utarsuno/git_repos/jira_tickets/sf-11/', ''))
			else:
				print('Did not find match for {' + f + '}')

sql_finder = SQLSearcher(project_root + 'ideal-magento-codebase')
sql_finder.prepare_source_root_copied()
sql_finder.prepare_source_root_to_check()

sql_finder.print_needed_full_paths()

#files_to_check = sql_finder.get_files_to_check()
#print('There are : ' + str(len(files_to_check)) + 'leftover files to check, they are :')
#for f in files_to_check:
#	print(str(f))

