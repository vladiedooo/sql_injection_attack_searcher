<?php

namespace Scc\Customers\Plugin\Model;

/**
 * Class AccountManagementPlugin
 * @package Scc\Customers\Plugin\Model
 */
class AccountManagementPlugin
{
    /**
     * @var \Magento\Eav\Model\Config
     */
    private $eavConfig;

    /**
     * @var \Magento\Customer\Api\CustomerRepositoryInterface
     */
    protected $_customerRepository;

    /**
     * @var \Magento\Customer\Model\ResourceModel\Group\Collection
     */
    protected $_groupCollection;

    /**
     * @var \Magento\Eav\Model\ResourceModel\Entity\Attribute\Option\Collection
     */
    protected $_attributeCollection;

    /**
     * @var \Scc\Customers\Helper\Type
     */
    protected $_sccCustomerTypeHelper;

    /**
     * AccountManagementPlugin constructor.
     *
     * @param \Magento\Eav\Model\Config                         $eavConfig
     * @param \Magento\Customer\Api\CustomerRepositoryInterface $customerRepository
     * @param \Magento\Customer\Model\ResourceModel\Group\Collection $groupCollection
     * @param \Magento\Eav\Model\ResourceModel\Entity\Attribute\Option\Collection $attributeCollection
     * @param \Scc\Customers\Helper\Type $sccCustomerTypeHelper
     */
    public function __construct(\Magento\Eav\Model\Config $eavConfig,
                                \Magento\Customer\Api\CustomerRepositoryInterface $customerRepository,
                                \Magento\Customer\Model\ResourceModel\Group\Collection $groupCollection,
                                \Magento\Eav\Model\ResourceModel\Entity\Attribute\Option\Collection $attributeCollection,
                                \Scc\Customers\Helper\Type $sccCustomerTypeHelper)
    {
        $this->eavConfig = $eavConfig;
        $this->_customerRepository = $customerRepository;
        $this->_groupCollection = $groupCollection;
        $this->_attributeCollection = $attributeCollection;
        $this->_sccCustomerTypeHelper = $sccCustomerTypeHelper;
    }

    /**
     * Assign the customer to a customer group based on the value of their type_of_customer attribute
     * Also, set the dob_month based on the dob attribute value
     *
     * @param \Magento\Customer\Model\AccountManagement\Interceptor $interceptor
     * @param \Magento\Customer\Model\Data\Customer $customer
     *
     * @return \Magento\Customer\Model\Data\Customer
     */
    public function afterCreateAccount($interceptor, $customer)
    {
        $this->_sccCustomerTypeHelper->setCustomerGroupOnCustomerDataObject($customer);

        if ($dobFull = $customer->getDob())
        {
            $dobMonth = date('F', strtotime($dobFull));

            $attribute = $this->eavConfig->getAttribute(\Magento\Customer\Model\Customer::ENTITY, 'dob_month');
            $attributeOptions = $attribute->getSource()->getAllOptions();

            foreach ($attributeOptions as $option)
            {
                if (strpos($option['label'], $dobMonth) !== FALSE)
                {
                    $customer->setCustomAttribute('dob_month', $option['value']);
                    break;
                }
            }
        }

        $savedCustomer = $this->_customerRepository->save($customer);
        return $savedCustomer;
    }

    /**
     * In the guest-order-confirmation-create-account registration flow, this plugin will assign a default birthday and
     *      customerType value to the customer's account
     *
     * @param \Magento\Customer\Model\AccountManagement\Interceptor $subject
     * @param                                                       $customer
     * @param null                                                  $password
     * @param string                                                $redirectUrl
     *
     * @return array
     */
    public function beforeCreateAccount(
        \Magento\Customer\Model\AccountManagement\Interceptor $subject,
        $customer,
        $password = null,
        $redirectUrl = ''
    )
    {
        $dob = $customer->getDob();

        if (empty($dob))
        {
            $customer->setDob('1900-01-01');
        }

        $typeOfCustomer = $customer->getCustomAttribute('type_of_customer');

        if (empty($typeOfCustomer))
        {
            $attribute = $this->eavConfig->getAttribute(\Magento\Customer\Model\Customer::ENTITY, 'type_of_customer');
            $attributeOptions = $attribute->getSource()->getAllOptions();
            $professionalValue = '';

            foreach ($attributeOptions as $option)
            {
                if (strpos(strtolower($option['label']), 'professional') !== FALSE)
                {
                    $professionalValue = $option['value'];
                }
            }

            if ($professionalValue)
            {
                $industryValue = '';
                $industryAttribute = $this->eavConfig->getAttribute(
                    \Magento\Customer\Model\Customer::ENTITY,
                    'professional_industry'
                );
                $industryAttributeOptions = $industryAttribute->getSource()->getAllOptions();

                foreach ($industryAttributeOptions as $option)
                {
                    if (strpos($option['label'], 'Automotive - Technician') !== FALSE)
                    {
                        $industryValue = $option['value'];
                    }
                }

                if ($industryValue)
                {

                    $customer->setCustomAttribute('type_of_customer', $professionalValue);
                    $customer->setCustomAttribute('professional_industry', $industryValue);
                }
            }
        }

        return [$customer, $password, $redirectUrl];
    }
}