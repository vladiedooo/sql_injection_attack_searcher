<?php
/**
 * Author: Sean Dunagan (github: dunagan5887)
 * Date: 1/3/17
 */

namespace Scc\AEMCatalog\Plugin\Helper;

/**
 * Class Image
 * @package Scc\Catalog\Plugin\Helper
 */
class ImagePlugin extends \Magento\Catalog\Helper\Image
{
    /**
     * @var \Magento\Catalog\Helper\Image\Interceptor
     */
    protected $_interceptedImageHelper;

    /**
     * Initialize base image file
     *
     * This method is the analog of \Magento\Catalog\Helper\Image::initBaseFile()
     *
     * @return $this
     */
    public function aemInitBaseFile()
    {
        $model = $this->_interceptedImageHelper->_getModel();
        /* @var \Scc\AEMCatalog\Model\Product\Image $model */
        $model->setAemBaseFile($this->_interceptedImageHelper->getProduct(), $model->getDestinationSubdir());
        return $this;
    }

    /**
     * Need to produce the AEM image url
     *
     * This method is the analog of \Magento\Catalog\Helper\Image::getUrl()
     *
     * @param \Magento\Catalog\Helper\Image\Interceptor $interceptor
     * @param \Closure $proceed
     * @return string
     */
    public function aroundGetUrl($interceptor, $proceed)
    {
        $this->_interceptedImageHelper = $interceptor;
        try {
            $this->aemApplyScheduledActions();
            return $this->_interceptedImageHelper->_getModel()->getUrl();
        } catch (\Exception $e) {
            // TODO Handle this with custom functionality
            return $this->getDefaultPlaceholderUrl();
        }
    }

    /**
     * This method will return data regarding the image size if it has been resized
     *
     * This method is the analog of \Magento\Catalog\Helper\Image::getResizedImageInfo()
     *
     * @param \Magento\Catalog\Helper\Image\Interceptor $interceptor
     * @param \Closure $proceed
     * @return array
     */
    public function aroundGetResizedImageInfo($interceptor, $proceed)
    {
        $this->_interceptedImageHelper = $interceptor;
        $this->aemApplyScheduledActions();
        // TODO Need to return the resized image info
        return $this->_interceptedImageHelper->_getModel()->getResizedImageInfo();
    }

    /**
     * This method is the analog of \Magento\Catalog\Helper\Image::applyScheduledActions()
     *
     * @return $this
     */
    public function aemApplyScheduledActions()
    {
        $this->aemInitBaseFile();
        return $this;
    }
}
