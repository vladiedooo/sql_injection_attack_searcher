<?php

namespace Scc\Customers\Setup\Data\Options\Professional;

/**
 * Class Industry
 * @package Scc\Customers\Setup\Data\Options\Professional\Industry
 */
class Industry
{
    /**
     * @return array
     */
    public function getOptionArray()
    {
        return array(
            'values' => array(
                'Agriculture',
                'Automotive - Auto Body',
                'Automotive - Repair and/or Restoration',
                'Automotive - Technician',
                'Aviation',
                'Construction',
                'General Repair & Maintenance',
                'Industrial - MRO',
                'Industrial - Technician',
                'Marine',
                'Mining',
                'Other'
            )
        );
    }
}
