<?php
/**
 * Author: Sean Dunagan (https://github.com/dunagan5887)
 * Created: 4/14/16
 */

namespace Dunagan\Base\Controller\Adminhtml;

/**
 * Author: Sean Dunagan (https://github.com/dunagan5887)
 * Created: 4/19/16
 *
 * Class GridAndFormController
 * @package Dunagan\Base\Controller\Adminhtml
 *
 * This Controller class provides abstract functionality
 */
abstract class GridAndFormController extends BaseController implements GridAndFormControllerInterface
{
    const EXCEPTION_SAVE_OBJECT_SHOW = 'An error occurred while attempting to %1 the %2 object: %3';
    const EXCEPTION_SAVE_OBJECT_LOG = "An exception occurred while attempting to %1 the %2 object: %3\nThe object's serialized data was: %4";

    const FORM_ELEMENT_NAME_TEMPLATE = '%s[%s]';

    /**
     * {@inheritdoc}
     */
    abstract public function getModelIdentifierField();

    /**
     * {@inheritdoc}
     */
    abstract public function getModelClass();

    /**
     * {@inheritdoc}
     */
    abstract public function getModelDescription();

    /**
     * {@inheritdoc}
     */
    abstract public function getModelEditBlockLayoutName();

    /**
     * {@inheritdoc}
     */
    abstract public function getModuleName();

    /**
     * {@inheritdoc}
     */
    abstract public function getBlockAdminhtmlPathSuffix();

    /**
     * This method is anticipated to be overridden by subclasses to implement data validation
     *
     * @param \Magento\Framework\Model\AbstractModel $objectToSave
     * @param array $posted_object_data
     * @return \Magento\Framework\Model\AbstractModel $objectToSave
     */
    public function validateDataAndCreateObject($objectToSave, $posted_object_data)
    {
        $objectToSave->setData($posted_object_data);
        return $objectToSave;
    }

    /**
     * This method is anticipated to be overridden by subclasses to implement data validation
     *
     * @param \Magento\Framework\Model\AbstractModel $objectToSave
     * @param array $posted_object_data
     * @return \Magento\Framework\Model\AbstractModel $objectToSave
     */
    public function validateDataAndUpdateObject($objectToSave, $posted_object_data)
    {
        $objectToSave->addData($posted_object_data);
        return $objectToSave;
    }

    /**
     * This is the controller action which is executed when an admin user visits the form page
     */
    public function executeEditAction()
    {
        $this->_initAction();
        // Set the action url on the edit block
        $model_edit_form_container_block_class = $this->getFormContainerBlockClass();
        $model_edit_block_layout_name = $this->getModelEditBlockLayoutName();
        // Create the form container block and add it to the content container
        $modelEditBlock = $this->_view->getLayout()->addBlock($model_edit_form_container_block_class,
                                                              $model_edit_block_layout_name, 'content');
        // Set the form POST action for the form
        $modelEditBlock->setData('action', $this->getUrl('*//save'));
        // Set the page description
        $model_description = $this->getModelDescription();
        $page_description = $this->isObjectExistingBeingEdited() ? __('Edit %1', $model_description)
                                                                 : __('Create %1', $model_description);
        // Add breadcrumb values
        $this->_addBreadcrumb($page_description, $page_description);
        // Add the page description to the page title
        $this->_view->getPage()->getConfig()->getTitle()->prepend($page_description);

        $this->_view->renderLayout();
    }

    /**
     * This is the action which executes the request to provide a form page for a new ORM object
     * At this point the executeNewAction() is coded to process both creations and updates
     */
    public function executeNewAction()
    {
        $this->executeEditAction();
    }

    /**
     * This is the action which executes the request to persist the ORM object creation/update to the database
     */
    public function executeSaveAction()
    {
        if ($this->getRequest()->getPostValue())
        {
            // Initialize this outside of the try-catch block so we can use it in the catch exception logging block
            $object_data_array = null;

            try
            {
                // Initialize the ORM object based on the id field passed in with the form POST
                $objectToSave = $this->_initializeObjectFromParamData();
                /** @var $objectToSave \Magento\Framework\Model\AbstractModel */
                $post_data = $this->getRequest()->getPostValue();
                // Get the post data relevant to the object being created/updated
                $object_data_array_parameter = $this->getObjectDataArrayParameter();
                $object_data_array = isset($post_data[$object_data_array_parameter])
                                        ? $post_data[$object_data_array_parameter] : null;
                if (is_null($object_data_array))
                {
                    // This should never happen, but account for the scenario where it does
                    throw new \Exception(self::GENERIC_CUSTOMER_FACING_MESSAGE);
                }

                if ($this->isObjectExistingBeingEdited())
                {
                    // Update the existing ORM object
                    $action_text = 'updated';
                    $this->validateDataAndUpdateObject($objectToSave, $object_data_array);
                }
                else
                {
                    // Create a new ORM object
                    $action_text = 'created';
                    $this->validateDataAndCreateObject($objectToSave, $object_data_array);
                }

                $objectToSave->save();
                $model_description = $this->getModelDescription();
                $this->messageManager->addSuccess(__('%1 was successfully %2.', $model_description, $action_text));

                $this->_redirect('*/*/edit', [$this->getModelIdentifierField() => $objectToSave->getId()]);
                return;
            }
            catch (\Exception $e)
            {
                $model_description = $this->getModelDescription();
                $action_text = $this->isObjectExistingBeingEdited() ? 'update' : 'create';
                $serialized_data = serialize($object_data_array);
                // Show an error message describing the exception to the admin user
                $error_to_show = __(self::EXCEPTION_SAVE_OBJECT_SHOW, $action_text, $model_description, $e->getMessage());
                $this->messageManager->addError($error_to_show);
                // Log an error message which includes the serialized form data
                $error_to_log = __(self::EXCEPTION_SAVE_OBJECT_LOG, $action_text, $model_description, $e->getMessage(),
                                    $serialized_data);
                $this->_objectManager->get('Psr\Log\LoggerInterface')->critical($error_to_log);

                if ($this->isObjectExistingBeingEdited())
                {
                    // Redirect back to the update page for the existing ORM object
                    $model_id_field = $this->getModelIdentifierField();
                    $this->_redirect('*/*/edit', [$model_id_field => $this->getRequest()->getParam($model_id_field)]);
                }
                else
                {
                    // Redirect back to the creation page for a new ORM object
                    $this->_redirect('*/*/new');
                }

                return;
            }
        }
        else
        {
            $this->messageManager->addError(
                __('There was an issue with your request. Please try again.')
            );
        }
        $this->_redirect('*/*/');
    }

    /**
     * This method initializes page data as well as initializing the object from the parameter data
     *
     * @throws \Exception
     */
    protected function _initAction()
    {
        parent::_initAction();

        $this->_initializeObjectFromParamData();
    }

    /**
     * This method initializes an ORM object based on the $identifier_field value included either in the HTML
     *  GET or POST.
     *
     * @return \Magento\Framework\Model\AbstractModel
     * @throws \Exception
     */
    protected function _initializeObjectFromParamData()
    {
        // Get the primary key field for this ORM object
        $identifier_field = $this->getModelIdentifierField();
        // Get the primary key value for this request
        $id = $this->getRequest()->getParam($identifier_field);
        // Create an ORM model of the specific class
        $objectToEdit = $this->_objectManager->create($this->getModelClass());
        if ($id)
        {
            // Load the ORM object from the database based on the $id value provided in the HTML GET or POST
            $objectToEdit->load($id);
            if (!$objectToEdit->getId())
            {
                // A primary key value was provided, but no row was found in the database table with that primary key value
                $model_description = $this->getModelDescription();
                $error_message = __('No %1 was found in the system with id %2', $model_description, $id);
                throw new \Exception($error_message);
            }
        }
        // Store the ORM object in the registry so that the block classes can execute functionality based off of whether
        //      we are updating an existing object or creating a new object, as well as populate data into input fields
        //      for an existing ORM object
        $this->_coreRegistry->register('current_object', $objectToEdit);
        return $objectToEdit;
    }

    /**
     * Returns the ORM object stored in the registry. This method is accessed by the block classes
     *
     * @return \Magento\Framework\Model\AbstractModel
     */
    public function getObjectToEdit()
    {
        return $this->_coreRegistry->registry('current_object');
    }

    /**
     * Returns whether the request is editing/saving an existing object or a new object
     *
     * @return bool
     */
    public function isObjectExistingBeingEdited()
    {
        return (bool)$this->getObjectToEdit()->getId();
    }

    /**
     * Returns the HTML form element name for the field passed in. It will return a string of the form
     *      array_name['field_name']
     *
     * @param string $field_name
     * @return string
     */
    public function getFormElementName($field_name)
    {
        $form_data_array_parameter = $this->getObjectDataArrayParameter();
        return sprintf(self::FORM_ELEMENT_NAME_TEMPLATE, $form_data_array_parameter, $field_name);
    }

    /**
     * Returns the name of the array which should contain all of the ORM data filled in the form by the admin user on
     *      the form page
     *
     * @return string
     */
    public function getObjectDataArrayParameter()
    {
        return 'edited_object_data';
    }

    /**
     * Returns the form container block class that should be used to render this page.
     * Subclasses can override this method if they need custom container block functionality
     *
     * @return string
     */
    public function getFormContainerBlockClass()
    {
        return 'Dunagan\Base\Block\Adminhtml\Widget\Form\Container';
    }
}
