<?php
/**
 * Author: Sean Dunagan (github: dunagan5887)
 * Date: 12/21/16
 */

namespace Scc\Catalog\Model;

/**
 * Class ProductRepository
 * @package Scc\Catalog\Model
 */
class ProductRepository
        extends \Magento\Catalog\Model\ProductRepository
        implements \Scc\Catalog\Api\ProductRepositoryInterface
{
    /**
     * {@inheritdoc}
     */
    public function getSelectList(\Scc\Framework\Api\SearchCriteriaInterface $searchCriteria)
    {
        // Check if the $searchCriteria defined any select fields for the products' queries
        $select_fields = $searchCriteria->getSelectFields();
        if (is_array($select_fields) && (!empty($select_fields)))
        {
            // Need to update $this->searchCriteriaBuilder to only include the attributes defined in $select_fields
            $this->searchCriteriaBuilder->addFilter('attribute_code', $select_fields, 'in');
        }

        return parent::getList($searchCriteria);
    }
}
