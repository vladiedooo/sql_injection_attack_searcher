<?php

namespace Scc\Customers\Block\Account;

use \Magento\Framework\Api\SearchCriteriaBuilder;
use \Magento\Customer\Api\CustomerRepositoryInterface;
use \Magento\Customer\Model\ResourceModel\GroupRepository;


class Premium extends \Magento\Framework\View\Element\Template
{
    protected $customerSession;
    private $customerRepository;
    private $groupRepository;

    /**
     * Construct
     *
     * @param \Magento\Framework\View\Element\Template\Context $context
     * @param \Magento\Customer\Model\Session $customerSession
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Magento\Customer\Model\Session $customerSession,
        \Magento\Customer\Api\CustomerRepositoryInterface $customerRepository,
        \Magento\Customer\Model\ResourceModel\GroupRepository $groupRepository,
        array $data = []
    ) {
        parent::__construct($context, $data);
        $this->customerSession = $customerSession;
        $this->customerRepository = $customerRepository;
        $this->groupRepository = $groupRepository;
    }

    public function getBlockName()
    {
        if ($this->customerSession->isLoggedIn())
        {
            $customer = $this->customerRepository->getById($this->customerSession->getCustomer()->getId());
            $groupId = $customer->getGroupId();
            $group = $this->groupRepository->getById($groupId);
            $code = $group->getCode();

            return $code.'_customer_dashboard';
        }
        return '';
    }
}