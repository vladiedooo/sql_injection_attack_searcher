<?php
/**
 * Author: Sean Dunagan (github: dunagan5887)
 * Date: 1/27/17
 */

namespace Scc\AEMConfigurableProduct\Plugin\Helper;

/**
 * Class DataPlugin
 * @package Scc\AEMConfigurableProduct\Plugin\Helper
 */
class DataPlugin extends \Magento\ConfigurableProduct\Helper\Data
{
    /**
     * @var \Scc\AEMCatalog\Model\Product\Image
     */
    protected $_sccAemCatalogProductImageModel;

    /**
     * @var array
     */
    protected $_image_types = array('img' => 'product_page_image_medium', 'full' => 'product_page_image_large',
                                    'thumb' => 'product_page_image_small');

    /**
     * @param $interceptor
     * @param $proceed
     * @param $currentProduct
     * @param $allowedProducts
     */
    public function aroundGetOptions($interceptor, $proceed, $currentProduct, $allowedProducts)
    {
        $options = $proceed($currentProduct, $allowedProducts);
        // Unset the $options['images'] array if it has been set
        unset($options['images']);
        $options['images'] = array();
        // Iterate over the allowed products, producing the images for each
        foreach($allowedProducts as $allowedProduct)
        {
            $product_id = $allowedProduct->getId();
            $options['images'][$product_id] = array();
            $options['images'][$product_id][0] = array();
            // For each product, we need the image urls for the thumbnail and main images
            foreach($this->_image_types as $option_array_index => $image_type)
            {
                $this->_sccAemCatalogProductImageModel->setAemBaseFile($allowedProduct, $image_type);
                $options['images'][$product_id][0][$option_array_index] = $this->_sccAemCatalogProductImageModel->getUrl();
            }
            // Set the ancillary fields as well
            $options['images'][$product_id][0]['caption'] = null;
            $options['images'][$product_id][0]['position'] = '1';
            $options['images'][$product_id][0]['isMain'] = true;
        }

        return $options;
    }

    /**
     * DataPlugin constructor.
     * @param \Magento\Catalog\Helper\Image $imageHelper
     * @param \Scc\AEMCatalog\Model\Product\Image $sccAemCatalogProductImageModel
     */
    public function __construct(\Magento\Catalog\Helper\Image $imageHelper,
                                \Scc\AEMCatalog\Model\Product\Image $sccAemCatalogProductImageModel)
    {
        parent::__construct($imageHelper);

        $this->_sccAemCatalogProductImageModel = $sccAemCatalogProductImageModel;
    }
}
