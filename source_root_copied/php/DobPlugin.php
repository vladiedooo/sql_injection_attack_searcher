<?php

namespace Scc\Customers\Plugin\Block;

class DobPlugin
{

    public function afterGetFieldHtml(
        \Magento\Customer\Block\Widget\Dob $subject,
        $result
    )
    {
        $result = str_replace('yearRange: "-120y:c+nn",', 'yearRange: "1900:", defaultDate: "01/01/1920",', $result);
        $result = str_replace('dateFormat: "M/d/yy",', 'dateFormat: "mm/dd/yy",', $result);

        return $result;
    }

    public function afterGetDateFormat(\Magento\Customer\Block\Widget\Dob $subject, $result)
    {
        $result = 'MM/dd/yyyy';

        return $result;
    }
}