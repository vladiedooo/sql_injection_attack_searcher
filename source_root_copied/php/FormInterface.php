<?php
/**
 * Author: Sean Dunagan (https://github.com/dunagan5887)
 * Created: 4/14/16
 */

namespace Dunagan\Base\Block\Adminhtml\Widget;

/**
 * Author: Sean Dunagan (https://github.com/dunagan5887)
 * Created: 4/19/16
 *
 * Interface FormInterface
 * @package Dunagan\Base\Block\Adminhtml\Widget
 *
 * This is an interface for block classes which display forms for creating/updating objects in the admin panel
 */
interface FormInterface
{
    /**
     * This method should add all of the necessary data fields to the fieldset of the form which will be submitted
     *      to update an existing object
     *
     * @param \Magento\Framework\Data\Form\Element\Fieldset $fieldset
     */
    public function addFieldsForExistingObject($fieldset);

    /**
     * This method should add all of the necessary data fields to the fieldset of the form which will be submitted
     *      to create a new object
     *
     * @param \Magento\Framework\Data\Form\Element\Fieldset $fieldset
     */
    public function addFieldsForNewObject($fieldset);
}