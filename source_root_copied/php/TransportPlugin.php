<?php
/**
 * Author: Sean Dunagan (github: dunagan5887)
 * Date: 9/28/16
 */

namespace Scc\Campaign\Plugin\Framework\Mail;

/**
 * Because all emails should be sent out via the Campaign system, no emails should be sent out using the native
 *      Magento functionality. This plugin was created to prevent emails from attempting to be sent out
 *
 * Class MailTransportPlugin
 * @package Scc\Campaign\Model\Plugin
 */
class TransportPlugin
{
    const EXCEPTION_DISPATCHING_CAMPAIGN_EMAIL_EVENT = 'An exception occurred while attempting to dispatch the campaign email event for email template %1: %2';

    /**
     * @var \Scc\Campaign\Plugin\Framework\Mail\Template\TransportBuilderPlugin
     */
    protected $_mailTemplateTransportBuilderPlugin;

    /**
     * @var \Scc\Campaign\Helper\Template
     */
    protected $_sccEmailTemplateHelper;

    /**
     * @var \Magento\Framework\Event\ManagerInterface
     */
    protected $_eventManager;

    /**
     * @var \Scc\Campaign\Model\LoggerInterface
     */
    protected $_logger;

    /**
     * @param \Magento\Framework\Mail\Transport\Interceptor $interceptor
     * @param callable $proceed
     * @return bool
     */
    public function aroundSendMessage($interceptor, callable $proceed)
    {
        // Instead of actually sending a message, fire off an event to queue up a transmission to the Campaign system
        $email_template_id = $this->_mailTemplateTransportBuilderPlugin->getTemplateIdentifier();
        if (empty($email_template_id))
        {
            return false;
        }

        $should_execute_native_email_functionality = true;
        // See if this email template should be handled by Campaign
        if ($this->_sccEmailTemplateHelper->isEmailTemplateOverridden($email_template_id))
        {
            try
            {
                $template_vars = $this->_mailTemplateTransportBuilderPlugin->getTemplateVars();
                $template_options = $this->_mailTemplateTransportBuilderPlugin->getTemplateOptions();
                $event_name = 'scc_campaign_transport_' . $email_template_id;
                $eventParameters = array('template_id' => $email_template_id, 'template_vars' => $template_vars,
                                         'template_options' => $template_options);
                $this->_eventManager->dispatch($event_name, $eventParameters);
                // If we have reached this point and no exceptions have been thrown, assume Campaign task was queued
                //      appropriately
                $should_execute_native_email_functionality = false;
            }
            catch(\Exception $e)
            {
                $error_message = __(self::EXCEPTION_DISPATCHING_CAMPAIGN_EMAIL_EVENT, $email_template_id, $e->getMessage());
                $this->_logger->critical($error_message);
                // We need to assume that a task was not successfully queued to send an email here. As such, we need to
                //      send the email with native Magento functionality
                $should_execute_native_email_functionality = true;
            }
        }

        if ($should_execute_native_email_functionality)
        {
            // Native Magento functionality should handle sending out this email
            return $proceed();
        }

        return false;
    }

    /**
     * MailTransportPlugin constructor.
     * @param \Scc\Campaign\Plugin\Framework\Mail\Template\TransportBuilderPlugin $mailTemplateTransportBuilderPlugin
     * @param \Scc\Campaign\Helper\Template $emailTemplateHelper
     * @param \Magento\Framework\Event\ManagerInterface $eventManager
     * @param \Scc\Campaign\Model\LoggerInterface $logger
     */
    public function __construct(\Scc\Campaign\Plugin\Framework\Mail\Template\TransportBuilderPlugin $mailTemplateTransportBuilderPlugin,
                                \Scc\Campaign\Helper\Template $emailTemplateHelper,
                                \Magento\Framework\Event\ManagerInterface $eventManager,
                                \Scc\Campaign\Model\LoggerInterface $logger)
    {
        $this->_mailTemplateTransportBuilderPlugin = $mailTemplateTransportBuilderPlugin;
        $this->_sccEmailTemplateHelper = $emailTemplateHelper;
        $this->_eventManager = $eventManager;
        $this->_logger = $logger;
    }
}
