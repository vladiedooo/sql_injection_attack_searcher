<?php
/**
 * Author: Sean Dunagan (github: dunagan5887)
 * Date: 9/18/16
 */

namespace Scc\WebApi\Model\Logger;

use Monolog\Logger;

/**
 * Logging handler to log error messages
 *
 * Class ErrorHandler
 * @package Scc\WebApi\Model\Logger
 */
class ErrorHandler extends \Magento\Framework\Logger\Handler\Base implements ErrorHandlerInterface
{
    /**
     * Logging level
     * @var int
     */
    protected $loggerType = Logger::ERROR;

    /**
     * File name
     * @var string
     */
    protected $fileName = '/var/log/scc_web_api_error.log';
}
