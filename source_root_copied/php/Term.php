<?php
/**
 * Author: Sean Dunagan (github: dunagan5887)
 * Date: 1/5/17
 */

namespace Scc\PremiumMembership\Model\Source\Registration;

/**
 * Class Term
 * @package Scc\PremiumMembership\Model\Source\Registration
 */
class Term
{
    const ONE_YEAR_OF_DAYS_VALUE = 365;
    const ONE_YEAR_OF_DAYS_LABEL = '365 Days';

    /**
     * @return array
     */
    public function getValues()
    {
        return [self::ONE_YEAR_OF_DAYS_LABEL];
    }
}
