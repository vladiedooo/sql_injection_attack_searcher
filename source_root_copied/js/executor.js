/**
* Copyright 2016 aheadWorks. All rights reserved.
* See LICENSE.txt for license details.
*/

define([], function () {
    'use strict';

    return {
        /**
         * Submit request
         *
         * @param {String} url
         * @returns {Object}
         */
        submit: function (url) {},

        /**
         * Get request result
         *
         * @returns {Object|null}
         */
        getResult: function () {}
    };
});
