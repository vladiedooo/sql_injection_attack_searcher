<?php
/**
 * Author: Sean Dunagan (github: dunagan5887)
 * Date: 4/26/17
 */

namespace Scc\Campaign\Model\Api\Url;

/**
 * Class ChangeMembershipJsspFactory
 *
 * @package Scc\Campaign\Model\Api\Url
 */
interface ChangeMembershipJsspFactoryInterface extends FactoryInterface
{
    /**
     * Returns the array index in which the Change Membership URL Factory expects to receive the customer email address
     *  value
     *
     * @return string
     */
    public function getExpectedCustomerEmailAddressIndex();

    /**
     * Returns the array index in which the Change Membership URL Factory expects to receive the customer id
     *
     * @return string
     */
    public function getExpectedCustomerIdIndex();

    /**
     * Returns the array index in which the Change Membership URL Factory expects to receive the membership value
     *
     * @return string
     */
    public function getExpectedMembershipValueIndex();

    /**
     * Returns the array index in which the Change Membership URL Factory expects to receive the website id
     *
     * @return string
     */
    public function getExpectedWebsiteIdIndex();
}
