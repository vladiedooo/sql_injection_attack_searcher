require([
    'jquery'
], function ($) {
    $(function () { // to ensure that code evaluates on page load
        $(document).scroll(function() {
            if($(window).scrollTop() === 0)
            { $('.navigation-container').removeClass('fixed-nav'); $('body').removeClass('fixed-nav'); $('.pencil-nav').removeClass('fixed-nav'); }
        });
    });
});
