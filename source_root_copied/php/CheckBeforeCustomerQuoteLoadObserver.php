<?php
/**
 * Author: Sean Dunagan (github: dunagan5887)
 * Date: 2/28/17
 */

namespace Scc\PremiumMembership\Observer\Checkout;

use Magento\Framework\Event\ObserverInterface;

/**
 * Class Before
 * @package Scc\PremiumMembership\Observer\Customer
 */
class CheckBeforeCustomerQuoteLoadObserver implements ObserverInterface
{
    const ERROR_PREMIUM_MEMBERSHIP_PRODUCT_IN_QUOTE = 'Customer with email %1 is already a Premium Member. As such, this customer can not buy the Premium Membership product. This product will be removed from the cart.';
    const EXCEPTION_CHECKING_CUSTOMER_QUOTE_FOR_PREMIUM_MEMBERSHIP = 'An exception occurred while checkout cart for customer %1 with quote id %2 for the premium membership product: %3';

    /**
     * @var \Scc\PremiumMembership\Helper\Product
     */
    protected $_sccPremiumMembershipProductHelper;

    /**
     * @var \Magento\Framework\Message\ManagerInterface
     */
    protected $_messageManager;

    /**
     * @var \Scc\PremiumMembership\Helper\Customer
     */
    protected $_sccPremiumMembershipCustomerHelper;

    /**
     * @var \Magento\Customer\Model\Session
     */
    protected $_customerSession;

    /**
     * @var \Magento\Quote\Api\CartRepositoryInterface
     */
    protected $_quoteRepository;

    /**
     * @var \Psr\Log\LoggerInterface
     */
    protected $_logger;

    /**
     * Checks to make sure that this is not a Premium Customer loading a cart with the Premium Membership Product
     *
     * @param \Magento\Framework\Event\Observer $observer
     * @return $this
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        $checkoutSession = $observer->getData('checkout_session');
        /* @var \Magento\Checkout\Model\Session $checkoutSession */
        $quote = $checkoutSession->getQuote();
        /* @var \Magento\Quote\Model\Quote $quote */

        try
        {
            // Check that the current user is logged in and is a Premium Member
            if (!$this->_sccPremiumMembershipCustomerHelper->isSessionCustomerPremium())
            {
                // If not, return
                return;
            }

            $quoteItems = $quote->getItemsCollection();
            /* @var \Magento\Quote\Model\ResourceModel\Quote\Item\Collection $quoteItems */
            $premium_membership_product_was_removed = false;
            foreach($quoteItems as $quoteItem)
            {
                /* @var \Magento\Quote\Model\Quote\Item $quoteItem */
                $sku = $quoteItem->getSku();
                if ($this->_sccPremiumMembershipProductHelper->isPremiumMembershipSku($sku))
                {
                    // Remove the item from the quote in this case
                    $premium_membership_product_was_removed = true;
                    $quote->removeItem($quoteItem->getId());
                    $customer_email = $this->_customerSession->getCustomer()->getEmail();
                    $error_message = __(self::ERROR_PREMIUM_MEMBERSHIP_PRODUCT_IN_QUOTE, $customer_email);
                    $this->_messageManager->addErrorMessage($error_message);
                }
            }

            if ($premium_membership_product_was_removed)
            {
                $this->_quoteRepository->save($quote);
            }
        }
        catch(\Exception $e)
        {
            $customer_id = ($this->_customerSession->isLoggedIn())
                                ? $this->_customerSession->getCustomer()->getId() : 'guest';

            $error_message = __(self::EXCEPTION_CHECKING_CUSTOMER_QUOTE_FOR_PREMIUM_MEMBERSHIP,
                                $customer_id, $quote->getId(), $e->getMessage());
            $this->_logger->error($error_message);
        }
    }

    /**
     * CheckBeforeCustomerQuoteLoadObserver constructor.
     * @param \Scc\PremiumMembership\Helper\Product $sccPremiumMembershipProductHelper
     * @param \Scc\PremiumMembership\Helper\Customer $sccPremiumMembershipCustomerHelper
     * @param \Magento\Framework\Message\ManagerInterface $messageManager
     * @param \Magento\Customer\Model\Session $customerSession
     * @param \Magento\Quote\Api\CartRepositoryInterface $quoteRepository
     * @param \Psr\Log\LoggerInterface $logger
     */
    public function __construct(\Scc\PremiumMembership\Helper\Product $sccPremiumMembershipProductHelper,
                                \Scc\PremiumMembership\Helper\Customer $sccPremiumMembershipCustomerHelper,
                                \Magento\Framework\Message\ManagerInterface $messageManager,
                                \Magento\Customer\Model\Session $customerSession,
                                \Magento\Quote\Api\CartRepositoryInterface $quoteRepository,
                                \Psr\Log\LoggerInterface $logger)
    {
        $this->_sccPremiumMembershipProductHelper = $sccPremiumMembershipProductHelper;
        $this->_sccPremiumMembershipCustomerHelper = $sccPremiumMembershipCustomerHelper;
        $this->_messageManager = $messageManager;
        $this->_customerSession = $customerSession;
        $this->_quoteRepository = $quoteRepository;
        $this->_logger = $logger;
    }
}
