<?php
/**
 * Author: Sean Dunagan (github: dunagan5887)
 * Date: 12/21/16
 */

namespace Scc\Framework\Api;

/**
 * Class SearchCriteria
 * @package Scc\Framework\Api
 */
class SearchCriteria extends \Magento\Framework\Api\SearchCriteria implements SearchCriteriaInterface
{
    const SELECT_FIELDS = 'select_fields';

    /**
     * {@inheritdoc}
     */
    public function getSelectFields()
    {
        $select_fields = $this->_get(self::SELECT_FIELDS);
        return is_array($select_fields) ? $select_fields : [];
    }

    /**
     * {@inheritdoc}
     */
    public function setSelectFields(array $select_fields = [])
    {
        return $this->setData(self::SELECT_FIELDS, $select_fields);
    }
}
