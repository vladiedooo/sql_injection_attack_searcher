<?php
/**
 * Author: Sean Dunagan (github: dunagan5887)
 * Date: 2/23/17
 */

namespace Scc\AEMTheme\Block\Head;

use Magento\Framework\View\Element\Template\Context;

/**
 * Class Shared
 * @package Scc\AEMTheme\Block\Head
 */
class Shared extends \Magento\Framework\View\Element\Template
{
    protected $_sharedThemeConfig;

    /**
     * @return string
     */
    protected function _toHtml()
    {
        return $this->_sharedThemeConfig->getSharedThemeAssetsToLinkHtml();
    }

    /**
     * Index constructor.
     * @param Context $context
     * @param \Scc\AEMTheme\Helper\Theme\Shared\Config $sharedThemeConfig
     * @param array $data
     */
    public function __construct(Context $context, \Scc\AEMTheme\Helper\Theme\Shared\Config $sharedThemeConfig,
                                array $data = [])
    {
        parent::__construct($context, $data);

        $this->_sharedThemeConfig = $sharedThemeConfig;
    }
}
