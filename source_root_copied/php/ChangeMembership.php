<?php
/**
 * Author: Sean Dunagan (github: dunagan5887)
 * Date: 4/25/17
 */

namespace Scc\Campaign\Model\Task\Queue\Membership;

/**
 * Class ChangeMembership
 *
 * @package Scc\Campaign\Model\Task\Queue\Membership
 */
class ChangeMembership extends \Dunagan\WebApi\Model\Task\Api\Transmission implements ChangeMembershipInterface
{
    /**
     * {@inheritdoc}
     */
    public function queueChangeMembershipTask($customer_id, $membership_value, $website_id)
    {
        $task_arguments = array($this->getApiTransmissionService()->getExpectedCustomerIdIndex() => $customer_id,
                                $this->getApiTransmissionService()->getExpectedMembershipValueIndex() => $membership_value,
                                $this->getApiTransmissionService()->getExpectedWebsiteIdIndex() => $website_id);

        $serialized_task_arguments = serialize($task_arguments);
        // Since customers can change membership multiple times, we do not want this task to be unique
        $nonUniqueTaskSingleton = $this->_taskSingleton;
        /* @var $uniqueTaskSingleton \Dunagan\ProcessQueue\Model\Task */
        $createdTask = $nonUniqueTaskSingleton->getResource()->createTask($this->getTaskCode(), get_class($this),
                                                                          $this->_task_method, $serialized_task_arguments,
                                                                          null, false, $customer_id);

        return $createdTask;
    }

    /**
     * THIS METHOD CURRENTLY NOT BEING USED AS IT'S NOT OPTIMAL FOR THE OBSERVER WHICH INVOKES THE QUEUEING OF THESE TASKS
     *
     * {@inheritdoc}
     */
    public function queueEntityTransmissionToExternalSystem($entity, array $additional_arguments_array = array(),
                                                            $queue_in_processing_state = false)
    {
        $expected_membership_index = $this->getApiTransmissionService()->getExpectedMembershipValueIndex();
        $expected_website_id_index = $this->getApiTransmissionService()->getExpectedWebsiteIdIndex();
        return $this->queueChangeMembershipTask($entity->getId(),
                                                $additional_arguments_array[$expected_membership_index],
                                                $additional_arguments_array[$expected_website_id_index]);
    }

    /**
     * @return \Scc\Campaign\Model\Api\Service\ChangeMembershipInterface
     */
    public function getApiTransmissionService()
    {
        return $this->_objectManager->get('\Scc\Campaign\Model\Api\Service\ChangeMembershipInterface');
    }

    /**
     * {@inheritdoc}
     */
    protected function _loadEntityToTransmit($entity_id)
    {
        $customerObject = $this->_objectManager->create('\Magento\Customer\Model\Customer')->load($entity_id);
        return $customerObject;
    }

    /**
     * {@inheritdoc}
     */
    public function getTaskCode()
    {
        return 'scc_campaign_transmit_membership_change';
    }

    /**
     * {@inheritdoc}
     */
    public function getExternalSystemDescription()
    {
        return 'Campaign';
    }

    /**
     * {@inheritdoc}
     */
    public function getEntityDescription()
    {
        return 'customer';
    }

    /**
     * Transmission constructor.
     * @param \Magento\Framework\ObjectManagerInterface $objectManager
     */
    public function __construct(\Magento\Framework\ObjectManagerInterface $objectManager)
    {
        parent::__construct($objectManager);

        $apiTransmissionService = $this->getApiTransmissionService();
        /* @var \Scc\Campaign\Model\Api\Service\ChangeMembershipInterface $apiTransmissionService */
        $this->_id_index = $apiTransmissionService->getExpectedCustomerIdIndex();
    }
}
