<?php
/**
 * Author: Sean Dunagan (https://github.com/dunagan5887)
 * Created: 4/14/16
 */

namespace Dunagan\Base\Controller\Adminhtml;

/**
 * Author: Sean Dunagan (https://github.com/dunagan5887)
 * Created: 4/19/16
 *
 * Interface GridAndFormControllerInterface
 * @package Dunagan\Base\Controller\Adminhtml
 *
 * This interface defines functionality that a controller should define for admin grid and form pages.
 * These are grid pages which provide a grid of all rows in a database table. Clicking on a row in the grid leads the
 *  user to a form page allowing the user to update values in the row. The grid can also allow the admin user to define
 *  a new row to be entered into the database table
 */
interface GridAndFormControllerInterface extends ControllerInterface
{
    /**
     * This method should return the primary key field for the database table which is being updated via the form page
     *
     * @return string
     */
    public function getModelIdentifierField();

    /**
     * This method should define any data validation regarding the data values which were entered in the admin form by
     *      the admin user. The method is also expected to set the posted data on the newly created ORM object
     *
     * @param \Magento\Framework\Model\AbstractModel $objectToSave
     * @param array $posted_object_data
     * @return \Magento\Framework\Model\AbstractModel
     */
    public function validateDataAndCreateObject($objectToSave, $posted_object_data);

    /**
     * This method should define any data validation regarding the data values which were entered in the admin form by
     *      the admin user. The method is also expected to set the posted data on the ORM object representing the row
     *      being updated in the database
     *
     * @param \Magento\Framework\Model\AbstractModel $objectToSave
     * @param array $posted_object_data
     * @return \Magento\Framework\Model\AbstractModel
     */
    public function validateDataAndUpdateObject($objectToSave, $posted_object_data);

    /**
     * This method should return the model class for the ORM object being updated/created.
     * For example, if the ORM object is created via the call $this->_objectManager->create('\Some\Module\Model\ORM');
     *  then this method should return '\Some\Module\Model\ORM'
     *
     * @return string
     */
    public function getModelClass();

    /**
     * This method should return an English description of the ORM object being updated/created.
     * For example, if the database table being updated represents Suppliers, this method might return 'Suppliers'
     *
     * @return string
     */
    public function getModelDescription();

    /**
     * This method should return the layout.xml name of the edit block being used to render the form.
     * For example, if the edit page has the following layout xml defined for the page:
     *
     *       <page xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:noNamespaceSchemaLocation="urn:magento:framework:View/Layout/etc/page_configuration.xsd">
     *           <body>
     *               <referenceContainer name="content">
     *                   <block class="Dunagan\Base\Block\Adminhtml\Widget\Form\Container" name="task_edit"/>
     *               </referenceContainer>
     *           </body>
     *       </page>
     *
     * then this method would return "task_edit"
     *
     * @return string
     */
    public function getModelEditBlockLayoutName();

    /**
     * This method should return the underscore-delimited name of the module which the form block will be found in.
     * This value is used to set the \Magento\Backend\Block\Widget\Container::_blockGroup field for the grid and form
     *      container classes
     * For example, if the form block is \Namespace\Module\Block\Adminhtml\ORM\Edit\Form.php, then this method
     *      should return "Namespace_Module"
     *
     *
     * @return string
     */
    public function getModuleName();

    /**
     * This method should return the block path suffix for the form block between the "Adminhtml" and "Edit" directories.
     *
     * For example, if the form block is \Namespace\Module\Block\Adminhtml\ORM\Edit\Form.php, then this method
     *      should return "ORM". It is necessary for the filepath structure to include the "Adminhtml" and "Edit"
     *      directories
     *
     * @return string
     */
    public function getBlockAdminhtmlPathSuffix();
}